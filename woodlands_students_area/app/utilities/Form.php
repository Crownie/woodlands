<?php
/**
 * Created by Tobi Ayilara.
 * Date: 06/02/2017
 * Time: 19:44
 */

namespace app\utilities;


class Form
{
    private static function getAttrString($element_attrs = [])
    {
        $str = '';
        foreach ($element_attrs as $name => $value) {
            $str .= $name . ' = "' . $value . '"';
        }
        return $str;
    }

    private static function getRequired($label)
    {
        $required = '';
        if (substr($label, 0, 1) == '*') {
            $required = 'required';
        }
        return $required;
    }

    public static function hiddenInput($field_name,$default = ''){
        $value = form_data($field_name, $default);
        echo '<input type="hidden" name="' . $field_name . '" value="'.$value.'">';
    }

    public static function input($field_name, $label, $default = '', $element_attrs = [], $parent_class_attr = 'col-sm-6')
    {
        $parent_class_attr .= ' ' . self::getFieldErrorClass($field_name);
        $element_attrs_default = [
            'class' => 'form-control',
            'name' => $field_name,
            'value' => form_data($field_name, $default),
            'placeholder' => '',
            'type' => 'text'
        ];

        $element_attrs = array_merge($element_attrs_default, $element_attrs);
        $element_attrs['class'] .= ' ' . $element_attrs_default['class'];


        echo '<div class="' . $parent_class_attr . ' form-group">
        <label >' . $label . '</label>
        <input ' . self::getRequired($label) . ' ' . self::getAttrString($element_attrs) . '>
        ' . self::fieldErrorMessage($field_name) . '
    </div>';
    }

    public static function select($field_name, $label, $default = '', $options = [], $parent_class_attr = 'col-sm-6')
    {
        $parent_class_attr .= ' ' . self::getFieldErrorClass($field_name);

        $value = form_data($field_name, $default);
        echo '<div class="' . $parent_class_attr . ' form-group">
        <label >' . $label . '</label>
        <select ' . self::getRequired($label) . ' name="' . $field_name . '" class="form-control">';

        foreach ($options as $key => $option) {
            if ($value == $key) {
                echo '<option selected value="' . $key . '">' . $option . '</option>';
            } else {
                echo '<option value="' . $key . '">' . $option . '</option>';
            }

        }

        echo '</select>' . self::fieldErrorMessage($field_name) . '</div>';
    }

    public static function searchableSelect($field_name, $label, $default = '', $options = [], $parent_class_attr = 'col-sm-6')
    {
        $parent_class_attr .= ' ' . self::getFieldErrorClass($field_name);

        $value = form_data($field_name, $default);
        echo '<div class="' . $parent_class_attr . ' form-group">
        <label >' . $label . '</label>
        <select ' . self::getRequired($label) . ' name="' . $field_name . '" class="form-control selectpicker" data-live-search="true">';

        foreach ($options as $key => $option) {
            if ($value == $key) {
                echo '<option selected value="' . $key . '">' . $option . '</option>';
            } else {
                echo '<option value="' . $key . '">' . $option . '</option>';
            }

        }

        echo '</select>' . self::fieldErrorMessage($field_name) . '</div>';
    }

    public static function multipleSelect($field_name, $label, $default = [], $options = [], $parent_class_attr = 'col-sm-6')
    {
        $parent_class_attr .= ' ' . self::getFieldErrorClass($field_name);

        $value = form_data($field_name, $default);
        $value = is_array($value)?$value:[];

        echo '<div class="' . $parent_class_attr . ' form-group">
        <label >' . $label . '</label>
        <select multiple="multiple" ' . self::getRequired($label) . ' name="' . $field_name . '[]" class="multiple-select form-control" >';

        foreach ($options as $key => $option) {

            $selected = '';
            if (in_array($key,$value)) {
                $selected = 'selected';
            }

            echo '<option '.$selected.' value="' . $key . '">' . $option . '</option>';

        }

        echo '</select>' . self::fieldErrorMessage($field_name) . '</div>';
    }

    public static function moneyInput($field_name, $label, $default = '', $parent_class_attr = 'col-sm-6')
    {
        $parent_class_attr .= ' ' . self::getFieldErrorClass($field_name);
        echo '
        <div class="' . $parent_class_attr . ' form-group">
           <label >' . $label . '</label>
            <div class="input-group">
                <div class="input-group-addon">£</div>
                <input ' . self::getRequired($label) . ' type="number" class="form-control" name="' . $field_name . '" value="' . form_data($field_name, $default) . '" step="any">
            </div>
            ' . self::fieldErrorMessage($field_name) . '
        </div>';
    }

    public static function textarea($field_name, $label, $default = '', $rows = 10, $parent_class_attr = 'col-sm-12')
    {
        $parent_class_attr .= ' ' . self::getFieldErrorClass($field_name);
        echo '<div class="' . $parent_class_attr . ' form-group">
                <label >' . $label . '</label>
                <textarea ' . self::getRequired($label) . ' name="' . $field_name . '" class="form-control" rows="' . $rows . '">' . form_data('description', $default) . '</textarea>
            ' . self::fieldErrorMessage($field_name) . '
            </div>';
    }

    public static function button($label, $class, $type = 'submit')
    {
        echo '
        <span class="clearfix"></span>
        <div class="col-sm-12 form-group">
            <button type="'.$type.'" class="btn ' . $class . '">' . $label . '</button>
        </div>';
    }

    public static function fileUpload($field_name, $label, $parent_class_attr = 'col-sm-6'){

        echo '<div class="col-sm-6">
                <label >'.$label.'</label>
                <input name="'.$field_name.'[]" type="file" multiple>
            </div>';
    }


    /**
     * @param $field_name
     * @param $label
     * @param string $default
     * @param array $options learn more http://eonasdan.github.io/bootstrap-datetimepicker/Options/
     * @param string $parent_class_attr
     */
    public static function datetime($field_name, $label, $default = '', $options = [], $parent_class_attr = 'col-sm-6')
    {
        $required = self::getRequired($label);
        $icon = 'calendar';
        $js_options_arr = json_encode($options);
        $element_id = 'datepicker-' . generate_random_string();

        if (is_object($default)) {
            try {
                $default = date("d/m/Y H:i:s", strtotime($default->$field_name));
            } catch (\Exception $e) {

            }
        }
        if (isset($options['format']) && $options['format'] == 'LT') {
            $icon = 'time';
        }

        $field_error_message = self::fieldErrorMessage($field_name);

        echo view()->render('partials/datepicker', get_defined_vars());
    }

    public static function time($field_name, $label, $default = '', $parent_class_attr = 'col-sm-6')
    {
        self::datetime($field_name, $label, $default, ['format' => 'HH:mm'], $parent_class_attr);
    }

    public static function date($field_name, $label, $default = '', $parent_class_attr = 'col-sm-6')
    {
        self::datetime($field_name, $label, $default, ['format' => 'DD/MM/YYYY'], $parent_class_attr);
    }

    /**
     * @param $field_name
     * @param $label
     * @param array $default an array of the keys of the selected checkboxes
     * @param array $options
     * @param string $parent_class_attr
     */
    public static function checkbox($field_name, $label, $default = [], $options = [], $parent_class_attr = 'col-sm-6')
    {
        $value = form_data($field_name, $default);
        $value = is_array($value)?$value:[];
        echo '<div class="' . $parent_class_attr . ' form-group">
            <label>' . $label . '</label>
            ';

        foreach ($options as $key => $option) {
            $checked = '';
            if (in_array($key,$value)) {
                $checked = 'checked';
            }

            echo '<div class="checkbox">
                        <label>
                            <input ' . $checked . ' type="checkbox" name="' . $field_name . '[]" value="' . $key . '"> ' . $option . '
                        </label>
                    </div>';

        }

        echo '</div>';
    }

    private static function fieldErrorMessage($field_name)
    {
        $form_errors = session()->has('form_errors') ? session()->get('form_errors') : [];
        if (is_array($form_errors) && key_exists($field_name, $form_errors)) {
            if (is_array($form_errors[$field_name])) {
                $error_msg = implode(' | ', $form_errors[$field_name]);
            } else {
                $error_msg = $form_errors[$field_name];
            }
            return '<span class="error">' . $error_msg . '</span>';
        }
        return '';
    }

    private static function getFieldErrorClass($field_name)
    {
        if (!empty(self::fieldErrorMessage($field_name))) {
            return 'has-error';
        }
        return '';
    }

    public static function row()
    {
        echo '<span class="clearfix"></span>';
    }

    public static function detailsChangeInput($field_name,$data){
        echo view()->render('details-change-request/partials/change-input',['field_name'=>$field_name,'data'=>$data]);
    }

}