<?php
/**
 * Created by Tobi Ayilara.
 * Date: 12/02/2017
 * Time: 17:13
 */

namespace app\utilities\validator;

class Validator
{
    private $data = [];
    private $rule_builders = [];
    private $errors = [];
    private $valid = true;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $field_name
     * @return ValidationRuleBuilder
     */
    public function startRulesFor($field_name)
    {
        $this->rule_builders[$field_name] = new ValidationRuleBuilder();
        return $this->rule_builders[$field_name];
    }

    public function isValid()
    {
        foreach ($this->rule_builders as $field_name => $rule_builder) {
            $rules = $rule_builder->getRulesArray();
            $required = false;
            foreach ($rules as $rule_name => $arguments) {
                switch ($rule_name) {
                    case 'required':
                        $required = true;
                        $this->testRequired($field_name);
                        break;
                    case 'length_between':
                        $this->testLengthBetween($field_name, $arguments);
                        break;
                    case 'email':
                        $this->testEmail($field_name);
                        break;
                    case 'alphanumeric':
                        $this->testAlphanumeric($field_name);
                        break;
                    case 'numeric':
                        $this->testNumeric($field_name);
                        break;
                    case 'matches_field':
                        $this->testMatchesField($field_name,$arguments);
                        break;
                    case 'greater_than':
                        $this->testGreaterThan($field_name,$arguments);
                        break;
                }
            }
        }
        return $this->valid;
    }

    private function testRequired($field_name)
    {
        $value = trim($this->data[$field_name]);
        if (strlen($value) < 1) {
            $this->addError($field_name, 'This field is required');
        }
    }

    private function testLengthBetween($field_name, $arguments)
    {
        $min = $arguments['min'];
        $max = $arguments['max'];
        $value = trim($this->data[$field_name]);
        if (strlen($value) < $min || strlen($value) > $max) {
            $this->addError($field_name, 'must be between ' . $min . ' - ' . $max . 'characters');
        }
    }


    private function testEmail($field_name)
    {
        $value = trim($this->data[$field_name]);

        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->addError($field_name, 'please enter a valid email');
        }
    }

    private function testAlphanumeric($field_name)
    {
        $value = trim($this->data[$field_name]);
        if (!ctype_alnum($value)) {
            $this->addError($field_name, 'This must contain only alphabets or numbers');
        }
    }

    private function testNumeric($field_name)
    {
        $value = trim($this->data[$field_name]);
        if (!is_numeric($value) && !empty($value)) {
            $this->addError($field_name, $field_name . ' must be numeric');
        }
    }

    private function testMatchesField($field_name, $args)
    {
        $other_field_name = $args['other_field_name'];
        $other_field_value = isset($this->data[$other_field_name]) ? $this->data[$other_field_name] : '';
        $value = $this->data[$field_name];
        if ($value !== $other_field_value) {
            $this->addError($field_name, $field_name . ' did not match ' . $other_field_name);
        }
    }

    private function testGreaterThan($field_name, $args)
    {
        $other_field_value = $args['other_field_value'];
        $error_message = $args['error_message'];
        $value = $this->data[$field_name];
        if ($value <= $other_field_value) {
            $this->addError($field_name, $error_message);
        }
    }

    private function addError($field_name, $error_msg)
    {
        $this->valid = false;
        if (!isset($this->errors[$field_name]) || !is_array($this->errors[$field_name])) {
            $this->errors[$field_name] = [];
        }
        $this->errors[$field_name][] = $error_msg;
    }

    public function getErrors()
    {
        return $this->errors;
    }



}