<?php
/**
 * Created by Tobi Ayilara.
 * Date: 12/02/2017
 * Time: 17:24
 */

namespace app\utilities\validator;


class ValidationRuleBuilder
{

    private $rules = [];

    /**
     * @return $this
     */
    public function required(){
        $this->rules['required'] = true;
        return $this;
    }

    public function length_between($min, $max){
        $this->rules['length_between'] = [
            'min'=>$min,
            'max'=>$max
        ];
        return $this;
    }

    public function email(){
        $this->rules['email'] = true;
        return $this;
    }

    public function alphanumeric(){
        $this->rules['alphanumeric'] = true;
        return $this;
    }

    public function numeric(){
        $this->rules['numeric'] = true;
        return $this;
    }

    public function matchesField($other_field_name){
        $this->rules['matches_field'] = ['other_field_name'=>$other_field_name];
        return $this;
    }
    public function greaterThan($value,$error_message){
        $this->rules['greater_than'] = ['other_field_value'=>$value,'error_message'=>$error_message];
        return $this;
    }

    public function getRulesArray(){
        return $this->rules;
    }
}