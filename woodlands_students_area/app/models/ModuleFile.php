<?php
/**
 * Created by PhpStorm.
 * User: Tomas Kompas
 * Date: 27/02/2017
 * Time: 14:39
 */

namespace app\models;


class ModuleFile extends Model
{
    protected $module_file_id;
    protected $url;
    /**
     * @return mixed
     */
    public function getModuleFileId()
    {
        return $this->module_file_id;
    }

    /**
     * @param mixed $module_file_id
     * @return ModuleFile
     */
    public function setModuleFileId($module_file_id)
    {
        $this->module_file_id = $module_file_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return ModuleFile
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

}