<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 15/02/2017
 * Time: 16:01
 */

namespace app\models;

use app\utilities\database_utilities\DatabaseTable;

class Attendance extends Model
{

    protected $attendance_id;
    protected $status;
    protected $student_id;
    protected $module_id;
    protected $week;


    /**
     * @return mixed
     */
    public function getAttendanceId()
    {
        return $this->attendance_id;
    }

    /**
     * @param mixed $attendance_id
     * @return Attendance
     */
    public function setAttendanceId($attendance_id)
    {
        $this->attendance_id = $attendance_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Attendance
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return Attendance
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     * @return Attendance
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
        return $this;
    }


    public static function getTable(){
        return new DatabaseTable('attendance',Attendance::class,'attendance_id');
    }

    public static function countAbsensePastLast3Weeks($student,$week){
        $absence_count = 1;
        for($i = $week-3; $i < $week; $i++){
            $attendances = Attendance::find('student_id = :student_id AND week = :week',
                [
                    'student_id'=>$student->getStudentId(),
                    'week'=>$i
                ]);
            $attendance = count($attendances)<=0?new Attendance():$attendances[0];
            if($attendance->getStatus()=='X'){
                $absence_count++;
            }
        }
        return $absence_count;
    }
}

