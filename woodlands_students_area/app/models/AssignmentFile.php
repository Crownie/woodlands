<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 11:35
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

class AssignmentFile extends Model
{
    protected $assignment_id;
    protected $file_url;

    /**
     * @return mixed
     */
    public function getAssignmentId()
    {
        return $this->assignment_id;
    }

    /**
     * @param mixed $assignment_id
     * @return AssignmentFile
     */
    public function setAssignmentId($assignment_id)
    {
        $this->assignment_id = $assignment_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }

    /**
     * @param mixed $file_url
     * @return AssignmentFile
     */
    public function setFileUrl($file_url)
    {
        $this->file_url = $file_url;
        return $this;
    }

    public static function getTable()
    {
        return new DatabaseTable('assignment_files',AssignmentFile::class,'assignment_file_id');
    }

}