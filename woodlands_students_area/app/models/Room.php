<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 11:39
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class Room extends Model
{
    protected $room_id;
    protected $room_number;
    protected $room_type;
    protected $capacity;

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     * @return Room
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomNumber()
    {
        return $this->room_number;
    }

    /**
     * @param mixed $room_number
     * @return Room
     */
    public function setRoomNumber($room_number)
    {
        $this->room_number = $room_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomType()
    {
        return $this->room_type;
    }

    /**
     * @param mixed $room_type
     * @return Room
     */
    public function setRoomType($room_type)
    {
        $this->room_type = $room_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param mixed $capacity
     * @return Room
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
        return $this;
    }


    public function getValidator(){
        $validator = new Validator($this->toArray());

        $validator->startRulesFor('room_number')->required()->alphanumeric();
        $validator->startRulesFor('room_type')->required();
        $validator->startRulesFor('capacity')->required()->numeric();

        return $validator;
    }

    public static function getTable(){
        return new DatabaseTable('rooms',Room::class,'room_id');
    }

}