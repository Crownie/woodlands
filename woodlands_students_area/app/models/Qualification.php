<?php
/**
 * Created by PhpStorm.
 * User: Tomas Kompas
 * Date: 27/02/2017
 * Time: 14:25
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

class Qualification extends Model
{
    protected $qualification_id;
    protected $student_id;
    protected $qualification_type;
    protected $qualification_name;
    protected $grade_achieved;
    protected $date_achieved;
    /**
     * @return mixed
     */
    public function getQualificationId()
    {
        return $this->qualification_id;
    }

    /**
     * @param mixed $qualification_id
     */
    public function setQualificationId($qualification_id)
    {
        $this->qualification_id = $qualification_id;
    }

    /**
     * @return mixed
     */
    public function getQualificationType()
    {
        return $this->qualification_type;
    }

    /**
     * @param mixed $qualification_type
     */
    public function setQualificationType($qualification_type)
    {
        $this->qualification_type = $qualification_type;
    }

    public function getQualificationTypeName(){
        $qualificationTypeName = $this->qualification_type;
        $qualificationTypes=[
            'fs' => 'Functional Skill',
            'elq' => 'Entry Level Qualfication',
            'gcse' => 'GCSE Qualification',
            'alvl' => 'A/AS Level Qualification',
            'ib' => 'International Baccalaureate Diploma',
            'vq' => 'Vocational Qualification',
            'heq' => 'Higher Education Qualification',
            'oth' => 'Other'
        ];

        foreach ($qualificationTypes as $key => $value){
            if ($qualificationTypeName==$key){
                $qualificationTypeName = $value;
            }
        }

        return $qualificationTypeName;
    }

    /**
     * @return mixed
     */
    public function getQualificationName()
    {
        return $this->qualification_name;
    }

    /**
     * @param mixed $qualification_name
     */
    public function setQualificationName($qualification_name)
    {
        $this->qualification_name = $qualification_name;
    }

    /**
     * @return mixed
     */
    public function getGradeAchieved()
    {
        return $this->grade_achieved;
    }

    /**
     * @param mixed $grade_achieved
     */
    public function setGradeAchieved($grade_achieved)
    {
        $this->grade_achieved = $grade_achieved;
    }

    /**
     * @return mixed
     */
    public function getDateAchieved()
    {
        return $this->date_achieved;
    }

    /**
     * @param mixed $date_achieved
     */
    public function setDateAchieved($date_achieved)
    {
        $this->date_achieved = $date_achieved;
    }/**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }
    /**
     * @param mixed $student_id
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
    }
    public static function getTable(){
        return new DatabaseTable('entry_qualifications', Qualification::class, 'qualification_id');
    }
    public static function clearQualifications($student_id){
                $table = static::getTable();
        $table->delete('student_id = :student_id',['student_id'=>$student_id]);
    }
}