<?php
/**
 * Created by Alex
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

class PersonalTutor extends Staff
{

    public static function getAll(){

        return self::find();
    }

    public static function find($conditions = '', $values = [], $sort = NULL){
        $pdo = DatabaseTable::getConnection();
        $criteria = $values;

        if(!empty($conditions)){
            $conditions = 'AND '.$conditions;
        }

        $stmt = $pdo->prepare("SELECT * FROM staff s 
                                JOIN staff_roles sr 
                                ON s.staff_id = sr.staff_id 
                                WHERE 
                                sr.role = 'PT' 
                                ".$conditions);

        $stmt->execute($criteria);

        $personal_tutors = DatabaseTable::getObjectArray(PersonalTutor::class,$stmt);
        return $personal_tutors;
    }

    public function getStudents(){
        $students=Student::find('personal_tutor_id=:personal_tutor_id',['personal_tutor_id'=>$this->staff_id]);
       return $students;
    }



}