<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 11:44
 */

namespace app\models;


class PersonalTutorial extends Model
{
    protected $personal_tutorial_id;
    protected $personal_tutorial_meeting;
    protected $date_of_meeting;
    protected $tutorial_summary;
    protected $action_points_summary;
    protected $next_meeting_date;
    protected $student_attended;
    protected $staff_id;
    protected $student_id;

    /**
     * @return mixed
     */
    public function getPersonalTutorialId()
    {
        return $this->personal_tutorial_id;
    }

    /**
     * @param mixed $personal_tutorial_id
     * @return PersonalTutorial
     */
    public function setPersonalTutorialId($personal_tutorial_id)
    {
        $this->personal_tutorial_id = $personal_tutorial_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalTutorialMeeting()
    {
        return $this->personal_tutorial_meeting;
    }

    /**
     * @param mixed $personal_tutorial_meeting
     * @return PersonalTutorial
     */
    public function setPersonalTutorialMeeting($personal_tutorial_meeting)
    {
        $this->personal_tutorial_meeting = $personal_tutorial_meeting;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateOfMeeting()
    {
        return $this->date_of_meeting;
    }

    /**
     * @param mixed $date_of_meeting
     * @return PersonalTutorial
     */
    public function setDateOfMeeting($date_of_meeting)
    {
        $this->date_of_meeting = $date_of_meeting;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTutorialSummary()
    {
        return $this->tutorial_summary;
    }

    /**
     * @param mixed $tutorial_summary
     * @return PersonalTutorial
     */
    public function setTutorialSummary($tutorial_summary)
    {
        $this->tutorial_summary = $tutorial_summary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionPointsSummary()
    {
        return $this->action_points_summary;
    }

    /**
     * @param mixed $action_points_summary
     * @return PersonalTutorial
     */
    public function setActionPointsSummary($action_points_summary)
    {
        $this->action_points_summary = $action_points_summary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextMeetingDate()
    {
        return $this->next_meeting_date;
    }

    /**
     * @param mixed $next_meeting_date
     * @return PersonalTutorial
     */
    public function setNextMeetingDate($next_meeting_date)
    {
        $this->next_meeting_date = $next_meeting_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudentAttended()
    {
        return $this->student_attended;
    }

    /**
     * @param mixed $student_attended
     * @return PersonalTutorial
     */
    public function setStudentAttended($student_attended)
    {
        $this->student_attended = $student_attended;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStaffId()
    {
        return $this->staff_id;
    }

    /**
     * @param mixed $staff_id
     * @return PersonalTutorial
     */
    public function setStaffId($staff_id)
    {
        $this->staff_id = $staff_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return PersonalTutorial
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }



    public function toArray()
    {
        // TODO: Implement toArray() method.
    }
}