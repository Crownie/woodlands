<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 15/02/2017
 * Time: 15:45
 */

namespace app\models;

use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;
use Symfony\Component\Config\Definition\Exception\Exception;

Class Student extends Model
{
    protected $student_id;
    protected $status;
    protected $dormancy_reason;
    protected $first_name;                  //
    protected $middle_name;                 //
    protected $surname;                     //
    protected $term_time_address_line1;     //
    protected $term_time_address_line2;     //
    protected $term_time_city;              //
    protected $term_time_country;           //
    protected $term_time_postcode;          //
    protected $address_line1;               //
    protected $address_line2;               //
    protected $city;                        //
    protected $country;                     //
    protected $postcode;                    //
    protected $phone;                       //
    protected $email;                       //
    protected $course_id;
    protected $personal_tutor_id;
    protected $level;
    protected $current_year;
    protected $password;

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return Student
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Student
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDormancyReason()
    {
        return $this->dormancy_reason;
    }

    /**
     * @param mixed $dormancy_reason
     * @return Student
     */
    public function setDormancyReason($dormancy_reason)
    {
        $this->dormancy_reason = $dormancy_reason;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     * @return Student
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * @param mixed $middle_name
     * @return Student
     */
    public function setMiddleName($middle_name)
    {
        $this->middle_name = $middle_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return Student
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTermTimeAddressLine1()
    {
        return $this->term_time_address_line1;
    }

    /**
     * @param mixed $term_time_address_line1
     * @return Student
     */
    public function setTermTimeAddressLine1($term_time_address_line1)
    {
        $this->term_time_address_line1 = $term_time_address_line1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTermTimeAddressLine2()
    {
        return $this->term_time_address_line2;
    }

    /**
     * @param mixed $term_time_address_line2
     * @return Student
     */
    public function setTermTimeAddressLine2($term_time_address_line2)
    {
        $this->term_time_address_line2 = $term_time_address_line2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTermTimeCity()
    {
        return $this->term_time_city;
    }

    /**
     * @param mixed $term_time_city
     * @return Student
     */
    public function setTermTimeCity($term_time_city)
    {
        $this->term_time_city = $term_time_city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTermTimeCountry()
    {
        return $this->term_time_country;
    }

    /**
     * @param mixed $term_time_country
     * @return Student
     */
    public function setTermTimeCountry($term_time_country)
    {
        $this->term_time_country = $term_time_country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTermTimePostcode()
    {
        return $this->term_time_postcode;
    }

    /**
     * @param mixed $term_time_postcode
     * @return Student
     */
    public function setTermTimePostcode($term_time_postcode)
    {
        $this->term_time_postcode = $term_time_postcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->address_line1;
    }

    /**
     * @param mixed $address_line1
     * @return Student
     */
    public function setAddressLine1($address_line1)
    {
        $this->address_line1 = $address_line1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->address_line2;
    }

    /**
     * @param mixed $address_line2
     * @return Student
     */
    public function setAddressLine2($address_line2)
    {
        $this->address_line2 = $address_line2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Student
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return Student
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     * @return Student
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Student
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Student
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @param mixed $course_id
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
    }


    public function getValidator()
    {
        $validator = new Validator($this->toArray());

        $validator->startRulesFor('first_name')->required();
        $validator->startRulesFor('surname')->required();
        $validator->startRulesFor('term_time_address_line1')->required();
        $validator->startRulesFor('term_time_city')->required();
        $validator->startRulesFor('term_time_country')->required();
        $validator->startRulesFor('term_time_postcode')->required();
        $validator->startRulesFor('phone')->required();
        $validator->startRulesFor('email')->required()->email();
        //$validator->startRulesFor('roles')->required();
        return $validator;
    }

    public static function getTable()
    {
        return new DatabaseTable('students', Student::class, 'student_id');
    }

    public function getCourse()
    {
        try{
            return Course::get($this->getCourseId());
        }
        catch(\Exception $e){
            return null;
        }

    }
    public function getQualifications(){
        try{
            return Qualification::find('student_id = :student_id',['student_id'=>$this->getStudentId()]);
        }
        catch(\Exception $e){
            return [];
        }
    }

    public function getFullName()
    {
        return $this->first_name.' '.$this->surname;
    }

    /**
     * @return mixed
     */
    public function getPersonalTutorId()
    {
        return $this->personal_tutor_id;
    }

    /**
     * @param mixed $personal_tutor_id
     */
    public function setPersonalTutorId($personal_tutor_id)
    {
        $this->personal_tutor_id = $personal_tutor_id;
    }

    public static function getStudentsByDeptAndLevel($department_id,$level,$personal_tutor_id){
        $pdo = DatabaseTable::getConnection();
        $ps = 's.personal_tutor_id = :personal_tutor_id ';
        $criteria = [
            'personal_tutor_id'=>$personal_tutor_id,
            'department_id'=>$department_id,
            'level'=>$level
        ];
        if(empty($personal_tutor_id)){
            $ps = 'personal_tutor_id IS NULL ';
            unset($criteria['personal_tutor_id']);
        }
        $stmt = $pdo->prepare('SELECT * FROM students s 
                                JOIN courses c 
                                ON s.course_id = c.course_id 
                                JOIN departments d 
                                ON c.department_id = d.department_id 
                                WHERE '.$ps.'
                                AND s.level = :level 
                                AND d.department_id = :department_id');

        $stmt->execute($criteria);

        $students = DatabaseTable::getObjectArray(Student::class,$stmt);
        return $students;
    }

    public function isAddressTheSame(){

        if(empty($this->getAddressLine1())){
            return ['yes'];
        }
        return [];
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getCurrentYear()
    {
        return $this->current_year;
    }

    /**
     * @param mixed $current_year
     */
    public function setCurrentYear($current_year)
    {
        $this->current_year = $current_year;
    }

    public static function getStudentsNotInModuleGroup($module_id,$level){
        $pdo = DatabaseTable::getConnection();

        $stmt = $pdo->prepare('
        SELECT * FROM students s
						WHERE
                        :module_id IN (
								SELECT module_id FROM course_modules cm
                                WHERE cm.course_id = s.course_id)
						AND
                        NOT EXISTS(
								SELECT * FROM student_module_groups
                                WHERE student_id = s.student_id AND module_id = :module_id)
                        AND s.level = :level;
        ');

       $stmt->execute(['module_id'=>$module_id,'level'=>$level]);

        $students = DatabaseTable::getObjectArray(static::class,$stmt);
        return $students;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}
