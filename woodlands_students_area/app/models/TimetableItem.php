<?php

/**
 * Created by Alex

 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class TimetableItem extends Model
{
    protected $timetable_item_id;
    protected $timetable_item_type;
    protected $module_group_number;
    protected $module_id;
    protected $day;
    protected $start_time;
    protected $end_time;
    protected $room_id;
    protected $timetable_id;

    /**
     * @return mixed
     */
    public function getTimetableItemId()
    {
        return $this->timetable_item_id;
    }

    /**
     * @param mixed $timetable_item_id
     * @return TimetableItem
     */
    public function setTimetableItemId($timetable_item_id)
    {
        $this->timetable_item_id = $timetable_item_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimetableItemType()
    {
        return $this->timetable_item_type;
    }

    /**
     * @param mixed $timetable_item_type
     * @return TimetableItem
     */
    public function setTimetableItemType($timetable_item_type)
    {
        $this->timetable_item_type = $timetable_item_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleGroupNumber()
    {
        return $this->module_group_number;
    }

    /**
     * @param mixed $module_group_number
     * @return TimetableItem
     */
    public function setModuleGroupNumber($module_group_number)
    {
        $this->module_group_number = $module_group_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     * @return TimetableItem
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     * @return TimetableItem
     */
    public function setDay($day)
    {
        $this->day = $day;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->start_time;
    }

    /**
     * @param mixed $start_time
     * @return TimetableItem
     */
    public function setStartTime($start_time)
    {
        $this->start_time = $start_time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->end_time;
    }

    /**
     * @param mixed $end_time
     * @return TimetableItem
     */
    public function setEndTime($end_time)
    {
        $this->end_time = $end_time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     * @return TimetableItem
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimetableId()
    {
        return $this->timetable_id;
    }

    /**
     * @param mixed $timetable_id
     * @return TimetableItem
     */
    public function setTimetableId($timetable_id)
    {
        $this->timetable_id = $timetable_id;
        return $this;
    }

    public function getModule(){
        return Module::get($this->module_id);
    }

    public static function getTable()
    {
        return new DatabaseTable('timetable_items', TimetableItem::class, 'timetable_item_id');
    }

    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('end_time')->greaterThan($this->start_time,'End time must be greater than start time');
        return $validator;
    }

    /**
     * @return Room
     */
    public function getRoom()
    {
        return Room::get($this->room_id);
    }

}