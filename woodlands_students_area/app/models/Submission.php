<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 15/02/2017
 * Time: 16:07
 */

namespace app\models;

use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class Submission extends Model
{

    protected $submission_id;
    protected $title;
    protected $date_submitted;
    protected $student_id;
    protected $assignment_id;
    protected $file_url;
    protected $grade;
    protected $feedback;

    /**
     * @return mixed
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * @param mixed $submission_id
     * @return Submission
     */
    public function setSubmissionId($submission_id)
    {
        $this->submission_id = $submission_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Submission
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateSubmitted()
    {
        return $this->date_submitted;
    }

    /**
     * @param mixed $date_submitted
     * @return Submission
     */
    public function setDateSubmitted($date_submitted)
    {
        $this->date_submitted = $date_submitted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return Submission
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssignmentId()
    {
        return $this->assignment_id;
    }

    /**
     * @param mixed $assignment_id
     * @return Submission
     */
    public function setAssignmentId($assignment_id)
    {
        $this->assignment_id = $assignment_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }

    /**
     * @param mixed $file_url
     * @return Submission
     */
    public function setFileUrl($file_url)
    {
        $this->file_url = $file_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     * @return Submission
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * @param mixed $feedback
     * @return Submission
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
        return $this;
    }



    public function getValidator()
    {
        $validator = new Validator($this->toArray());
        return $validator;
    }


    public static function getTable(){
        return new DatabaseTable('submissions',Submission::class,'submission_id');
    }

    /**
     * @return Student
     */
    public function getStudent(){
        return Student::get($this->student_id);
    }


}
