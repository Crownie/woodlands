<?php
/**
 * Created by Tobi Ayilara.
 * Date: 06/03/2017
 * Time: 08:12
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class CourseModule extends Model
{
    protected $course_id;
    protected $module_id;

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @param mixed $course_id
     * @return CourseModule
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }


    /**
     * @return Module
     */
    public function getModule(){
        return Module::get($this->module_id);
    }

    /**
     * @return Course
     */
    public function getCourse(){
        return Course::get($this->course_id);
    }


    public function getValidator()
    {
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('course_id')->required();
        $validator->startRulesFor('module_id')->required();
        return $validator;
    }

    public static function getTable()
    {
        return new DatabaseTable('course_modules', CourseModule::class, null);
    }

    public static function updateModules($course_id, $module_ids)
    {
        $table = static::getTable();
        $table->delete('course_id = :id', ['id' => $course_id]);

        foreach ($module_ids as $module_id){
            $table->insert(['course_id'=>$course_id,'module_id'=>$module_id]);
        }
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
    }
}