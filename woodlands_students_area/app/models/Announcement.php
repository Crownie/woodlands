<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 2017/2/20
 * Time: 13:34
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class Announcement extends Model
{
    protected $announcement_id;
    protected $title;
    protected $announcement_type;
    protected $message;
    protected $module_id;
    protected $course_id;
    protected $target;
    protected $date_posted;

    /**
     * @return mixed
     */
    public function getAnnouncementId()
    {
        return $this->announcement_id;
    }

    /**
     * @param mixed $announcement_id
     */
    public function setAnnouncementId($announcement_id)
    {
        $this->announcement_id = $announcement_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAnnouncementType()
    {
        return $this->announcement_type;
    }

    /**
     * @param mixed $announcement_type
     */
    public function setAnnouncementType($announcement_type)
    {
        $this->announcement_type = $announcement_type;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
    }

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @param mixed $course_id
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return mixed
     */
    public function getDatePosted()
    {
        return $this->date_posted;
    }

    /**
     * @param mixed $date_posted
     */
    public function setDatePosted($date_posted)
    {
        $this->date_posted = $date_posted;
    }


    public static function getTable(){
        return new DatabaseTable('announcements',static::class,'announcement_id');
    }

    public static function getAnnouncementsForStudent($student_id){
        $pdo = DatabaseTable::getConnection();

        $stmt = $pdo->prepare('
SELECT * FROM announcements
WHERE announcement_type = \'GENERAL\'
OR  
	(announcement_type = \'MODULE\' 
		AND 
		module_id IN (SELECT cm.module_id FROM students s
						JOIN course_modules cm
						ON s.course_id = cm.course_id
						WHERE s.student_id = :student_id AND cm.module_id = module_id)
	)
OR (announcement_type = \'COURSE\'
	AND course_id IN (SELECT course_id FROM students WHERE student_id = :student_id)
    )
ORDER BY announcement_id DESC;
        ');

        $stmt->execute(['student_id'=>$student_id]);

        $announcements = DatabaseTable::getObjectArray(Announcement::class,$stmt);
        return $announcements;
    }

    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('title')->required();
        $validator->startRulesFor('announcement_type')->required();
        $validator->startRulesFor('target')->required();
        $validator->startRulesFor('message')->required();

        return $validator;
    }


}