<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 11:39
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class Department extends Model
{
    protected $department_id;
    protected $name;

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * @param mixed $department_id
     * @return Department
     */
    public function setDepartmentId($department_id)
    {
        $this->department_id = $department_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }




    public static function getTable(){
        return new DatabaseTable('departments',Department::class,'department_id');
    }

}