<?php
/**
 Created by Alex
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

class StudentModuleGroup extends Model
{
    protected $module_id;
    protected $student_id;
    protected $group_number;

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return StudentModuleGroup
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     * @return StudentModuleGroup
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupNumber()
    {
        return $this->group_number;
    }

    /**
     * @param mixed $group_number
     * @return StudentModuleGroup
     */
    public function setGroupNumber($group_number)
    {
        $this->group_number = $group_number;
        return $this;
    }

    public static function getTable()
    {
        return new DatabaseTable('student_module_groups', StudentModuleGroup::class, 'student_module_group_id');
    }


    public function getStudent(){
        return Student::get($this->student_id);
    }

}