<?php
/**
 * Created by Alex
 */

namespace app\models;


class ExamFile extends File
{
    protected $exam_id;
    protected $file_url;

    /**
     * @return mixed
     */
    public function getExamId()
    {
        return $this->exam_id;
    }

    /**
     * @param mixed $exam_id
     * @return ExamFile
     */
    public function setExamId($exam_id)
    {
        $this->exam_id = $exam_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }

    /**
     * @param mixed $file_url
     * @return ExamFile
     */
    public function setFileUrl($file_url)
    {
        $this->file_url = $file_url;
        return $this;
    }

    public function toArray()
    {
        // TODO: Implement toArray() method.
    }

}