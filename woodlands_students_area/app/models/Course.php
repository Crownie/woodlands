<?php
/**
 * Created by Tobi Ayilara.
 * Date: 06/02/2017
 * Time: 10:56
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class Course extends Model
{
    protected $course_id;
    protected $title;
    protected $cost;
    protected $start_year;
    protected $duration;
    protected $description;
    protected $ucas_points;



    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @param mixed $course_id
     * @return Course
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Course
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $cost
     * @return Course
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartYear()
    {
        return $this->start_year;
    }

    /**
     * @param mixed $start_year
     * @return Course
     */
    public function setStartYear($start_year)
    {
        $this->start_year = $start_year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return Course
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Course
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUcasPoints()
    {
        return $this->ucas_points;
    }

    /**
     * @param mixed $ucas_points
     * @return Course
     */
    public function setUcasPoints($ucas_points)
    {
        $this->ucas_points = $ucas_points;
        return $this;
    }

    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('title')->required();
        $validator->startRulesFor('start_year')->required();
        return $validator;
    }

    public static function getTable(){
        return new DatabaseTable('courses',Course::class,'course_id');
    }

    /**
     * @return array
     */
    public function getCourseModules()
    {
        $course_modules = CourseModule::find('course_id=:course_id', ['course_id' => $this->course_id]);
        return $course_modules;
    }

    public static function updateModules($id, $selected)
    {

    }

}