<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 12:45
 */

namespace app;


use Silex\Application;
use Silex\ControllerCollection;
use Silex\Provider\CsrfServiceProvider;
use Silex\Provider\RoutingServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class Router extends Application
{
    public function __construct(array $values = array())
    {
        parent::__construct($values);
        $this->register(new RoutingServiceProvider());
        $this->register(new SessionServiceProvider());
        $this->register(new CsrfServiceProvider());
    }

    /**
     * registers predefined routes for the specified controller class and $baseRouteName.
     * -for example; if the $baseRouteName is "courses" then the registered routes will be;
     * + GET /courses/    --->>    showRecordList() named courses.showRecordList
     * + GET /courses/create    --->>    showRecordForm() named courses.showRecordForm
     * + GET /courses/{id}    --->>    showRecord() named courses.showRecord
     * + GET /courses/{id}/edit    --->>    showRecordEditForm() named courses.showRecordEditForm
     * + POST /courses/    --->>    storeRecord() named courses.storeRecord
     * + POST /courses/{id}    --->>    updateRecord() named courses.updateRecord
     * + POST /courses/{id}/delete    --->>    deleteRecord() named courses.deleteRecord
     * @param $className string the class's fully qualified name
     * @param $baseRouteName string
     */
    public function crud($className, $baseRouteName){
        /** @var $collection ControllerCollection */
        $collection = $this['controllers_factory'];

        //e.g. rms.wuc.ac.uk/courses/
        $collection->get('/', $className . '::showRecordList')->bind($baseRouteName.'.showRecordList');
        //e.g. rms.wuc.ac.uk/courses/create
        $collection->get('/create', $className . '::showRecordForm')->bind($baseRouteName . '.showRecordForm');
        //e.g. rms.wuc.ac.uk/courses/1
        $collection->get('/{id}', $className . '::showRecord')->bind($baseRouteName . '.showRecord')->assert('id', '\d+');
        //e.g. rms.wuc.ac.uk/courses/1/edit
        $collection->get('/{id}/edit', $className . '::showRecordEditForm')->bind($baseRouteName . '.showRecordEditForm')->assert('id', '\d+');
        //e.g. rms.wuc.ac.uk/courses/
        $collection->post('/', $className . '::storeRecord')->bind($baseRouteName . '.storeRecord');
        //e.g. rms.wuc.ac.uk/courses/1
        $collection->post('/{id}', $className . '::updateRecord')->bind($baseRouteName . '.updateRecord')->assert('id', '\d+');
        //e.g. rms.wuc.ac.uk/courses/1/delete
        $collection->post('/{id}/delete', $className . '::deleteRecord')->bind($baseRouteName . '.deleteRecord')->assert('id', '\d+');

        $this->mount($baseRouteName, $collection);
    }

    public function generateUrl($name, $parameters = array())
    {
        return $this['url_generator']->generate($name, $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function getCurrentUrl()
    {
        $r = $this['request_context'];

        return $r->getScheme() . '://' . $r->getHost() . $r->getPathInfo();
    }

    public function getSession()
    {
        return $this['session'];
    }

    /**
     * @return CsrfTokenManager
     */
    public function getTokenManager(){
        return $this['csrf.token_manager'];
    }
}