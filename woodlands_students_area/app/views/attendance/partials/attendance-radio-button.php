<?php
use app\models\Attendance;

$uid = generate_random_string();

    $options = [
        'X'=>'X',
        'O'=>'O',
        'A'=>'A'
    ];

$criteria = [
    'attendance_type'=>$attendance_type,
    'module_id'=>$module_id,
    'student_id'=>$student->getStudentId(),
    'week'=>$week
];
$attendances = Attendance::find('attendance_type = :attendance_type AND module_id = :module_id AND student_id = :student_id AND week = :week',
    $criteria);

$attendance = count($attendances)<=0?new Attendance([]):$attendances[0];

$value = count($attendance->getStatus())==0?'X':$attendance->getStatus();
?>
<div class="input-group">
    <div class="attendance-radio btn-group">
        <?php
            foreach ($options as $option){
                $active = '';
                if($option == $value){
                    $active = 'active';
                }
                ?>
                <a class="btn btn-success btn-sm <?=$active?>" data-toggle="attendance-<?=$uid?>" data-title="<?=$option?>"><?=$option?></a>
                <?php
            }

        ?>
    </div>
    <input type="hidden" value="<?=$value?>" data-student-id="<?=$student->getStudentId()?>"  data-attendance-type="<?=$attendance_type?>" name="attendance-<?=$uid?>" id="attendance-<?=$uid?>">
</div>