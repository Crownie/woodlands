<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Document</title>
<script>
    var csrf = '<?=csrf_token()?>';
</script>
<?php
style('bootstrap/css/bootstrap.css');
style('font-awesome/css/font-awesome.css');
style('css/timetablejs.css');
style('css/bootstrap-datetimepicker.min.css');
style('css/bootstrap-select.min.css');
style('css/toastr.min.css');
style('css/sweetalert.css');
style('css/multi-select.css');
style('css/fullcalendar.min.css');
style('css/fullcalendar.print.min.css','print');
style('css/style.css');

script('js/jquery.min.js');
script('js/moment.js');
script('bootstrap/js/bootstrap.js');
script('js/bootstrap-datetimepicker.js');
script('js/bootstrap-select.min.js');
script('js/printThis.js');
script('js/timetable.min.js');
script('js/toastr.min.js');
script('js/sweetalert.min.js');
script('js/jquery.quicksearch.js');
script('js/jquery.multi-select.js');
script('js/fullcalendar.min.js');
script('js/diary.js');
script('js/multiple-select-modal-ajax.js');
script('js/main.js');
?>