<div class="sidebar">
    <ul class="nav sidebar-nav">
        <?php
        sidebarMenuItem('Modules', route('modules.showRecordList'), 'book', []);
        sidebarMenuItem('Assignments', route('assignments.showRecordList'), 'file-text', []);
        sidebarMenuItem('Timetable', route('timetable.showRecordList'), 'th-list');
        sidebarMenuItem('Diary', route('diary.showRecordList'), 'calendar');
        ?>
    </ul>
</div>