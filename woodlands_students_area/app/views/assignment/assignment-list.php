<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Assignments</h2>


<table class="table table-striped">
    <thead>
    <tr>
        <th>Module</th>
        <th>Assignment Title</th>
        <th>Start Date</th>
        <th>Due date</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody
    <?php
    foreach ($assignments as $assignment){
        //dd($assignment);
        $this->insert('assignment/partials/assignment-item',['assignment'=>$assignment]);
    }
    ?>

    </tbody>
</table>

