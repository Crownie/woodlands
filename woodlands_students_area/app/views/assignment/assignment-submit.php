<?php
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Submit Assignment</h2>

<form action="<?=route('assignments.submitAssignment',['id'=>$id])?>" method="POST" enctype="multipart/form-data">
    <?php

    Form::input('title', '*Submission Title', '');

    Form::input('submission_file','*Upload file', '', ['type'=>'file']);

    Form::button('Submit','btn btn-success');

    ?>
</form>