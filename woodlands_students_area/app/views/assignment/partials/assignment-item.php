<tr data-id="<?=$assignment->getAssignmentId()?>">
    <th><?=$assignment->getModule()->getTitle()?></th>
    <th><?=$assignment->getTitle()?></th>
    <th><?=$assignment->getStartDate()?></th>
    <th><?=$assignment->getDueDate()?></th>

    <th>
        <a href="<?=route('assignments.showRecord',['id'=>$assignment->getAssignmentId()])?>" class="btn btn-default">View</a>
    </th>
</tr>