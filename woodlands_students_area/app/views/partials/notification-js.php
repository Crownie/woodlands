<form name="ignore_me">
    <input type="hidden" id="page_is_dirty" name="page_is_dirty" value="0" />
</form>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    };

    var dirty_bit = document.getElementById('page_is_dirty');
    if (dirty_bit.value == '1'){
        //user clicked back button
        //toast already shown to user
    }else{
        toastr["<?=$notification->type?>"]("<?=$notification->message?>");
        dirty_bit.value = '1';
    }

</script>