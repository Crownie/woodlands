<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Create Room</h2>

<form action="<?=route('rooms.storeRecord')?>" method="POST">
    <?php $this->insert('room/room-form',get_defined_vars()) ?>
</form>
