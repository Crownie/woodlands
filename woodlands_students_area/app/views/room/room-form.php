<?php
/**
 * Created by Saif
 * Date: 27/02/2017
 * Time: 14:40
 */
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

Form::input('room_number', '*Room Number', $room->getRoomNumber());

Form::input('room_type', 'Room Type', $room->getRoomType());

Form::input('capacity', '*Capacity', $room->getCapacity());

Form::button('Submit', 'btn-success');