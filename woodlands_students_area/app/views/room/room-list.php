<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Rooms</h2>

<p>
    <a href="<?=route('rooms.showRecordForm')?>" class="btn btn-success pull-right">Create Room</a>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Room Number</th>
        <th>Room Type</th>
        <th>Capacity</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($rooms as $room){
        $this->insert('room/partials/room-item',['room'=>$room]);
    }
    ?>
    </tbody>
</table>