<?php use app\models\Course;
use app\models\CourseModule;
use app\models\Module;

$this->layout('module/module-tab-layout', get_defined_vars())
/**@var $module Module */
?>


<table class="table table-striped">
    <thead>
    <tr>
        <th>Assignment Title</th>
        <th>Start Date</th>
        <th>Due date</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody
    <?php
    foreach ($assignments as $assignment){
        //dd($assignment);
        $this->insert('assignment/partials/assignment-item',['assignment'=>$assignment]);
    }
    ?>


    </tbody>
</table>
