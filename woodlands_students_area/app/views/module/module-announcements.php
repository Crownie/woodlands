<?php use app\models\Course;
use app\models\CourseModule;
use app\models\Module;

$this->layout('module/module-tab-layout', get_defined_vars())
/**@var $module Module */
?>

<div class="container">
    <?php
    foreach ($announcements as $announcement) {
        if (!empty($announcement->getModuleId()) && $announcement->getModuleId() == $module->getModuleId()) {

            ?>
            <br>
            <div class="row well">
                <div class="col-sm-12">
                    <p><strong><?= $announcement->getTitle() ?></strong></p>
                    <div style="border-top:1px solid #cfcfcf;border-bottom:1px solid #cfcfcf;">
                        <strong>Posted
                            on:</strong> <?= date('l, j F Y h:i A', strtotime($announcement->getDatePosted())) ?> |
                        <strong>Posted by:</strong> Simon White
                    </div>
                    <p>
                        <?= $announcement->getMessage() ?>
                    </p>
                </div>
            </div>


            <?php
        }
    }
    ?>
</div>
