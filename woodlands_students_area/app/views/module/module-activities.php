<?php use app\models\Course;
use app\models\CourseModule;
use app\models\Module;

$this->layout('module/module-tab-layout', get_defined_vars())
/**@var $module Module */
?>


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Topic 1-Introduction
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <table class="table table-striped">

                    <tbody>
                    <tr>

                        <th>Attached Files</th>
                        <td><a href="#">introduction.ppt (191 KB)</a></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td><a href = "#">activity1.docx (300 KB)</a></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Topic 2-Data Representation
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <table class="table table-striped">

                    <tbody>
                    <tr>

                        <th>Attached Files</th>
                        <td><a href ="#">dataRepresentation.ppt (400 KB)</a></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td><a href="#">activity2.docx (260 KB)</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Topic 3-Logic
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <table class="table table-striped">

                    <tbody>
                    <tr>

                        <th>Attached Files</th>
                        <td><a href ="#">introductionToLogic.ppt (500 KB)</a></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td><a href="#" >activity3.docx (400 KB)</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
