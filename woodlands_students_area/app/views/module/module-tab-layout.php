<?php
use app\models\Module;

$this->layout('layouts/master', ['title' => 'User Profile']);
/**@var $module Module */

?>
<h2 class="page-header"><?= $module->getTitle() ?></h2>
<?php
$tabs = [
    'Announcements' => route('module.announcements', ['id' => 1]),
    'Module Activities' => route('module.activities', ['id' => 1]),
    'Grades' => route('module.grades', ['id' => 1]),
    'About Module' => route('module.about', ['id' => 1])
];
?>
<script>
    $(function () {
        $('body').on('click', '[data-toggle="tab"]', function (e) {
            e.preventDefault();
            location = $(this).attr('href');
        })
    });
</script>
<ul class="nav nav-tabs">

    <?php
    foreach ($tabs as $tab_name => $url) {
        ?>
        <li class="<?= is_current_route($url) ? 'active' : '' ?>"><a data-toggle="tab"
                                                                     href="<?= $url ?>"><?= $tab_name ?></a></li>
        <?php
    }

    ?>
</ul>
<div class="tab-content">
    <div id="menu1" class="tab-pane fade in active">
        <?= $this->section('content') ?>
    </div>
</div>
