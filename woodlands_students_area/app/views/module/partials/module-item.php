<tr data-id="<?=$module->getModuleCode()?>">
    <th><?=$module->getModuleCode()?></th>
    <td><?=$module->getTitle()?></td>
    <td><?=$module->getLevel()?> Years</td>
    <td><?=$module->getCredits()?></td>

    <th>
        <a href="<?=route('modules.showRecord',['id'=>$module->getModuleId()])?>" class="btn btn-default">View</a>
        <a href="<?=route('modules.showRecordEditForm',['id'=>$module->getModuleId()])?>" class="btn btn-default">Edit</a>
        <div class="btn btn-default" data-toggle="modal" data-target="#course-to-module-modal">Assign to Module</div>
        <?php
        gen_delete_button(route('modules.deleteRecord',['id'=>$module->getModuleId()]),'this will delete the '.$module->getTitle().' module');
        ?>
        <div class="btn btn-default">Archive</div>
    </th>
</tr>