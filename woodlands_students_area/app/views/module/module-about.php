<?php use app\models\Course;
use app\models\CourseModule;
use app\models\Module;

$this->layout('module/module-tab-layout', get_defined_vars())
/**@var $module Module */
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Module Details</h1>
    </div>
    <div class="panel-body">
        <div>
            <strong>Module Leader</strong>
            <div class="col sm6">
                <?php
                $module_leader = $module->getModuleLeader();
                if(!empty($module_leader)){
                    echo $module_leader->getFullName();
                }else{
                    echo 'No leader';
                }
                ?>
                <br> <br>
                <p>As technology develops, the demand for computer-literate mathematicians continues to expand. From understanding the behaviour of complex software to
                    designing super-fast algorithms, maths is essential.</p>

                <p>Computational mathematics involves mathematical research in
                    areas of science where computing plays a central and essential
                    role, emphasizing algorithms, numerical methods, and symbolic computations.
                    Computation in research is prominen</p>


            </div>
        </div>
    </div>
</div>