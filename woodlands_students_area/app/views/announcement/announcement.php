<?php
$this->layout('layouts/master', ['title' => 'User Profile']);
?>

<h2 class="page-header">Announcements</h2>

<div class="announcements-container">
    <?php
    foreach ($announcements as $announcement) {
        $active_class = ($announcement->getDatePosted()>$date_read)?  $active_class = 'active':'';
        ?>
        <br>
        <div class="row well <?=$active_class?>">
            <div class="col-sm-12">
                <p><strong><?= $announcement->getTitle() ?></strong></p>
                <div style="border-top:1px solid #cfcfcf;border-bottom:1px solid #cfcfcf;">
                    <strong>Posted
                        on:</strong> <?= date('l, j F Y h:i A', strtotime($announcement->getDatePosted())) ?> |
                    <strong>Posted by:</strong> Simon White
                </div>
                <p>
                    <?= $announcement->getMessage() ?>
                </p>
            </div>
        </div>


        <?php

    }
    ?>
</div>