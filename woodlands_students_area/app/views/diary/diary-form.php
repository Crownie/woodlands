<?php
/**
 * Created by Tobi Ayilara.
 * Date: 05/02/2017
 * Time: 17:10
 */
use app\models\Event;
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php
/* @var $event Event*/
Form::input('title', '*Event Title', $event->getTitle(),[],'col-sm-12');
Form::row();
Form::datetime('start_date','*Start Date',$event->getStartDate());
Form::datetime('end_date','*End Date',$event->getStartDate());
Form::row();
Form::input('location', 'Location', $event->getLocation());
Form::row();
Form::textarea('description','Description',$event->getDescription());
Form::button('Submit','btn btn-success');

?>
