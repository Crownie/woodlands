<?php
use app\controllers\ModuleController;
use app\controllers\RoomController;
use app\models\Module;
use app\utilities\Form;

/* @var $timetable_item \app\models\TimetableItem */
?>
<div class="row"><strong class="col-sm-3" >Module:</strong class="col-sm-3" >
    <div class="col-sm-9"><?= Module::get($timetable_item->getModuleId())->getTitle() ?></div>
</div>
<div class="row"><strong class="col-sm-3" >Type:</strong class="col-sm-3" >
    <div class="col-sm-9"><?= $timetable_item->getTimetableItemType() ?></div>
</div>
<div class="row"><strong class="col-sm-3" >Group:</strong class="col-sm-3" >
    <div class="col-sm-9"><?php
        if ($timetable_item->getModuleGroupNumber() == 0) {
            echo 'All Groups';
        } else {
            echo 'Group ' . $timetable_item->getModuleGroupNumber();
        }
        ?></div>
</div>
<div class="row"><strong class="col-sm-3" >Day:</strong class="col-sm-3" >
    <div class="col-sm-9"><?= $timetable_item->getDay() ?></div>
</div>
<div class="row"><strong class="col-sm-3" >Start Time:</strong class="col-sm-3" >
    <div class="col-sm-9"><?= $timetable_item->getStartTime() ?></div>
</div>
<div class="row"><strong class="col-sm-3" >End Time:</strong class="col-sm-3" >
    <div class="col-sm-9"><?= $timetable_item->getEndTime() ?></div>
</div>
<div class="row"><strong class="col-sm-3" >Room:</strong class="col-sm-3" >
    <div class="col-sm-9"><?= $timetable_item->getRoom()->getRoomNumber() ?></div>
</div>

