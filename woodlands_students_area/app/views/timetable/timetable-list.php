<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>


<h2 class="page-header">Timetable</h2>

<?php

if(empty($timetable)){
    ?>
    You don't have a timetable yet
    <?php
}else{
    ?>
    <div class="pull-right btn btn-default print-element" data-target=".timetable-container">
        <i class="fa fa-print"></i>
        Print Timetable
    </div>
    <span class="clearfix"></span>

    <div class="timetable-container">
        <style>
            @media print {
                .timetable-container h4{
                    display: none;
                }
                .timetable-container h4{
                    display: block;
                }
            }
        </style>
        <h4><?=$timetable->getTitle()?> Timetable</h4>
        <div class="timetable" data-json-url="<?=route('timetable.getEvents')?>" data-edit-form-url="<?=route('timetable.getItemDetails')?>"></div>
    </div>
    <?php
}

?>
<!-- Modal -->
<div class="modal fade" id="timetable-entry-editor-modal" tabindex="-1" role="dialog" aria-labelledby="timetable-entry-editor-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Timetable Item</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>