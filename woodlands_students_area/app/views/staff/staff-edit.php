<?php $this->layout('layouts/master', ['title' => 'User Profile'])?>
<h2 class="page-header">Edit Staff</h2>

<form action="<?=route('staff.updateRecord',['id'=>$id])?>" method="POST">
    <?php $this->insert('staff/staff-form',get_defined_vars()) ?>
</form>
