<?php $this->layout('layouts/master', ['title' => 'User Profile'])?>

<h2 class="page-header">Create Staff</h2>
<p>
    <a href="<?=route('staff.showRecordList')?>" class="pull-left btn btn-success">List</a>
<div class="clearfix"></div>
</p>
<form action="<?=route('staff.storeRecord')?>" method="POST">
    <?php $this->insert('staff/staff-form',get_defined_vars()) ?>
</form>
