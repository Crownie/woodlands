<tr data-id="<?=$staff->getStaffId()?>">
    <th><?=$staff->getStaffId()?></th>
    <th><?=$staff->getFirstName()?></th>
    <td><?=$staff->getSurname()?></td>
    <td><?=$staff->getPhone()?></td>
    <td><?=$staff->getEmail()?></td>
    <th>
        <a href="<?=route('staff.showRecord',['id'=>$staff->getStaffId()])?>" class="btn btn-default">View</a>
        <a href="<?=route('staff.showRecordEditForm',['id'=>$staff->getStaffId()])?>" class="btn btn-default">Edit</a>
        <?php
        gen_delete_button(route('staff.deleteRecord',['id'=>$staff->getStaffId()]),'this will delete the '.$staff->getFirstName().' record');
        ?>
        <div data-staff-id="<?=$staff->getStaffId()?>" class="btn btn-default staff-archive-btn" data-toggle="modal" data-target="#staff-archive-modal">Archive</div>
    </th>
</tr>