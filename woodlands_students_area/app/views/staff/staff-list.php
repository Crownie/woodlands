<?php use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'User Profile']) ?>
<h2 class="page-header">List Staff</h2>

<p>
    <a href="<?=route('staff.showRecordForm')?>" class="btn btn-success pull-right">Create staff</a>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Staff ID</th>
        <th>First Name</th>
        <th>Surname</th>
        <th>Contact Phone</th>
        <th>Email</th>
        <th>Actions</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($staff as $row){
        $this->insert('staff/partials/staff-item',['staff'=>$row]);
    }
    ?>


    </tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="staff-archive-modal" tabindex="-1" role="dialog" aria-labelledby="staff-archive-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Archive Staff</h4>
            </div>
            <div class="modal-body">
                <form action="<?=route('staff.archive')?>" method="post">
                    <?php
                    echo csrf_input();
                    Form::hiddenInput('staff_id','');
                    Form::select('dormancy_reason','*Dormancy Reason','',[
                        'RETIRED'=>'RETIRED',
                        'RESIGNED'=>'RESIGNED',
                        'MISCONDUCT'=>'MISCONDUCT'
                    ]);
                    Form::row();
                    Form::button('Archive','btn btn-success');
                    Form::row();
                    ?>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>