<?php
$uid = generate_random_string();
?>
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
            <div class="col-sm-4">
                <div>
                    <?php
                    echo $personal_tutor->getFullName();
                    ?>
                </div>
            </div>
            <div class="col-sm-4">Number of tutees: <?= count($students) ?></div>
            <div class="btn btn-default btn-multiple-select-modal" data-title="Assign Students to <?=$personal_tutor->getFullName()?>" data-options-url="<?=route('personal-tutors.getStudentOptions',['id'=>$personal_tutor->getStaffId()]).'?department='.$department_id.'&level='.$lev?>" data-save-url="<?=route('personal-tutors.addStudentToPersonalTutor',['id'=>$personal_tutor->getStaffId()])?>">Assign to Student</div>
            <a class="pull-right btn btn-default accordion-toggle-btn" role="button" data-toggle="collapse"
               data-parent="#accordion" href="#collapse<?= $uid ?>"><i class="fa fa-caret-up"></i></a>
        </h4>
    </div>
    <div id="collapse<?= $uid ?>" class="panel-collapse collapse" aria-expanded="false" role="tabpanel"
         aria-labelledby="headingOne">
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Student id</th>
                    <th>First name</th>
                    <th>Surname</th>
                    <th>Course</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach ($students as $student) {
                    ?>
                    <tr>
                        <th><?= $student->getStudentId() ?></th>
                        <td><?= $student->getFirstName() ?></td>
                        <td><?= $student->getSurname() ?></td>
                        <?php
                        $course = $student->getCourse();
                        if ($course) {
                            ?>
                            <td><?= $course->getTitle() ?></td>
                            <?php
                        }else{
                            echo '<td>No course</td>';
                        }
                        ?>
                        <td>
                            <a href="<?=route('students.removePersonalTutor',['id'=>$student->getStudentId()])?>" class="btn btn-default">Remove</a>
                        </td>
                    </tr>
                    <?php

                }

                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
