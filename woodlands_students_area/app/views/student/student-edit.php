<?php
use app\models\Student;
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'View Student']);

?>


<h2 class="page-header">Update Contact Details</h2>




<form id="student-form" action="" method="POST">
    <h4>Contact Information</h4>
    <?php
    //////////////////////////////////////////////////////////////////////////////////////////////
    Form::input('phone', '*Contact Number', $student->getPhone());
    Form::input('email', '*Email Address', $student->getEmail());
    ?>
    <h4>Term-time Address</h4>
    <?php
    Form::input('term_time_address_line1', '*Address Line 1', $student->getTermTimeAddressLine1());
    Form::input('term_time_address_line2', 'Address Line 2', $student->getTermTimeAddressLine2());
    Form::input('term_time_city', '*City', $student->getTermTimeCity());
    Form::input('term_time_country', '*Country', $student->getTermTimeCountry());
    Form::input('term_time_postcode', '*Postcode', $student->getTermTimePostcode());
    Form::row();
    ?>
    <hr>
    <h4>Non Term-time Address</h4>
    <?php



    Form::checkbox('both_addresses_same','',$student->isAddressTheSame(),[
        'yes' => 'Same as term-time address'
    ],'col-sm-6 both_addresses_same');
    Form::row();

    Form::input('address_line1', '*Address Line 1', $student->getAddressLine1());
    Form::input('address_line2', 'Address Line 2', $student->getAddressLine2());
    Form::input('city', '*City', $student->getCity());
    Form::input('country', '*Country', $student->getCountry());
    Form::input('postcode', '*Postcode', $student->getPostcode());
    ?>
    <hr>
    <?php
    Form::button('Submit', 'btn btn-success');
    ?>
</form>