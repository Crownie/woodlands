<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Edit Course</h2>

<form action="<?=route('courses.updateRecord',['id'=>$id])?>" method="POST">
    <?php $this->insert('course/course-form',get_defined_vars()) ?>
</form>
