<?php
/**
 * Created by Tobi Ayilara.
 * Date: 05/02/2017
 * Time: 17:10
 */
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

Form::input('title', '*Course Title', $course->getTitle());
Form::input('ucas_points', 'UCAS Points', $course->getUcasPoints(), ['type' => 'number']);
Form::row();
Form::select('duration', '*Duration', $course->getDuration(), [
    '1' => '1 year',
    '2' => '2 years',
    '3' => '3 years',
    '4' => '4 years',
    '5' => '5 years']);
Form::moneyInput('cost', '*Cost', $course->getCost());
Form::row();

$years = [];
for ($i = date('Y'), $j = $i + 5; $i < $j; $i++) {
    $years['' . $i] = $i;
}
Form::select('start_year', '*Start Year', $course->getStartYear(), $years);

Form::textarea('description', 'Description', $course->getDescription());

Form::multipleSelect('course_modules','Course Modules',$selected_module_options,$module_options);

Form::button('Submit', 'btn-success');

//Form::checkbox('test_ch','Test Checkbox',['1','2'],['1'=>'Item 1', '2'=>'item 2']);
?>
