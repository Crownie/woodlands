<?php use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Courses</h2>

<p>
<a href="<?=route('courses.showRecordForm')?>" class="btn btn-success pull-right">Create Course</a>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Course Code</th>
        <th>Title</th>
        <th>Duration</th>
        <th>Cost</th>
        <th>Start Year</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($courses as $course){
        $this->insert('course/partials/course-item',['course'=>$course]);
    }
    ?>


    </tbody>
</table>



