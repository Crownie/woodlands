<tr data-id="<?=$course->getCourseId()?>">
    <th><?=$course->getCourseId()?></th>
    <td><?=$course->getTitle()?></td>
    <td><?=$course->getDuration()?> Years</td>
    <td><?=$course->getCost()?></td>
    <td><?=$course->getStartYear()?></td>
    <th>
        <a href="<?=route('courses.showRecord',['id'=>$course->getCourseId()])?>" class="btn btn-default">View</a>
        <a href="<?=route('courses.showRecordEditForm',['id'=>$course->getCourseId()])?>" class="btn btn-default">Edit</a>
        <div class="btn btn-default btn-multiple-select-modal" data-title="Assign Modules to <?=$course->getTitle()?>" data-options-url="<?=route('courses.getModuleOptions',['id'=>$course->getCourseId()])?>" data-save-url="<?=route('courses.addModulesToCourse',['id'=>$course->getCourseId()])?>">Assign to Module</div>
        <?php
            gen_delete_button(route('courses.deleteRecord',['id'=>$course->getCourseId()]),'this will delete the '.$course->getTitle().' course');
        ?>
        <div class="btn btn-default">Archive</div>
    </th>
</tr>