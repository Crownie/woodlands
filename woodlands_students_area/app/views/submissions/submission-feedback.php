<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Assignment Feedback</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Comments</h1>
    </div>
    <div class="panel-body">
        <dl>
            <p><?= $submission->getFeedback() ?></p>

    </div>
</div>
