<?php


function localDBConnection(){
    $server = 'localhost';
    $username = 'admin';
    $password = 'password';
    $schema = 'dev_woodlands';

    $dsn = 'mysql:dbname='.$schema.';host='.$server;

    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ];

    return new PDO($dsn,$username,$password,$options);
}

function liveDBConnection(){
    $server = 'goldsalem.com';
    $username = 'devsolutions';
    $password = 'devsolutions123';
    $schema = 'dev_woodlands';

    $dsn = 'mysql:dbname='.$schema.';host='.$server;

    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ];

    return new PDO($dsn,$username,$password,$options);
}

$pdo = localDBConnection();
