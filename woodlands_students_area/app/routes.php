<?php
/**
 * Created by Tobi Ayilara.
 * Date: 04/04/2017
 * Time: 10:14
 */
use app\Router;

/** @var $router Router */

$router->get('/',function(){
    return redirect('modules.showRecordList');
});

$router->get('/login','\\app\\Controllers\\AuthController::showLoginForm')->bind(unprotect('login.form'));

$router->post('/login','\\app\\Controllers\\AuthController::login')->bind(unprotect('login'));

$router->get('/logout','\\app\\Controllers\\AuthController::logout')->bind(unprotect('logout'));

$router->get('my-profile','\\app\\Controllers\\StudentController::showRecord')->bind('students.showRecord');
$router->get('my-profile/edit','\\app\\Controllers\\StudentController::showRecordEditForm')->bind('students.showRecordEditForm');
$router->post('my-profile/edit','\\app\\Controllers\\StudentController::submitRequest');

$router->get('announcements','\\app\\Controllers\\AnnouncementController::showRecordList')->bind('announcements.showRecordList');

$router->get('modules','\\app\\Controllers\\ModuleController::showRecordList')->bind('modules.showRecordList');
$router->get('module/{id}/announcements','\\app\\Controllers\\ModuleController::announcements')->bind('module.announcements');
$router->get('module/{id}/activities','\\app\\Controllers\\ModuleController::activities')->bind('module.activities');
$router->get('module/{id}/assessments','\\app\\Controllers\\ModuleController::assessments')->bind('module.assessments');
$router->get('module/{id}/grades','\\app\\Controllers\\ModuleController::grades')->bind('module.grades');
$router->get('module/{id}/about','\\app\\Controllers\\ModuleController::about')->bind('module.about');

$router->get('',function(){

})->bind('assignments.showRecordList');

$router->get('timetable','\\app\\Controllers\\TimetableController::showRecordList')->bind('timetable.showRecordList');
$router->get('timetable/get-events','\\app\\Controllers\\TimetableController::getEvents')->bind('timetable.getEvents');
$router->get('timetable/get-item-details','\\app\\Controllers\\TimetableController::getItemDetails')->bind('timetable.getItemDetails');

$router->get('',function(){

})->bind('attendance.showRecordList');

$router->get('',function(){

})->bind('diary.showRecordList');

$router->get('assignments','\\app\\Controllers\\AssignmentController::showRecordList')->bind('assignments.showRecordList');
$router->get('assignments/{id}','\\app\\Controllers\\AssignmentController::showRecord')->bind('assignments.showRecord');
$router->get('assignments/{id}/submit','\\app\\Controllers\\AssignmentController::showSubmissionForm')->bind('assignments.showSubmissionForm');
$router->post('assignments/{id}/submit','\\app\\Controllers\\AssignmentController::submitAssignment')->bind('assignments.submitAssignment');

$router->crud('\\app\\Controllers\\SubmissionController','submission');


