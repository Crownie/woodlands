$(function () {

    var $modal = $('#multiple-select-modal'),
        modalTitle = '',
        optionsUrl = '',
        saveUrl = '';

    function init(){
        $('#multiple-select').multiSelect({
            selectableHeader: '<p><strong>Unassigned</strong></p><input type="text" id="selectable-search" class="search-input form-control" autocomplete="off" placeholder="filter">',
            selectionHeader: '<p><strong>Assigned</strong></p><input type="text" id="selection-search" class="search-input form-control" autocomplete="off" placeholder="filter">',
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = $('#selectable-search'),
                    $selectionSearch = $('#selection-search'),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });
    }



    $modal.on('show.bs.modal', function (e) {

        init();


        $.get(optionsUrl,function(dataStr){
            var data = JSON.parse(dataStr);
            for(var i =0,j=data.length;i<j;i++){
                var item = data[i];
                $('#multiple-select').multiSelect('addOption', { value: item.id, text: item.text, index: 0 });
                if(item.hasOwnProperty('selected') && item.selected == true){
                    $('#multiple-select').multiSelect('select',item.id);
                }
            }

            $('#multiple-select').multiSelect('refresh');
        });

    });

    $('.btn-multiple-select-modal').click(function () {
        optionsUrl = $(this).data('options-url');
        saveUrl = $(this).data('save-url');
        modalTitle = $(this).data('title');
        $modal.find('.modal-title').text(modalTitle);
        $modal.modal('show');
    });

    $('.btn-multiple-select-modal-save').click(function(){
        $('#multiple-select').multiSelect('refresh');
        var selectedValues = $('#multiple-select').val();
        console.log(selectedValues);
        $.post(saveUrl,{'_token':csrf,'selected':selectedValues},function(response){
            if(response.length==0){
                //success
                toastr["success"]("changes was saved successfully");
                $modal.modal('hide');
                location.reload();
            }else{
                //error
                toastr["error"]("An unexpected error occurred while trying to save changes");
            }
        });
    });
});