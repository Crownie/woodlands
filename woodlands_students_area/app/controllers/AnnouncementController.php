<?php
/**
 * Created by Tobi Ayilara.
 * Date: 05/04/2017
 * Time: 20:16
 */

namespace app\controllers;


use app\models\Announcement;
use app\models\Student;
use app\utilities\database_utilities\DatabaseTable;

class AnnouncementController
{

    public function showRecordList()
    {
        $student = AuthController::getLoggedInStudent();
        $announcements = Announcement::getAnnouncementsForStudent($student->getStudentId());
        $date_read = self::getStudentDateReadAnnouncement($student->getStudentId());
        self::updateStudentDateReadAnnouncement($student->getStudentId());
        return view()->render('announcement/announcement', get_defined_vars());
    }

    public static function getStudentDateReadAnnouncement($student_id)
    {
        $pdo = DatabaseTable::getConnection();
        $result = $pdo->query('
SELECT date_read_announcement FROM students
WHERE student_id =' . $student_id);

        $s = $result->fetch();
        return $s['date_read_announcement'];
    }

    public static function updateStudentDateReadAnnouncement($student_id)
    {
        $pdo = DatabaseTable::getConnection();
        $result = $pdo->query('
UPDATE students SET date_read_announcement =\'' . date('Y-m-d H:i:s') . '\'
WHERE student_id =' . $student_id);
    }

    public static function getUnreadCount()
    {
        $student = AuthController::getLoggedInStudent();
        $date_read = self::getStudentDateReadAnnouncement($student->getStudentId());
        $announcements = Announcement::getAnnouncementsForStudent($student->getStudentId());
        $count = 0;

        foreach ($announcements as $announcement) {
            if ($announcement->getDatePosted() > $date_read) {
                $count++;
            }
        }
        if ($count == 0) {
            return '';
        }
        return $count;
    }

}