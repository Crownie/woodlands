<?php
/**
 * Created by Alex
 */

namespace app\controllers;


use app\models\Department;
use app\models\PersonalTutor;
use app\models\PersonalTutorial;
use app\models\Staff;
use app\models\Student;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;


class PersonalTutorController extends CRUDController
{
    private $personalTutorialsTable;

    public function __construct()
    {

    }

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $lev = $_GET['level']; //note: lev was used as variable name to avoid collision with template generator
        $department_id = $_GET['department'];
        $personal_tutors = PersonalTutor::find('department_id = :department_id',['department_id'=>$department_id]);
        //dd($personal_tutors);
        return view()->render('personal-tutor/personal-tutor', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        //$personalTutors = new PersonalTutorial([]);
        //return view()->render('personal-tutors/',['personal-tutors'=>$personalTutors]);
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        // TODO: Implement showRecord() method.
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement showRecordEditForm() method.
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        // TODO: Implement storeRecord() method.
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement updateRecord() method.
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement deleteRecord() method.
    }

    public function getStudentOptions(Request $request,$id){

        $lev = $_GET['level']; //note: lev was used as variable name to avoid collision with template generator
        $department_id = $_GET['department'];

        $students = Student::getStudentsByDeptAndLevel($department_id,$lev,null);

        $student_options=[];

        foreach ($students as $student){
            /* @var $student Student*/
            $student_options[] =
                [
                    'id'=>$student->getStudentId(),
                    'text'=>$student->getStudentId().' - '.$student->getFullName()
                ];
        }

        return json_encode($student_options);
    }

    public function addStudentToPersonalTutor(Request $request,$id){
        $data = get_post_array($request);
        $student_ids = $data['selected'];
        foreach ($student_ids as $student_id){
            $student = Student::get($student_id);
            $student->setPersonalTutorId($id);
            try{
                $student->save();
                notify_success('changes was saved successfully');
            }catch(Exception $exception){

            }
        }
        return '';
    }

    public static function getDepartmentOptions(){
        $departments = Department::getAll();
        $department_options = [];
        foreach ($departments as $department){
            $department_options [$department->getDepartmentId()] = $department->getName();
        }
        return $department_options;
    }
}