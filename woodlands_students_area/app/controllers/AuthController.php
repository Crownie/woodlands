<?php
/**
 * Created by Tobi Ayilara.
 * Date: 06/02/2017
 * Time: 08:39
 */

namespace app\controllers;


use app\models\Staff;
use app\models\Student;
use Symfony\Component\HttpFoundation\Request;

class AuthController
{
    /**
     * Check if user is logged. redirect if user is not logged in. return null if logged in
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public static function authenticate()
    {
        //TODO: check if the user is logged in
        if(self::getLoggedInStudent() == null){
            return redirect('login.form');
        }
    }

    public function showLoginForm(){
        return view()->render('auth/login');
    }

    public function login(Request $request){
        //TODO: implement the authentication
        $data = get_post_array($request);
        try{
            $student = Student::get($data['username']);
            if(!password_verify($data['password'],$student->getPassword())){
                throw new \Exception('wrong password');
            }
            session()->set('student_id',$student->getStudentId());
            notify_success('Welcome back '.$student->getFirstName());
            return redirect_url('/');
        }catch(\Exception $exception){
            notify_error('Incorrect username or password');
            return redirect('login');
        }
    }

    public function logout(){
        //TODO: remove the user from session
        session()->remove('staff_id');
        notify_success('You are now logged out');
        return redirect('login.form');
    }

    public static function getLoggedInStaff(){
        $staff_id = session()->get('staff_id');
        $staff = null;

        try{
            $staff = Staff::get($staff_id);
        }catch (\Exception $exception){

        }
        return $staff;
    }

    public static function getLoggedInStudent(){
        $student_id = session()->get('student_id');
        $student = null;

        try{
            $student = Student::get($student_id);
        }catch (\Exception $exception){

        }
        return $student;
    }

}