<?php
/**
 * Created by PhpStorm.
 * User: Tomas Kompas
 * Date: 27/02/2017
 * Time: 14:35
 */

namespace app\controllers;


use app\utilities\database_utilities\DatabaseTable;
use Symfony\Component\HttpFoundation\Request;
use app\utilities\validator\Validator;
use app\models\Qualification;

class QualificationController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        // TODO: Implement showRecordList() method.
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        // TODO: Implement showRecordForm() method.
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        // TODO: Implement showRecord() method.
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement showRecordEditForm() method.
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        // TODO: Implement storeRecord() method.
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement updateRecord() method.
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement deleteRecord() method.
    }
}