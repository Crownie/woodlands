<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 20/02/2017
 * Time: 15:49
 */

namespace app\controllers;


use app\models\Announcement;
use app\models\Assignment;
use app\models\Course;
use app\models\CourseModule;
use app\models\Module;
use app\models\Student;
use app\utilities\database_utilities\DatabaseTable;
use Symfony\Component\HttpFoundation\Request;

class ModuleController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $student = AuthController::getLoggedInStudent();
        $course_modules = CourseModule::find('course_id = :course_id',['course_id'=>$student->getCourseId()]);

        return view()->render('module/module-list', get_defined_vars());
    }

    public function about($id)
    {
        $module = Module::get($id);
        return view()->render('module/module-about', get_defined_vars());
    }


    public function grades($id)
    {
        $module = Module::get($id);
        return view()->render('module/module-grades', get_defined_vars());
    }

    public function assessments($id)
    {
        $module = Module::get($id);
        $assignments = Assignment::find('module_id=:module_id',['module_id'=>$id]);
        return view()->render('module/module-assessments', get_defined_vars());
    }

    public function activities($id)
    {
        $module = Module::get($id);
        return view()->render('module/module-activities', get_defined_vars());
    }

    public function announcements($id)
    {

        $module = Module::get($id);
        $student = AuthController::getLoggedInStudent();
        $announcements = Announcement::getAnnouncementsForStudent($student->getStudentId());
        return view()->render('module/module-announcements', get_defined_vars());
    }


}