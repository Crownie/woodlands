<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\Controllers;

use app\models\Assignment;
use app\models\AssignmentFile;
use app\models\Submission;
use Symfony\Component\HttpFoundation\Request;

class AssignmentController
{

    public function showRecordList(Request $request)
    {

        $assignments = Assignment::getAll();

        return view()->render('assignment/assignment-list', get_defined_vars());
    }

    public function showRecord(Request $request, $id)
    {
        $student = AuthController::getLoggedInStudent();

        $assignment = Assignment::get($id);
        $assignment_files = AssignmentFile::find('assignment_id = :assignment_id',['assignment_id'=>$assignment->getAssignmentId()]);
        $submissions = Submission::find('student_id=:student_id AND assignment_id=:assignment_id',[
            'student_id'=>$student->getStudentId(),
            'assignment_id'=>$id
        ]);
        return view()->render('assignment/assignment-view', get_defined_vars());
    }

    public function showSubmissionForm(Request $request, $id)
    {
        $assignment = Assignment::get($id);
        return view()->render('assignment/assignment-submit', ['assignment'=>$assignment, 'id'=>$id]);
    }


    public function submitAssignment(Request $request,$id)
    {
        $student = AuthController::getLoggedInStudent();
        $data = get_post_array($request);

        $data['student_id'] =$student->getStudentId();
        $data['assignment_id'] = $id;
        $submission = new Submission($data);

        $upload_dir = __DIR__."/../uploads/";


        if(isset($_FILES['submission_file'])){

            $fileDestination= $upload_dir.basename($_FILES['submission_file']['name']);
            if (is_dir($upload_dir) && is_writable($upload_dir)) {
                move_uploaded_file($_FILES['submission_file']['tmp_name'],$fileDestination);
                $submission->setFileUrl('//students.wuc.ac.uk/app/uploads/'.$_FILES['submission_file']['name']);
                $submission->setDateSubmitted(date('Y-m-d H:i:s'));
                $submission->insert();
                notify_success('Assignment submitted successfully');
                return redirect('assignments.showRecord',['id'=>$id]);
            } else {
                notify_error('folder doesnt exist');
                return redirect('assignments.showSubmissionForm',['id'=>$id]);
            }

        }else{
            notify_error('Sorry :\'( The file is too big or corrupt');
            return redirect('assignments.showSubmissionForm',['id'=>$id]);
        }

    }



}