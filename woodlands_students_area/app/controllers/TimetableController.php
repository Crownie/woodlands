<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\controllers;

use app\models\Student;
use app\models\StudentModuleGroup;
use app\models\Timetable;
use app\models\TimetableItem;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;


class TimetableController
{


    public function __construct()
    {

    }

    public function showRecordList(Request $request)
    {
        /* @var $student Student*/
        $student = AuthController::getLoggedInStudent();

        $criteria = [
            'course_id'=>$student->getCourseId(),
            'level'=>$student->getLevel(),
            'year'=>$student->getCurrentYear()
        ];

        $timetableItems = [];
        $timetables = Timetable::find('course_id = :course_id AND level =:level AND year = :year',$criteria);

        if(count($timetables)>0){
            $timetable = $timetables[0];
        }
        return view()->render('timetable/timetable-list', get_defined_vars());
    }


    public function getEvents(Request $request)
    {
        /* @var $student Student*/
        $student = AuthController::getLoggedInStudent();

        $criteria = [
            'course_id'=>$student->getCourseId(),
            'level'=>$student->getLevel(),
            'year'=>$student->getCurrentYear()
        ];

        $timetableItems = [];
        $timetables = Timetable::find('course_id = :course_id AND level =:level AND year = :year',$criteria);

        if(count($timetables)>0){
            $timetable = $timetables[0];
            $timetableItems = TimetableItem::find('timetable_id = :timetable_id',['timetable_id'=>$timetable->getTimetableId()]);
        }

        $student_module_groups = StudentModuleGroup::find('student_id = :student_id',[
            'student_id'=>$student->getStudentId()
        ]);


        $timetableItemArrs = [];

        foreach ($timetableItems as $timetableItem){
            /* @var $timetableItem TimetableItem*/
            $module = $timetableItem->getModule();

            //TODO: filter out only timetable for the group the student belongs to
            if($timetableItem->getModuleGroupNumber() == '0' || $this->inModuleGroup($student_module_groups,$timetableItem->getModuleId(),$timetableItem->getModuleGroupNumber())){
                $module_code = $module->getModuleCode();
                $module_group_number = $timetableItem->getModuleGroupNumber();
                $room_number = $timetableItem->getRoom()->getRoomNumber();
                $timetableItemArr = $timetableItem->toArray();
                $timetableItemArr['module_code']=$module_code;
                $timetableItemArr['title'] = view()->render('timetable/partials/time-entry',get_defined_vars());
                $timetableItemArrs[] = $timetableItemArr;
            }


        }
        //var_dump($data);

        return json_encode($timetableItemArrs);

    }

    private function inModuleGroup($student_module_groups,$module_id,$group_number){
        foreach ($student_module_groups as $student_module_group){
            if($module_id==$student_module_group->getModuleId() && $group_number == $student_module_group->getGroupNumber()){

                return true;
            }
        }
        return false;
    }

    public function addItem(Request $request,$id){
       $data = get_post_array($request);
        $timetable_item = new TimetableItem($data);
        $validator = $timetable_item->getValidator();
        if($validator->isValid()){
            $timetable_item->save();
        }else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
        }
        return redirect('timetables.showRecordEditForm',['id'=>$id]);
    }

    public function getItemDetails(Request $request){
        $item_id = $_GET['item-id'];
        $timetable_item = TimetableItem::get($item_id);

        return view()->render('timetable/partials/timetable-item-view',get_defined_vars());
    }

    public function updateItem(Request $request,$id){
        $data = get_post_array($request);
        $timetable_item = TimetableItem::get($id);
        $timetable_item->updateAttributes($data);
        $validator = $timetable_item->getValidator();
        if($validator->isValid()){
            $timetable_item->save();
            notify_success('Updated successfully');
        }else{
            notify_error('an error occurred when trying to update the timetable item');

        }
        return redirect_back();
    }

    public function deleteItem(Request $request,$id){
        $timetable_item = TimetableItem::get($id);
        $timetable_item->delete();
        notify_success('Timetable item deleted successfully');
        return redirect_back();
    }

    public function archive(Request $request,$id){
        $timetable = Timetable::get($id);
        $timetable->setStatus('DORMANT');
        $timetable->save();
        notify_success('Timetable was archived successfully');
        return redirect_back();
    }
}