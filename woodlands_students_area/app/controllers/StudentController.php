<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 20/02/2017
 * Time: 15:51
 */

namespace app\controllers;


use app\models\DetailsChangeRequest;
use app\models\Qualification;
use app\models\Student;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class StudentController
{
    public function showRecord()
    {
        $student = AuthController::getLoggedInStudent();
        return view()->render('student/student-view', get_defined_vars());
    }

    public function showRecordEditForm()
    {
        $student = AuthController::getLoggedInStudent();
        return view()->render('student/student-edit', get_defined_vars());
    }

    public function submitRequest(Request $request)
    {
        $data = get_post_array($request);
        unset($data['both_addresses_same']);
        $student = AuthController::getLoggedInStudent();

        $student_data = $student->toArray();
        $something_changed = false;
        foreach ($data as $key => $value) {
            if (isset($student_data[$key])) {

                if ($value !== $student_data[$key]) {

                    $something_changed = true;
                    break;
                }
            }
        }


        if ($something_changed) {
            $json_string = json_encode($data);
            $dc_request = new DetailsChangeRequest();
            $dc_request->setStudentId($student->getStudentId());
            $dc_request->setStatus('OPEN');
            $dc_request->setChangedData($json_string);
            $dc_request->save();
            notify_success('The changes has been forward to the Admin for review');
            return redirect('students.showRecord');
        } else {
            notify_error('no field was changed');
            return redirect('students.showRecordEditForm');
        }
    }
}