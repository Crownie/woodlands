<?php
/**
 * Created by Tobi Ayilara.
 * Date: 04/02/2017
 * Time: 22:13
 */

namespace app\controllers;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

abstract class CRUDController
{
    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public abstract function showRecordList(Request $request);

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public abstract function showRecordForm(Request $request);

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public abstract function showRecord(Request $request, $id);

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public abstract function showRecordEditForm(Request $request, $id);

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public abstract function storeRecord(Request $request);

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public abstract function updateRecord(Request $request, $id);

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public abstract function deleteRecord(Request $request, $id);

}