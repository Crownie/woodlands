<?php
/**
 * Created by Tobi Ayilara.
 * Date: 08/03/2017
 * Time: 16:17
 */

namespace app\controllers;


use app\models\Event;
use Symfony\Component\HttpFoundation\Request;

class DiaryController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        return view()->render('diary/diary-list');
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $event = new Event();

        $course_options = CourseController::getCourseOptions();

        return view()->render('diary/diary-create',get_defined_vars());
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        // TODO: Implement showRecord() method.
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement showRecordEditForm() method.
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);
        $data['start_date'] = date('Y-m-d H:i:s',strtotime($data['start_date']));
        $data['end_date'] = date('Y-m-d H:i:s',strtotime($data['end_date']));

        $event = new Event($data);
        $validator = $event->getValidator();


        if($validator->isValid()){
        $event->save();
        //dd($event);
        notify_success('Event was created successfully');
        return redirect('diary.showRecordList');

        } else {
            $errors = $validator->getErrors();
            notify_error('Event was not successful');
            show_form_errors($errors);
            return redirect('diary.showRecordForm');

        }

        //dd($data);
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement updateRecord() method.
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement deleteRecord() method.
    }

    public function getEvents(Request $request)
    {
        $events = Event::getAll();
        $eventArrs = [];
        foreach ($events as $event){
            $eventArr = $event->toArray();
            $eventArr['start'] = $eventArr['start_date'];
            $eventArrs[] = $eventArr;
        }
        //var_dump($data);

        return json_encode($eventArrs);

    }
}