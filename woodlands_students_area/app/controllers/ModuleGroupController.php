<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class ModuleGroupController extends CRUDController
{

    /**
     * Displays the record list
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        // TODO: Implement index() method.
    }

    /**
     * Displays the form for creating new record
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        // TODO: Implement create() method.
    }

    /**
     * Displays a single record specified by the id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        // TODO: Implement show() method.
    }

    /**
     * Displays the form for editing a record
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement edit() method.
    }

    /**
     * Stores a new record
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        // TODO: Implement store() method.
    }

    /**
     * Updates the record specified by the id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement update() method.
    }

    /**
     * Deletes the record specified by the id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement delete() method.
    }
}