<?php
/**
 * Created by Tobi Ayilara.
 * Date: 04/02/2017
 * Time: 20:03
 */


use app\StudentsApp;

require_once __DIR__ . '/../autoload.php';
require_once  __DIR__ . '/helpers/helper_functions.php';

StudentsApp::main();