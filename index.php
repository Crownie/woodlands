
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Woodlands University College</title>

    <!-- Bootstrap core CSS -->
    <link href="woodlands_rms/app/assets/css/style.min.css" rel="stylesheet">

    <style>
        body{
            padding-top: 132px;
        }
        .navbar-brand,.navbar-brand:hover{
            height: 87px;
            margin-top: -21px;
            margin-bottom: 24px;
            background-color:transparent !important;
            border:none;
        }
    </style>

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Static navbar -->
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="http://students.wuc.ac.uk">Student Area</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <div class="logo"></div>
                <span class="clearfix"></span>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="search" placeholder="Search" class="form-control">
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Welcome to WUC</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur consequatur earum eligendi, ipsa
            laboriosam maiores modi nesciunt nobis! Commodi doloremque est exercitationem facere nobis nostrum
            similique, totam vitae. Animi, laudantium.</p>
        <p><a class="btn btn-success btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-4">
            <h2>News</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, et facere facilis iste iusto libero
                officia quidem reprehenderit saepe voluptates! Adipisci corporis quaerat quia? Aperiam commodi itaque
                labore nihil voluptatibus.</p>
            <p><a class="btn btn-default" href="#" role="button">Read more &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Courses</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, et facere facilis iste iusto libero
                officia q</p>
            <form class="form-inline">
                <div class="form-group">
                    <input type="search" placeholder="Search" class="form-control">
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
            </form>
            <br>
            <p><a class="btn btn-default" href="#" role="button">Browse all courses &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Open Days</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, et facere facilis iste iusto libero
                officia quidem reprehenderit saepe voluptates! Adipisci corporis quaerat quia? Aperiam commodi itaque
                labore nihil voluptatibus.</p>
            <p><a class="btn btn-default" href="#" role="button">Learn more &raquo;</a></p>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; <?=date('Y')?> Woodlands University College.</p>
    </footer>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="woodlands_rms/app/assets/js/jquery.min.js"><\/script>')</script>
</body>
</html>
