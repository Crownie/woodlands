<?php
/**
 * Created by Tobi Ayilara.
 * Date: 04/02/2017
 * Time: 23:06
 */
use app\models\Student;
use app\RecordManagementSystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;
use app\utilities\Notifier;


function send_cause_for_concern(Student $student, $issue)
{
    $subject = "Private & Confidential";
    $to = $student->getEmail();

    $message = view()->render('email/cause-for-concern', get_defined_vars());

    send_email($to,$subject,$message);
}

function send_email($to,$subject,$message){
    $from = new SendGrid\Email(null, "admin@wuc.ac.uk");
    $to = new SendGrid\Email(null, $to);

    $content = new SendGrid\Content("text/html", $message);
    $mail = new SendGrid\Mail($from, $subject, $to, $content);

    $apiKey = SENDGRID_API_KEY;
    $sg = new \SendGrid($apiKey);

    $response = $sg->client->mail()->send()->post($mail);
}

function sidebarMenuItem($title, $url, $icon, $submenu = [])
{
    $currentPageUrl = get_current_route();
    $collapsedClass = 'collapsed';
    $activeClass = '';
    $hasSub = count($submenu) > 0;

    foreach ($submenu as $item) {
        if ($currentPageUrl == remove_url_params($item['url'])) {
            $collapsedClass = '';
            break;
        }
    }

    if ($currentPageUrl == $url) {
        $collapsedClass = '';
        if (!$hasSub) {
            $activeClass = 'active';
        }
    }

    echo '<li class="' . $collapsedClass . ' ' . $activeClass . '">
    <a href="' . $url . '"> <i class="fa fa-' . $icon . '"></i> ' . $title;
    if ($hasSub) {
        echo '<i class="pull-right fa fa-plus"></i><i class="pull-right fa fa-minus"></i>';
    }
    echo '</a>';
    if ($hasSub) {
        echo '<ul class="nav">';
        foreach ($submenu as $item) {
            echo '<li class="' . ((remove_url_params($item['url']) == $currentPageUrl) ? 'active' : '') . '"><a href="' . $item['url'] . '">' . $item['title'] . '</a></li>';
        }
        echo '</ul>';
    }
    echo '</li>';
}

function remove_url_params($url)
{
    $arr = explode('?', $url);
    return $arr[0];
}


function asset($url)
{
    return BASE_URL . '/app/assets/' . $url;
}

function script($url)
{
    echo '<script src="' . asset($url) . '"></script>';
}

function style($url, $media = 'all')
{
    echo '<link rel="stylesheet" href="' . asset($url) . '" media="' . $media . '" />';
}

function view()
{
    $templates = new League\Plates\Engine(__DIR__ . '/../views');
    return $templates;
}

/**
 * @param $name
 * @param array $parameters
 * @return string
 */
function route($name, $parameters = array())
{
    return RecordManagementSystem::getRouter()->generateUrl($name, $parameters);
}

function redirect($name, $parameters = array())
{
    return redirect_url(route($name, $parameters));
}

function redirect_back()
{
    return redirect_url($_SERVER['HTTP_REFERER']);
}

function redirect_url($url)
{
    return RecordManagementSystem::getRouter()->redirect($url);
}

function get_current_route()
{
    return RecordManagementSystem::getRouter()->getCurrentUrl();
}

function is_current_route($url)
{
    return $url == get_current_route();
}

function notify_error($message)
{
    Notifier::error($message);
}

function notify_success($message)
{
    Notifier::success($message);
}

function display_notification()
{
    Notifier::display();
}

/**
 * @return \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
 */
function flash()
{
    return session()->getFlashBag();
}

function dd($var)
{
    var_dump($var);
    die();
}

function get_post_array(\Symfony\Component\HttpFoundation\Request $request)
{
    return $request->request->getIterator()->getArrayCopy();
}

/**
 *
 * @param $field_name
 * @param string $default can be an instance of the a model object or a string
 * @return mixed|string the value of the field or the previous value that was sent
 */
function form_data($field_name, $default = '')
{
    $value = '';
    $form_data = session()->has('form_data') ? session()->get('form_data') : [];

    if (is_array($form_data) && isset($form_data[$field_name])) {
        //if old value is in session
        $value = $form_data[$field_name];
        unset($form_data[$field_name]);
        session()->set('form_data', $form_data);
    } else {
        //use provided value or get it from provided object
        if (is_object($default)) {
            try {
                $value = $default->$field_name;
            } catch (Exception $e) {

            }
        } else {
            $value = $default;
        }
    }
    return $value;
}

/**
 * @return \Symfony\Component\HttpFoundation\Session\Session
 */
function session()
{
    return RecordManagementSystem::getRouter()->getSession();
}

function csrf_token()
{
    $token = RecordManagementSystem::getRouter()->getTokenManager()->getToken('woodlands');
    return $token;
}

function csrf_input()
{
    $token = csrf_token();
    return '<input type="hidden" name="_token" value="' . $token . '">';
}

function is_csrf_token_valid($token)
{
    return RecordManagementSystem::getRouter()->getTokenManager()->isTokenValid(new CsrfToken('woodlands', $token));
}

function unprotect($routeName)
{
    global $unprotectedRoutes;
    if (empty($unprotectedRoutes)) {
        $unprotectedRoutes = [];
    }
    $unprotectedRoutes[] = $routeName;
    return $routeName;
}

function is_route_protected(Request $request)
{
    global $unprotectedRoutes;
    $currentRouteName = $request->attributes->get('_route');
    if ($unprotectedRoutes) {
        $urns = $unprotectedRoutes;

        foreach ($urns as $urn) {
            if ($urn == $currentRouteName) {
                return false;
            }
        }
    }

    return true;
}

function show_form_errors($errors)
{
    notify_error('There was an error in the form!');
    session()->set('form_errors', $errors);
}

function gen_delete_button($url, $warningCaption)
{
    echo '<div class="btn btn-danger btn-delete-warn" data-route="' . $url . '" data-warning-caption="' . $warningCaption . '" >Delete</div>';
}

function generate_random_string($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
    //REFERENCE: http://stackoverflow.com/a/4356295
}