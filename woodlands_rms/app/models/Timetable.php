<?php

namespace app\models;
use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;


/**
 * Created by PhpStorm.
 * User: mahbu
 * Date: 15/02/2017
 * Time: 17:11
 */

class Timetable extends Model
{
    protected $timetable_id;
    protected $course_id;
    protected $year;
    protected $level;

    /**
     * @return mixed
     */
    public function getTimetableId()
    {
        return $this->timetable_id;
    }

    /**
     * @param mixed $timetable_id
     * @return Timetable
     */
    public function setTimetableId($timetable_id)
    {
        $this->timetable_id = $timetable_id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return Timetable
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @param mixed $course_id
     * @return Timetable
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
        return $this;
    }

    public function getTitle(){
        $course = Course::get($this->course_id);

        return $course->getTitle(). ' - Level '.$this->level.' - '.$this->year;
    }



    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('title')->required();
        return $validator;
    }

    public static function getTable(){
        return new DatabaseTable('timetables',Timetable::class,'timetable_id');
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }


}