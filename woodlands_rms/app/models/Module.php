<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 15/02/2017
 * Time: 15:26
 */

namespace app\models;

use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

Class Module extends Model
{
    protected $module_id;
    protected $module_code;
    protected $level;
    protected $credits;
    protected $title;
    protected $ass1;
    protected $ass2;
    protected $exam;
    protected $status;

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return Module
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param mixed $credits
     * @return Module
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Module
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAss1()
    {
        return $this->ass1;
    }

    /**
     * @param mixed $ass1
     * @return Module
     */
    public function setAss1($ass1)
    {
        $this->ass1 = $ass1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAss2()
    {
        return $this->ass2;
    }

    /**
     * @param mixed $ass2
     * @return Module
     */
    public function setAss2($ass2)
    {
        $this->ass2 = $ass2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * @param mixed $exam
     * @return Module
     */
    public function setExam($exam)
    {
        $this->exam = $exam;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getModuleCode()
    {
        return $this->module_code;
    }

    /**
     * @param mixed $module_code
     * @return Module
     */
    public function setModuleCode($module_code)
    {
        $this->module_code = $module_code;
        return $this;
    }

    public function getModuleLeader(){
        $module_leaders = ModuleLeader::find('module_id = :module_id',['module_id'=>$this->module_id]);
        if(count($module_leaders)){
            $module_leader = $module_leaders[0];
            $staff = $module_leader->getStaff();
            return $staff;
        }
        return null;
    }

    public static function getTable(){
        return new DatabaseTable('modules',Module::class,'module_id');
    }

    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('module_code')->required();
        $validator->startRulesFor('level')->required();
        $validator->startRulesFor('ass1')->required()->numeric();
        $validator->startRulesFor('ass2')->required()->numeric();
        $validator->startRulesFor('exam')->required()->numeric();
        return $validator;
    }

    public function delete(){
        $table = static::getTable();
        $id_column_name = $table->getIdColumnName();
        $table->delete('module_id = :id',['id'=>$this->$id_column_name]);
    }

    public static function search($keyword,$status){
        $pdo = DatabaseTable::getConnection();

        $criteria = [
            'search' => '%' . $keyword . '%',
            'keyword'=>$keyword,
            'status'=>$status
        ];


        $stmt = $pdo->prepare('
        SELECT * FROM modules 
						WHERE
                        (module_code =:keyword OR 
                        title LIKE :search)
                         AND status = :status;
        ');

        $stmt->execute($criteria);

        $staff = DatabaseTable::getObjectArray(static::class,$stmt);
        return $staff;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}