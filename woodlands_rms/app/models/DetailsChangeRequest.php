<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 11:39
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class DetailsChangeRequest extends Model
{
    protected $student_id;
    protected $changed_data;
    protected $status;

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
    }

    /**
     * @return mixed
     */
    public function getChangedData()
    {
        return $this->changed_data;
    }

    /**
     * @param mixed $changed_data
     */
    public function setChangedData($changed_data)
    {
        $this->changed_data = $changed_data;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getValidator()
    {
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('changed_data')->required();
        $validator->startRulesFor('status')->required();
        return $validator;
    }




    public static function getTable(){
        return new DatabaseTable('details_change_requests',static::class,'student_id');
    }

}