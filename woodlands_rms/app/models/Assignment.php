<?php
namespace app\models;

use app\models\Model;
use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

/**
 * Created by PhpStorm.
 * User: mahbu
 * Date: 15/02/2017
 * Time: 17:14
 */

class Assignment extends Model
{
    protected $start_date;
    protected $due_date;
    protected $title;
    protected $description;
    protected $module_id;
    protected $assignment_id;

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     * @return Assignment
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->due_date;
    }

    /**
     * @param mixed $due_date
     * @return Assignment
     */
    public function setDueDate($due_date)
    {
        $this->due_date = $due_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Assignment
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Assignment
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_code
     * @return Assignment
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssignmentId()
    {
        return $this->assignment_id;
    }

    /**
     * @param mixed $assignment_id
     * @return Assignment
     */
    public function setAssignmentId($assignment_id)
    {
        $this->assignment_id = $assignment_id;
        return $this;
    }

    /**
     * @return Module
     */
    public function getModule(){
        return Module::get($this->module_id);
    }

    public static function getTable(){
        return new DatabaseTable('assignments',Assignment::class,'assignment_id');
    }

    public function getValidator()
    {
        $validator = new Validator($this->toArray());
        return $validator;
    }


}