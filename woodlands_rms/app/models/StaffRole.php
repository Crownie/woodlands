<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\models;

use app\utilities\database_utilities\DatabaseTable;

class StaffRole extends Model
{
    protected $role_id;
    protected $role;
    protected $staff_id;

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @param mixed $role_id
     * @return StaffRole
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     * @return StaffRole
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStaffId()
    {
        return $this->staff_id;
    }

    /**
     * @param mixed $staff_id
     * @return StaffRole
     */
    public function setStaffId($staff_id)
    {
        $this->staff_id = $staff_id;
        return $this;
    }
    public static function getTable()
    {
        return new DatabaseTable('staff_roles', StaffRole::class, 'role_id');
    }
    public static function clearStaffRoles($staff_id){
        $table = static::getTable();
        $table->delete('staff_id = :staff_id',['staff_id'=>$staff_id]);
    }

}