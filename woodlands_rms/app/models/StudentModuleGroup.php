<?php
/**
 Created by Alex
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

class StudentModuleGroup extends Model
{
    protected $students_module_group_id;
    protected $module_id;
    protected $student_id;
    protected $group_number;

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return StudentModuleGroup
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_id
     * @return StudentModuleGroup
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupNumber()
    {
        return $this->group_number;
    }

    /**
     * @param mixed $group_number
     * @return StudentModuleGroup
     */
    public function setGroupNumber($group_number)
    {
        $this->group_number = $group_number;
        return $this;
    }

    public static function getTable()
    {
        return new DatabaseTable('student_module_groups', StudentModuleGroup::class, 'students_module_group_id');
    }


    public function getStudent(){
        return Student::get($this->student_id);
    }

    /**
     * @param $module_id
     * @param $group_number
     * @param $level
     * @return Student
     */
    public static function getStudentsByGroupAndLevel($module_id, $group_number){
        $pdo = DatabaseTable::getConnection();

        $stmt = $pdo->prepare('
            SELECT s.student_id, s.first_name, s.surname, s.course_id FROM student_module_groups smg
JOIN students s
ON s.student_id = smg.student_id
JOIN modules m
ON m.module_id = smg.module_id
WHERE s.status=\'LIVE\' AND s.level = m.level AND smg.module_id = :module_id AND smg.group_number = :group_number;
        ');
        $criteria = [
            'module_id'=>$module_id,
            'group_number'=>$group_number
        ];
        $stmt->execute($criteria);

        $students = DatabaseTable::getObjectArray(Student::class,$stmt);
        return $students;
    }

}