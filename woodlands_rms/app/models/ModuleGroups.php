<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\models;

class ModuleGroups extends Model
{
    protected $module_group_id;
    protected $level;
    protected $year;
    protected $group_number;
    protected $modules_module_code;

    /**
     * @return mixed
     */
    public function getModuleGroupId()
    {
        return $this->module_group_id;
    }

    /**
     * @param mixed $module_group_id
     * @return ModuleGroups
     */
    public function setModuleGroupId($module_group_id)
    {
        $this->module_group_id = $module_group_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return ModuleGroups
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return ModuleGroups
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupNumber()
    {
        return $this->group_number;
    }

    /**
     * @param mixed $group_number
     * @return ModuleGroups
     */
    public function setGroupNumber($group_number)
    {
        $this->group_number = $group_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModulesModuleCode()
    {
        return $this->modules_module_code;
    }

    /**
     * @param mixed $modules_module_code
     * @return ModuleGroups
     */
    public function setModulesModuleCode($modules_module_code)
    {
        $this->modules_module_code = $modules_module_code;
        return $this;
    }


}
