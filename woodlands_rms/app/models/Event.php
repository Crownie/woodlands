<?php
/**
 * Created by Tobi Ayilara.
 * Date: 09/03/2017
 * Time: 22:38
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

class Event extends Model
{
    protected $event_id;
    protected $title;
    protected $start_date;
    protected $end_date;
    protected $location;
    protected $description;
    protected $student_id;
    protected $staff_id;


    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @param mixed $event_id
     * @return Event
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     * @return Event
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     * @return Event
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return Event
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     * @return Event
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStaffId()
    {
        return $this->staff_id;
    }

    /**
     * @param mixed $staff_id
     * @return Event
     */
    public function setStaffId($staff_id)
    {
        $this->staff_id = $staff_id;
        return $this;
    }


    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('title')->required();
        $validator->startRulesFor('start_date')->required();
        $validator->startRulesFor('end_date')->required();
        $validator->startRulesFor('location')->required();
        $validator->startRulesFor('description')->required();
        return $validator;
    }

    public static function getTable(){
        return new DatabaseTable('events',Event::class,'event_id');
    }


}