<?php
/**
 * Created by Alex.
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

class ModuleLeader extends Model
{
    protected $staff_id;
    protected $module_id;

    /**
     * @return mixed
     */
    public function getStaffId()
    {
        return $this->staff_id;
    }

    /**
     * @param mixed $staff_id
     * @return ModuleLeader
     */
    public function setStaffId($staff_id)
    {
        $this->staff_id = $staff_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * @param mixed $module_code
     * @return ModuleLeader
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
        return $this;
    }

    public function getStaff(){
        return Staff::get($this->staff_id);
    }

    public static function getTable(){
        return new DatabaseTable('module_leaders',static::class,'module_leader_id');
    }
    public static function clearModuleLeader($module_id){
        $table = static::getTable();

        $table->delete('module_id = :module_id',['module_id'=>$module_id]);
    }
}