<?php
namespace app\models;

use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;

Class Staff extends Model
{
    protected $staff_id;
    protected $status;
    protected $dormancy_reason;
    protected $first_name;
    protected $middle_name;
    protected $surname;
    protected $address_line1;
    protected $address_line2;
    protected $city;
    protected $country;
    protected $postcode;
    protected $phone;
    protected $email;
    protected $password;
    
    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = [];
        $staff_roles = StaffRole::find('staff_id = :staff_id',['staff_id'=>$this->staff_id]);
        foreach ($staff_roles as $staff_role){
            $roles[] = $staff_role->getRole();
        }

        return $roles;
    }




    public function getStaffId()
    {
        return $this->staff_id;
    }

    /**
     * @param mixed $staff_id
     */
    public function setStaffId($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDormancyReason()
    {
        return $this->dormancy_reason;
    }

    /**
     * @param mixed $dormancy_reason
     */
    public function setDormancyReason($dormancy_reason)
    {
        $this->dormancy_reason = $dormancy_reason;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * @param mixed $middle_name
     */
    public function setMiddleName($middle_name)
    {
        $this->middle_name = $middle_name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }
    public function getFullName(){
        return $this->getFirstName().' '.$this->getSurname();

    }
    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->address_line1;
    }

    /**
     * @param mixed $address_line1
     */
    public function setAddressLine1($address_line1)
    {
        $this->address_line1 = $address_line1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->address_line2;
    }

    /**
     * @param mixed $address_line2
     */
    public function setAddressLine2($address_line2)
    {
        $this->address_line2 = $address_line2;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getValidator()
    {
        $validator = new Validator($this->toArray());
        //$validator->startRulesFor('staff_id')->required();
        $validator->startRulesFor('first_name')->required()->alpha();
        $validator->startRulesFor('surname')->required()->alpha();
        $validator->startRulesFor('address_line1')->required();
        $validator->startRulesFor('city')->required();
        $validator->startRulesFor('country')->required();
        $validator->startRulesFor('postcode')->required();
        $validator->startRulesFor('phone')->required();
        $validator->startRulesFor('email')->required()->email();
        //$validator->startRulesFor('roles')->required();
        return $validator;
    }
    public static function getTable()
    {
        return new DatabaseTable('staff', static::class, 'staff_id');
    }
    public function delete(){
        $staffRoles= StaffRole::find('staff_id=:staff_id', ['staff_id'=>$this->staff_id]);
        foreach($staffRoles as $staffRole){
            $staffRole->delete();
        }
        parent::delete();
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public static function search($keyword,$status){
        $pdo = DatabaseTable::getConnection();

        $criteria = [
            'search' => '%' . $keyword . '%',
            'keyword'=>$keyword,
            'status'=>$status
        ];


        $stmt = $pdo->prepare('
        SELECT * FROM staff s
						WHERE
                        (staff_id =:keyword OR 
                        CONCAT(first_name, \' \', surname) LIKE :search OR
                        first_name LIKE :search OR 
                        surname LIKE :search)
                         AND status = :status;
        ');

        $stmt->execute($criteria);

        $staff = DatabaseTable::getObjectArray(static::class,$stmt);
        return $staff;
    }
}