<?php
/**
 * Created by Tobi Ayilara.
 * Date: 13/02/2017
 * Time: 20:18
 */

namespace app\models;


use app\utilities\database_utilities\DatabaseTable;

abstract class Model
{
    public function __construct($attributes = [])
    {
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * @return DatabaseTable
     */
    public static function getTable()
    {
        //TODO: throw no table connected
        return null;
    }

    public static function find($conditions = '', $values = [], $sort = null)
    {
        $table = static::getTable();

        return $table->find($conditions, $values, $sort);
    }

    /**
     * @param $id
     * @return Model
     * @throws \Exception
     */
    public static function get($id)
    {
        $table = static::getTable();
        $id_column_name = $table->getIdColumnName();
        if($id_column_name ==null){
            throw new \Exception('cannot use get method for a table with a null id_column_name specified');
        }
        $results = self::find($conditions = $id_column_name . ' = :id', $values = ['id' => $id]);

        if (count($results) >= 1) {
            return $results[0];
        }
        throw new \Exception('Record Not Found');
    }

    public static function getAll()
    {
        return self::find();
    }

    public function save(){
        $table = static::getTable();
        $id_column_name = $table->getIdColumnName();
        $table->save($this->toArray(),$id_column_name);
    }

    public function insert(){
        $table = static::getTable();
        return $table->insert($this->toArray());
    }

    public function update(){
        $table = static::getTable();
        $id_column_name = $table->getIdColumnName();
        $table->update($this->toArray(),$id_column_name);
    }

    public function delete(){
        $table = static::getTable();
        $id_column_name = $table->getIdColumnName();
        $table->delete($id_column_name. '= :id',['id'=>$this->$id_column_name]);
    }

    public function updateAttributes($attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
    }
}