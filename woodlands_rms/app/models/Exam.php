<?php
/**
 * Created by PhpStorm.
 * User: Tomas Kompas
 * Date: 27/02/2017
 * Time: 14:37
 */

namespace app\models;


class Exam extends Model
{
    protected $exam_id;
    protected $start_date;
    protected $hours;
    protected $title;
    protected $module_code;
    protected $location;
    /**
     * @return mixed
     */
    public function getExamId()
    {
        return $this->exam_id;
    }

    /**
     * @param mixed $exam_id
     * @return Exam
     */
    public function setExamId($exam_id)
    {
        $this->exam_id = $exam_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     * @return Exam
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @param mixed $hours
     * @return Exam
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Exam
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleCode()
    {
        return $this->module_code;
    }

    /**
     * @param mixed $module_code
     * @return Exam
     */
    public function setModuleCode($module_code)
    {
        $this->module_code = $module_code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return Exam
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

}