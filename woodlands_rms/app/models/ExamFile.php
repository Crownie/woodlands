<?php
/**
 * Created by Alex
 */

namespace app\models;

use app\utilities\database_utilities\DatabaseTable;
use app\utilities\validator\Validator;
class ExamFile extends Model
{
    protected $exam_id;
    protected $file_url;

    /**
     * @return mixed
     */
    public function getExamId()
    {
        return $this->exam_id;
    }

    /**
     * @param mixed $exam_id
     * @return ExamFile
     */
    public function setExamId($exam_id)
    {
        $this->exam_id = $exam_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }

    /**
     * @param mixed $file_url
     * @return ExamFile
     */
    public function setFileUrl($file_url)
    {
        $this->file_url = $file_url;
        return $this;
    }

    public function getValidator(){
        $validator = new Validator($this->toArray());
        $validator->startRulesFor('exam_id')->required();
        $validator->startRulesFor('file_url')->required();
        return $validator;
    }

    public function toArray()
    {
        // TODO: Implement toArray() method.
    }
    public static function getTable()
    {
        return new DatabaseTable('exam_files',ExamFile::class,'exam_file_id');
    }


}