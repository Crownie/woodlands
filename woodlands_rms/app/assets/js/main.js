
//@prepros-prepend jquery.min.js
//@prepros-prepend moment.js
//@prepros-prepend bootstrap.min.js
//@prepros-prepend bootstrap-datetimepicker.js
//@prepros-prepend bootstrap-select.min.js
//@prepros-prepend printThis.js
//@prepros-prepend timetable.min.js
//@prepros-prepend toastr.min.js
//@prepros-prepend sweetalert.min.js
//@prepros-prepend jquery.quicksearch.js
//@prepros-prepend jquery.multi-select.js
//@prepros-prepend fullcalendar.min.js
//@prepros-prepend diary.js
//@prepros-prepend multiple-select-modal-ajax.js

function htmlPost(path, params, method) {
    //code taken from http://stackoverflow.com/a/133997
    method = method || "post"; // Set method to post by default if not specified.

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

    $(function () {

        var studentTimetable = new Timetable();

        if($('.timetable').length>0){
            studentTimetable.setScope(9, 18); // optional, only whole hours between 0 and 23

            studentTimetable.addLocations(['Monday', 'Tuesday', 'Wednesday', 'Thursday','Friday']);
            var json_url = $('.timetable').data('json-url');

            $.get(json_url,function(data){
                var arr = JSON.parse(data);
                console.log(arr);
                for(var i =0; i<arr.length; i++){
                    var entry = arr[i];
                    var day = entry.day.toLowerCase().capitalizeFirstLetter();
                    //this is where we last stopped , need to make date dynamic ^, we need a start/end time in database

                    studentTimetable.addEvent(entry.title, day, moment(entry.start_time, "HH:mm").toDate(), moment(entry.end_time, "HH:mm").toDate(),{
                        data:{
                            'timetable_item_id':entry.timetable_item_id
                        }
                    });
                }
                var renderer = new Timetable.Renderer(studentTimetable);
                renderer.draw('.timetable');
            });


            $('body').on('click','.time-entry',function(){
                var $modal = $('#timetable-entry-editor-modal');
                var $this = $(this);
                var timetable_item_id = $this.data('timetable_item_id');
                var form_url = $('.timetable').data('edit-form-url');
                $modal.find('.modal-body').html('..Loading');
                $.get(form_url+'?item-id='+timetable_item_id,function(data){
                    $modal.find('.modal-body').html(data);
                });

                $modal.modal('show');
            });

        }

        /*Button warn*/
        $('body').on('click','.btn-delete-warn',function(){
            var warningCaption = $(this).data('warning-caption');
            var route = $(this).data('route');

            swal({
                    title: "Are you sure?",
                    text: warningCaption,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function(){
                    //yes
                    htmlPost(route,{'_token':csrf})
                });
        });


        /*Multiselect on steroid*/

        function uniqId() {
            return Math.round(new Date().getTime() + (Math.random() * 100));
        }


        $('.multiple-select').each(function(i,e){
            //if element has no id set unique id
            if(!$(this).attr('id')){
                $(this).attr('id',uniqId());
            }

            $(e).multiSelect({
                selectableHeader: '<p><i>Unassigned</i></p><input type="text" class="selectable-search search-input form-control" autocomplete="off" placeholder="filter">',
                selectionHeader: '<p><i>Assigned</i></p><input type="text" class="selection-search search-input form-control" autocomplete="off" placeholder="filter">',
                afterInit: function (ms) {

                    var that = this,
                        $selectableSearch = that.$container.find('.selectable-search'),
                        $selectionSearch = that.$container.find('.selection-search'),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                    console.log($selectableSearch);
                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function (e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function (e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
                },
                afterSelect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
        });
        //This works for event listening for checkboxes
        //Source: http://stackoverflow.com/a/3442342
        $('#student-form [name="both_addresses_same[]"]').change(function() {
            var addressFields = ['address_line1','address_line2', 'city','country','postcode'];
            // this will contain a reference to the checkbox
            if (this.checked) {
                // the checkbox is now checked
                for(var i =0; i<addressFields.length; i++){
                    $('#student-form [name="'+addressFields[i]+'"]').attr("disabled","");
                }
                //console.log('checked');
            } else {
                // the checkbox is now no longer checked
                for(var i =0; i<addressFields.length; i++){
                    $('#student-form [name="'+addressFields[i]+'"]').removeAttr("disabled");
                }
            }
        });

        $('#student-form [name="both_addresses_same[]"]').change();

        //console.log($('#announcement-form [name="announcement_type"]'));
        $('#announcement-form [name="announcement_type"]').change(function(){
            var moduleField = 'module_id';
            var courseField = 'course_id';
            //alert('HI');
            if ($('#announcement-form [name="announcement_type"]').val() == "COURSE"){
                $('#announcement-form [name = "'+moduleField+'"]').attr("disabled","");
                $('#announcement-form [name = "'+courseField+'"]').removeAttr("disabled");
                $('#announcement-form [name = "'+moduleField+'"]').siblings("button").attr("disabled","");
                $('#announcement-form [name = "'+courseField+'"]').siblings("button").removeAttr("disabled");
                $('#announcement-form [name = "'+courseField+'"]').siblings("button").removeClass("disabled");

            }
            else if ($('#announcement-form [name="announcement_type"]').val() == "MODULE"){
                $('#announcement-form [name = "'+courseField+'"]').attr("disabled","");
                $('#announcement-form [name = "'+moduleField+'"]').removeAttr("disabled");
                $('#announcement-form [name = "'+courseField+'"]').siblings("button").attr("disabled","");
                $('#announcement-form [name = "'+moduleField+'"]').siblings("button").removeAttr("disabled");
                $('#announcement-form [name = "'+moduleField+'"]').siblings("button").removeClass("disabled");
            }
            else{
                $('#announcement-form [name = "'+courseField+'"]').attr("disabled","");
                $('#announcement-form [name = "'+moduleField+'"]').attr("disabled","");
                $('#announcement-form [name = "'+courseField+'"]').siblings("button").attr("disabled","");
                $('#announcement-form [name = "'+moduleField+'"]').siblings("button").attr("disabled","");
            }
        });
        $('#announcement-form [name="announcement_type"]').change();


        $('.add-qualification-btn').click(function (e) {
            e.preventDefault();
            var num = $('.qualification').length+1;
            $.get('http://rms.wuc.ac.uk/students/get-qualification-fields?num='+num,function (data) {
                $('.add-qualification-btn').parent().before(data);
                $('.add-qualification-btn').html('Add another qualification');
            });
        });

        $('body').on('click','.remove-qualification',function (e) {
            //alert('hi');
           $(this).closest('.panel').remove();
        });





//Attendance radio button
        $('.attendance-radio a').on('click', function(){
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            var $input = $('#'+tog);
            $input.prop('value', sel);

            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').addClass('active');

            var data = {
                attendance_type:$input.data('attendance-type'),
                student_id : $input.data('student-id'),
                status : sel,
                _token:csrf
            };

            $.post('',data,function(){

            });
        });

        $('.student-archive-btn').click(function(){
            var student_id = $(this).data('student-id');
            $('#student-archive-modal').find('input[name="student_id"]').val(student_id);
        });


        $('.staff-archive-btn').click(function(){
            var staff_id = $(this).data('staff-id');
            $('#staff-archive-modal').find('input[name="staff_id"]').val(staff_id);
        });

        $('.accept-change').click(function(){
            var $input = $(this).closest('.details-change-input').find('input');
            var value = $input.val();

            var $tartget_field = $('[name = "'+$input.data('target-field')+'"]');

            if($tartget_field.prop('type')=='text'){
                $tartget_field.val(value);
                $(this).closest('.details-change-input').fadeOut();
                $tartget_field.addClass('animate-highlight');
            }
        });

        $('.reject-change').click(function(){
            $(this).closest('.details-change-input').fadeOut();
        });

        $('.print-element').click(function(){
            var target = $(this).data('target');
            $(target).printThis();
        });

            $('.next-tab-btn').click(function(){
                var target = $(this).data('target');
                $('a[href="'+target+'"]').tab('show');
            });

    });

