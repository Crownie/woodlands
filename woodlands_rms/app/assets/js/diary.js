$(function () {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate:   moment(),
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        dayClick: function(date, jsEvent, view) {

            console.log('Clicked on: ' + date.format());

        },
        eventClick: function(calEvent, jsEvent, view) {

            //console.log('Event: ' + calEvent.title);

            $('#diary-modal').find('.event-title').html(calEvent.title);

            $('#diary-modal').find('.start-date').html(calEvent.start_date);

            $('#diary-modal').find('.end-date').html(calEvent.end_date);

            $('#diary-modal').find('.location').html(calEvent.location);

            $('#diary-modal').find('.description').html(calEvent.description);

            $('#diary-modal').modal('show');

        },
        events: [
        ]
    });

    if($('#calendar').length >0){
        var json_url = $('#calendar').data('json-url');

        $.get(json_url,function(data){
            var eventsArr = JSON.parse(data);

            for(var i = 0; i<eventsArr.length;i++){

                $('#calendar').fullCalendar( 'renderEvent',eventsArr[i],true );
            }

        });
    }



});