var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');

gulp.task('css', function() {
    gulp.src([
        './css/bootstrap.css',
        './css/font-awesome.css',
        './css/timetablejs.css',
        './css/bootstrap-datetimepicker.min.css',
        './css/bootstrap-select.min.css',
        './css/toastr.min.css',
        './css/sweetalert.css',
        './css/multi-select.css',
        './css/fullcalendar.min.css',
        './css/style.css'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css/'));
});