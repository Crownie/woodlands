<?php
/**
 * Created by Tobi Ayilara.
 * Date: 04/02/2017
 * Time: 20:03
 */
use app\Router;

/** @var $router Router */

$router->get('/','\\app\\Controllers\\DashboardController::index')->bind('dashboard');


$router->get('/login','\\app\\Controllers\\AuthController::showLoginForm')->bind(unprotect('login.form'));

$router->post('/login','\\app\\Controllers\\AuthController::login')->bind(unprotect('login'));

$router->get('/logout','\\app\\Controllers\\AuthController::logout')->bind(unprotect('logout'));

$router->crud('\\app\\Controllers\\CourseController','courses');

$router->crud('\\app\\Controllers\\AssignmentController','assignments');
$router->crud('\\app\\Controllers\\StaffController','staff');
$router->crud('\\app\\Controllers\\RoomController','rooms');
$router->crud('\\app\\Controllers\\StudentController','students');
$router->crud('\\app\\Controllers\\TimetableController','timetables');
$router->crud('\\app\\Controllers\\AnnouncementController','announcements');

$router->crud('\\app\\Controllers\\ModuleGroupController','module-groups');
$router->get('/module-groups/{group_number}/student-options','\\app\\Controllers\\ModuleGroupController::getStudentOptions')->bind('module-groups.getStudentOptions');
$router->post('/module-groups/{group_number}/add','\\app\\Controllers\\ModuleGroupController::addStudentToGroup')->bind('module-groups.addStudentToGroup');
$router->get('/module-groups/{{student_id}}/{module_id}/remove','\\app\\Controllers\\ModuleGroupController::removeStudent')->bind('module-groups.removeStudent');
$router->get('/module-groups/{student_id}/{group_number}/{module_id}','\\app\\Controllers\\ModuleGroupController::moveToGroup')->bind('module-groups.moveToGroup');


$router->get('/courses/{id}/module-options','\\app\\Controllers\\CourseController::getModuleOptions')->bind('courses.getModuleOptions');
$router->post('/courses/{id}/module-options/add','\\app\\Controllers\\CourseController::addModulesToCourse')->bind('courses.addModulesToCourse');
$router->crud('\\app\\Controllers\\ModuleController','modules');

$router->get('/modules/{id}/course-options','\\app\\Controllers\\ModuleController::getCourseOptions')->bind('modules.getCourseOptions');
$router->post('/modules/{id}/modules-options/add','\\app\\Controllers\\ModuleController::addCourseToModule')->bind('modules.addCourseToModule');

$router->crud('\\app\\Controllers\\PersonalTutorController','personal-tutors');


$router->crud('\\app\\Controllers\\DiaryController','diary');
$router->get('/diary/get-events','\\app\\Controllers\\DiaryController::getEvents')->bind('diary.getEvents');
$router->crud('\\app\\Controllers\\AttendanceController','attendance');
$router->crud('\\app\\Controllers\\SubmissionController','submission');

$router->get('/students/get-qualification-fields','\\app\\Controllers\\StudentController::getQualificationFields')->bind('students.getQualificationFields');
$router->get('/timetables/{id}/get-events','\\app\\Controllers\\TimetableController::getEvents')->bind('timetables.getEvents');
$router->post('/timetables/{id}/add-item','\\app\\Controllers\\TimetableController::addItem')->bind('timetables.addItem');
$router->get('/timetables/{id}/get-item-edit-form','\\app\\Controllers\\TimetableController::getItemEditForm')->bind('timetables.getItemEditForm');
$router->post('/timetables/{id}/update-timetable-item','\\app\\Controllers\\TimetableController::updateItem')->bind('timetables.updateItem');
$router->post('/timetables/{id}/delete-timetable-item','\\app\\Controllers\\TimetableController::deleteItem')->bind('timetables.deleteItem');

$router->get('/personal-tutor/{id}/student-options','\\app\\Controllers\\PersonalTutorController::getStudentOptions')->bind('personal-tutors.getStudentOptions');
$router->post('/personal-tutor/{id}/add','\\app\\Controllers\\PersonalTutorController::addStudentToPersonalTutor')->bind('personal-tutors.addStudentToPersonalTutor');

$router->get('/students/{id}/remove-personal-tutor','\\app\\Controllers\\StudentController::removePersonalTutor')->bind('students.removePersonalTutor');
$router->post('/students/archive','\\app\\Controllers\\StudentController::archive')->bind('students.archive');
$router->post('/students/make-live/{id}','\\app\\Controllers\\StudentController::makeLive')->bind('students.makeLive');
$router->post('/staff/archive','\\app\\Controllers\\StaffController::archive')->bind('staff.archive');
$router->post('/modules/archive/{id}','\\app\\Controllers\\ModuleController::archive')->bind('modules.archive');


$router->get('/students/details-change-requests','\\app\\Controllers\\DetailsChangeRequestController::showChangeRequests')->bind('details-change-requests.showChangeRequests');
$router->get('/students/review-change-request/{id}','\\app\\Controllers\\DetailsChangeRequestController::reviewChangeRequest')->bind('details-change-requests.reviewChangeRequest');
$router->post('/students/review-change-request/{id}','\\app\\Controllers\\DetailsChangeRequestController::saveChanges');
$router->get('/students/timetables/{student_id}','\\app\\Controllers\\TimetableController::getStudentEvents')->bind('timetables.getStudentEvents');
$router->get('/reports/tutor-timetable/{staff_id}/events','\\app\\Controllers\\TimetableController::getTutorTimetableItems')->bind('timetables.getTutorTimetableItems');

$router->get('/reports','\\app\\Controllers\\ReportController::listReports')->bind('reports.listReports');
$router->get('/reports/tutor-timetable','\\app\\Controllers\\ReportController::reportTimetableForTutor')->bind('reports.reportTimetableForTutor');
$router->get('/reports/student-timetable','\\app\\Controllers\\ReportController::reportTimetableForStudent')->bind('reports.reportTimetableForStudent');
$router->get('/reports/student-timetable','\\app\\Controllers\\ReportController::reportTimetableForStudent')->bind('reports.reportTimetableForStudent');
$router->get('/reports/tutees-by-tutor','\\app\\Controllers\\ReportController::reportTuteesByTutor')->bind('reports.reportTuteesByTutor');
$router->get('/reports/grade-by-student','\\app\\Controllers\\ReportController::reportGradesByStudent')->bind('reports.reportGradesByStudent');
