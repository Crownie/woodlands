<?php
/**
 * Created by Tobi Ayilara.
 * Date: 11/02/2017
 * Time: 15:43
 */

namespace app\utilities\database_utilities;

use Exception;
use PDO;

class DatabaseTable
{
    /**@var $pdo PDO */
    private static $pdo;
    private $table_name = '';
    private $class_name = null;
    private $id_column_name = '';

    public function __construct($table_name, $class_name, $id_column_name = null)
    {
        $this->table_name = $table_name;
        $this->class_name = $class_name;
        $this->id_column_name = $id_column_name;
    }

    public static function setConnection(\PDO $pdo)
    {
        self::$pdo = $pdo;
    }

    public static function getConnection(){
        return self::$pdo;
    }

    public function getIdColumnName()
    {
        return $this->id_column_name;
    }

    public function getAll()
    {
        return $this->find();
    }

    public function find($conditions = '', $values = null, $sort = null)
    {
        $sort_str = '';
        if ($sort != null) {
            $sort_str = 'ORDER BY ' . $sort[0] . ' ' . ($sort[1] == true ? 'ASC' : 'DESC');
        }

        if ($conditions == null) {
            $stmt = self::$pdo->prepare('SELECT * FROM ' . $this->table_name . ' ' . $sort_str);
        } else {
            $stmt = self::$pdo->prepare('SELECT * FROM ' . $this->table_name . ' WHERE ' . $conditions . ' ' . $sort_str);
        }

        $object_array = [];

        $stmt->execute($values);

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


        foreach ($result as $row) {
            try {
                $object_array[] = new $this->class_name($row);
            } catch (\Exception $exception) {
                $object_array[] = $row;
            }
        }


        return $object_array;
    }

    public function insert($record)
    {
        $keys = array_keys($record);
        $values = implode(', ', $keys);
        $valuesWithColon = implode(', :', $keys);
        $query = 'INSERT INTO ' . $this->table_name . ' (' . $values . ') VALUES (:' . $valuesWithColon . ')';
        $stmt = self::$pdo->prepare($query);
        $stmt->execute($record);
        return self::$pdo->lastInsertId();
    }

    public function update($record, $primaryKey)
    {
        $keys = array_keys($record);

        $updateArr = [];

        foreach ($keys as $key) {
            $updateArr[] = $key . ' = :' . $key;
        }

        $implodedStr = implode(', ', $updateArr);

        $stmt = self::$pdo->prepare('UPDATE ' . $this->table_name . ' 
				 SET ' . $implodedStr . '
				 WHERE ' . $primaryKey . '= :primaryKeyValue');

        $record['primaryKeyValue'] = $record[$primaryKey];


        $stmt->execute($record);
        return $stmt;
    }

    /**
     * @param $record
     * @param null $primaryKey
     * @throws Exception
     */
    public function save($record, $primaryKey = null){
        try{
            $this->insert($record);
        }catch(\PDOException $pdo_exception){
            if($pdo_exception->getCode()==23000){
                try{
                    $this->update($record,$primaryKey);
                }catch (Exception $updateException){
                    throw $updateException;
                }
            }else{
                throw $pdo_exception;
            }
        }
    }

    public function delete($conditions, $values)
    {
        $stmt = self::$pdo->prepare('DELETE FROM ' . $this->table_name . ' WHERE ' . $conditions);
        $stmt->execute($values);
    }

    public static function getObjectArray($class_name,$stmt){
        $object_array = [];


        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


        foreach ($result as $row) {
            try {
                $object_array[] = new $class_name($row);
            } catch (\Exception $exception) {
                $object_array[] = $row;
            }
        }


        return $object_array;
    }

}