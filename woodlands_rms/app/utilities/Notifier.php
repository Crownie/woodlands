<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 14/01/2017
 * Time: 09:44
 */

namespace app\utilities;


class Notifier
{
    //TODO: append to notifications instead of overriding existing
    public static function error($message){
        $notification = new \stdClass();
        $notification->message = $message;
        $notification->type = 'error';

        session()->set('notification',$notification);
    }
    
    public static function success($message){
        $notification = new \stdClass();
        $notification->message = $message;
        $notification->type = 'success';

        session()->set('notification',$notification);
    }
    
    public static function display(){
        if(session()->has('notification') && !empty(session()->get('notification'))){
            $notification = session()->get('notification');

            echo view()->render('partials/notification-js',get_defined_vars());
        }
        session()->remove('notification');
    }
}