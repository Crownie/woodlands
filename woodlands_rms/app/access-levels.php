<?php

use app\RecordManagementSystem;

$access_levels = [
    'courses.showRecordList' => ['CL', 'ML', 'PT'],
    'courses.showRecordForm' => ['CL'],
    'courses.showRecord' => ['CL', 'ML', 'PT'],
    'courses.showRecordEditForm' => ['CL'],
    'courses.storeRecord' => ['CL'],
    'courses.updateRecord' => ['CL'],
    'courses.deleteRecord' => ['CL'],
    'assignments.showRecordList' => ['CL', 'ML', 'PT'],
    'assignments.showRecordForm' => ['CL', 'ML', 'PT'],
    'assignments.showRecord' => ['CL', 'ML', 'PT'],
    'assignments.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'assignments.storeRecord' => ['CL', 'ML', 'PT'],
    'assignments.updateRecord' => ['CL', 'ML', 'PT'],
    'assignments.deleteRecord' => ['CL', 'ML', 'PT'],
    'staff.showRecordList' => ['CL', 'ML', 'PT'],
    'staff.showRecordForm' => ['CL'],
    'staff.showRecord' => ['CL', 'ML', 'PT'],
    'staff.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'staff.storeRecord' => ['CL', 'ML', 'PT'],
    'staff.updateRecord' => ['CL', 'ML', 'PT'],
    'staff.deleteRecord' => ['CL', 'ML', 'PT'],
    'rooms.showRecordList' => ['CL', 'ML', 'PT'],
    'rooms.showRecordForm' => ['CL'],
    'rooms.showRecord' => ['CL', 'ML', 'PT'],
    'rooms.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'rooms.storeRecord' => ['CL', 'ML', 'PT'],
    'rooms.updateRecord' => ['CL', 'ML', 'PT'],
    'rooms.deleteRecord' => ['CL', 'ML', 'PT'],
    'students.showRecordList' => ['CL', 'ML', 'PT'],
    'students.showRecordForm' => ['CL'],
    'students.showRecord' => ['CL', 'ML', 'PT'],
    'students.showRecordEditForm' => ['CL'],
    'students.storeRecord' => ['CL', 'ML'],
    'students.updateRecord' => ['CL', 'ML'],
    'students.deleteRecord' => ['CL', 'ML'],
    'timetables.showRecordList' => ['CL', 'ML', 'PT'],
    'timetables.showRecordForm' => ['CL'],
    'timetables.showRecord' => ['CL', 'ML', 'PT'],
    'timetables.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'timetables.storeRecord' => ['CL'],
    'timetables.updateRecord' => ['CL'],
    'timetables.deleteRecord' => ['CL'],
    'announcements.showRecordList' => ['CL', 'ML', 'PT'],
    'announcements.showRecordForm' => ['CL', 'ML', 'PT'],
    'announcements.showRecord' => ['CL', 'ML', 'PT'],
    'announcements.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'announcements.storeRecord' => ['CL', 'ML', 'PT'],
    'announcements.updateRecord' => ['CL', 'ML', 'PT'],
    'announcements.deleteRecord' => ['CL', 'ML', 'PT'],
    'module-groups.showRecordList' => ['CL', 'ML', 'PT'],
    'module-groups.showRecordForm' => ['CL', 'ML', 'PT'],
    'module-groups.showRecord' => ['CL', 'ML', 'PT'],
    'module-groups.showRecordEditForm' => ['CL'],
    'module-groups.storeRecord' => ['CL', 'ML', 'PT'],
    'module-groups.updateRecord' => ['CL', 'ML', 'PT'],
    'module-groups.deleteRecord' => ['CL', 'ML', 'PT'],
    'module-groups.getStudentOptions' => ['CL', 'ML', 'PT'],
    'module-groups.addStudentToGroup' => ['CL', 'ML', 'PT'],
    'module-groups.removeStudent' => ['CL', 'ML', 'PT'],
    'module-groups.moveToGroup' => ['CL', 'ML', 'PT'],
    'courses.getModuleOptions' => ['CL', 'ML', 'PT'],
    'courses.addModulesToCourse' => ['CL', 'ML', 'PT'],
    'modules.showRecordList' => ['CL', 'ML', 'PT'],
    'modules.showRecordForm' => ['CL'],
    'modules.showRecord' => ['CL', 'ML', 'PT'],
    'modules.showRecordEditForm' => ['CL'],
    'modules.storeRecord' => ['CL', 'ML', 'PT'],
    'modules.updateRecord' => ['CL', 'ML', 'PT'],
    'modules.deleteRecord' => ['CL', 'ML', 'PT'],
    'modules.getCourseOptions' => ['CL', 'ML', 'PT'],
    'modules.addCourseToModule' => ['CL', 'ML', 'PT'],
    'personal-tutors.showRecordList' => ['CL', 'ML', 'PT'],
    'personal-tutors.showRecordForm' => ['CL', 'ML', 'PT'],
    'personal-tutors.showRecord' => ['CL', 'ML', 'PT'],
    'personal-tutors.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'personal-tutors.storeRecord' => ['CL', 'ML', 'PT'],
    'personal-tutors.updateRecord' => ['CL', 'ML', 'PT'],
    'personal-tutors.deleteRecord' => ['CL', 'ML', 'PT'],
    'diary.showRecordList' => ['CL', 'ML', 'PT'],
    'diary.showRecordForm' => ['CL', 'ML', 'PT'],
    'diary.showRecord' => ['CL', 'ML', 'PT'],
    'diary.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'diary.storeRecord' => ['CL', 'ML', 'PT'],
    'diary.updateRecord' => ['CL', 'ML', 'PT'],
    'diary.deleteRecord' => ['CL', 'ML', 'PT'],
    'diary.getEvents' => ['CL', 'ML', 'PT'],
    'attendance.showRecordList' => ['CL', 'ML', 'PT'],
    'attendance.showRecordForm' => ['CL', 'ML', 'PT'],
    'attendance.showRecord' => ['CL', 'ML', 'PT'],
    'attendance.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'attendance.storeRecord' => ['CL', 'ML', 'PT'],
    'attendance.updateRecord' => ['CL', 'ML', 'PT'],
    'attendance.deleteRecord' => ['CL', 'ML', 'PT'],
    'submission.showRecordList' => ['CL', 'ML', 'PT'],
    'submission.showRecordForm' => ['CL', 'ML', 'PT'],
    'submission.showRecord' => ['CL', 'ML', 'PT'],
    'submission.showRecordEditForm' => ['CL', 'ML', 'PT'],
    'submission.storeRecord' => ['CL', 'ML', 'PT'],
    'submission.updateRecord' => ['CL', 'ML', 'PT'],
    'submission.deleteRecord' => ['CL', 'ML', 'PT'],
    'students.getQualificationFields' => ['CL', 'ML', 'PT'],
    'timetables.getEvents' => ['CL', 'ML', 'PT'],
    'timetables.addItem' => ['CL'],
    'timetables.getItemEditForm' => ['CL','ML','PT'],
    'timetables.updateItem' => ['CL'],
    'timetables.deleteItem' => ['CL'],
    'personal-tutors.getStudentOptions' => ['CL', 'ML', 'PT'],
    'personal-tutors.addStudentToPersonalTutor' => ['CL', 'ML', 'PT'],
    'students.removePersonalTutor' => ['CL', 'ML', 'PT'],
    'students.archive' => ['CL', 'ML', 'PT'],
    'students.makeLive' => ['CL', 'ML', 'PT'],
    'staff.archive' => ['CL', 'ML', 'PT'],
    'modules.archive' => ['CL', 'ML', 'PT'],
    'details-change-requests.showChangeRequests' => ['CL'],
    'details-change-requests.reviewChangeRequest' => ['CL'],
    'POST_students_review_change_request_id' => ['CL'],
    'timetables.getStudentEvents' => ['CL', 'ML', 'PT'],
    'timetables.getTutorTimetableItems' => ['CL', 'ML', 'PT'],
    'reports.listReports' => ['CL', 'ML', 'PT'],
    'reports.reportTimetableForTutor' => ['CL', 'ML', 'PT'],
    'reports.reportTimetableForStudent' => ['CL', 'ML', 'PT'],
    'reports.reportTuteesByTutor' => ['CL', 'ML', 'PT'],
    'reports.reportGradesByStudent' => ['CL', 'ML', 'PT'],
];

function get_access_level_by_url(){
    global $access_levels;
    foreach ($access_levels as $name => $roles){
        if(is_current_route(route(''))){

        }
    }
}

function get_current_route_roles($specified_route_name=null){
    global $access_levels;

    $route_name=!empty($specified_route_name)?$specified_route_name:RecordManagementSystem::getCurrentRouteName();

    foreach ($access_levels as $name => $roles){
        if($name == $route_name){
            return $roles;
        }
    }
    return [];
}

function authorized($specified_route_name=null){
    $allowed_roles = get_current_route_roles($specified_route_name);
    //dd($allowed_roles);
    if(count($allowed_roles)<=0){
        return true;
    }else{

        $staff = \app\controllers\AuthController::getLoggedInStaff();
        if($staff){
            $roles = $staff->getRoles();
            foreach ($roles as $role){

                if(in_array($role,$allowed_roles)){
                    //user is authorised
                    return true;
                }
            }
        }
    }

    return false;
}