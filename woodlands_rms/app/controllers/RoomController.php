<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 27/02/2017
 * Time: 14:18
 */

namespace app\controllers;


use app\models\Room;
use Symfony\Component\HttpFoundation\Request;

class RoomController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $rooms = Room::getAll();
        return view()->render('room/room-list', ['rooms'=>$rooms]);
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {


       $room = new Room([]);
       return view()->render('room/room-create',['room'=> $room] );
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {

    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {

    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);

        $room = new Room($data);

            $room->save();


        notify_success('Room was created successfully');
        return redirect('rooms.showRecordList');
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement updateRecord() method.
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try {
            $room = Room::get($id);
            $room->delete();
            notify_success('Room was deleted successfully');
        } catch (\Exception $e) {
            dd($e->getMessage());
            notify_error('An internal error occurred while trying to delete Room');
        }

        return redirect('rooms.showRecordList');
    }

    public static function getRoomOptions()
    {
        $rooms = Room::getAll();
        $room_options = [];
        foreach($rooms as $room){
            /* @var $room Room*/
            $room_options[$room->getRoomId()]=$room->getRoomNumber().' - '.$room->getRoomType().' ('.$room->getCapacity().')';
        }
        return $room_options;
    }
}