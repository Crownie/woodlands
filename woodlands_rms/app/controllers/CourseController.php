<?php
/**
 * Created by Tobi Ayilara.
 * Date: 04/02/2017
 * Time: 21:53
 */

namespace app\Controllers;


use app\models\CourseModule;
use app\models\Module;
use Symfony\Component\HttpFoundation\Request;
use app\models\Course;

class CourseController extends CRUDController
{

    public function __construct()
    {

    }

    public function showRecordList(Request $request)
    {
        $courses = Course::getAll();

        return view()->render('course/course-list', ['courses' => $courses]);
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $course = new Course([]);
        $modules = Module::getAll();

        $module_options = [];
        $selected_module_options = [];

        foreach ($modules as $module) {
            /* @var $module Module */
            $module_options[$module->getModuleId()] = $module->getModuleCode() . ' - ' . $module->getTitle();
        }

        return view()->render('course/course-create', get_defined_vars());
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        $course = Course::get($id);
        return view()->render('course/course-view', ['id' => $id, 'course' => $course]);
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        $course = Course::get($id);

        $modules = Module::getAll();
        $course_modules = CourseModule::find('course_id=:course_id', ['course_id' => $id]);

        $module_options = [];
        $selected_module_options = [];

        foreach ($modules as $module) {
            /* @var $module Module */
            $module_options[$module->getModuleId()] = $module->getModuleCode() . ' - ' . $module->getTitle();
        }

        foreach ($course_modules as $course_module){
            /* @var $course_module CourseModule */
            $selected_module_options[] = $course_module->getModuleId();
        }

        return view()->render('course/course-edit', get_defined_vars());
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);

        $module_codes = $data['course_modules'];

        unset($data['course_modules']);
        $course = new Course($data);




        $validator = $course->getValidator();

        if ($validator->isValid()) {
            try {
                $id = $course->insert();
                CourseModule::updateModules($id, $module_codes);
                notify_success('Course was created successfully');
                return redirect('courses.showRecordList');
            } catch (\Exception $e) {
                notify_error($e->getMessage());
                return redirect('courses.showRecordForm');
            }
        } else {
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('courses.showRecordForm');
        }

    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        /* @var $course Course */
        $data = get_post_array($request);
        $course = Course::get($id);

        $module_codes = $data['course_modules'];

        unset($data['course_modules']);

        $course->updateAttributes($data);

        $validator = $course->getValidator();
        if ($validator->isValid()) {
            try {
                $course->save();
                CourseModule::updateModules($id, $module_codes);
                notify_success('Course was updated successfully');
                return redirect('courses.showRecordList');
            } catch (\Exception $e) {
                notify_error($e->getMessage());
                return redirect('courses.showRecordEditForm', ['id' => $id]);
            }
        } else {
            $errors = $validator->getErrors();
            dd($errors);

            show_form_errors($errors);
            return redirect('courses.showRecordEditForm', ['id' => $id]);

        }
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try {
            $course = Course::get($id);
            $course->delete();
            notify_success('Course was deleted successfully');
        } catch (\Exception $e) {
            notify_error('An internal error occurred while trying to delete course');
        }

        return redirect('courses.showRecordList');
    }

    public function getModuleOptions($id)
    {
        $module_options = [];

        $modules = Module::getAll();
        $selected_modules = CourseModule::find('course_id=:course_id', ['course_id' => $id]);

        foreach ($modules as $module) {
            /* @var $module Module */
            $selected = false;

            for ($i = 0, $j = count($selected_modules); $i < $j; $i++) {
                $selected_module = $selected_modules[$i];

                if ($selected_module->getModuleId() == $module->getModuleId()) {
                    $selected = true;
                    array_splice($selected_modules, $i, 1);

                    break;
                }
            }

            $module_options[] = [
                'id' => $module->getModuleId(),
                'text' => $module->getModuleCode() . ' - ' . $module->getTitle(),
                'selected' => $selected
            ];
        }

        return json_encode($module_options);
    }

    public function addModulesToCourse(Request $request, $id)
    {
        $data = get_post_array($request);
        $selected = isset($data['selected']) ? $data['selected'] : [];
        CourseModule::updateModules($id, $selected);
        return '';
    }

    /**
     * @return array Array of courses with id as the key and title as the value
     */
    public static function getCourseOptions(){
        $courses = Course::getAll();
        $course_options = [];
        foreach ($courses as $course) {
            /* @var $course Course */
            $course_options[$course->getCourseId()] = $course->getTitle();
        }
        return $course_options;
    }

    public function archive(Request $request,$id){
        $data = get_post_array($request);

        $course = Course::get($id);
        $course->setStatus('DORMANT');
        $course->save();
        notify_success('Course was archived successfully');
        return redirect_back();
    }
}