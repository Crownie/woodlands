<?php
/**
 * Created by PhpStorm.
 * User: Tomas Kompas
 * Date: 09/04/2017
 * Time: 15:37
 */

namespace app\controllers;


use app\models\Announcement;
use Symfony\Component\HttpFoundation\Request;
use app\utilities\validator\Validator;

class AnnouncementController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $announcements = Announcement::find('',[],['announcement_id',false]);
        return view()->render('announcement/announcement-list',['announcements'=>$announcements]);
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $announcement = new Announcement([]);
        return view()->render('announcement/announcement-create',['announcement'=>$announcement]);
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        // TODO: Implement showRecord() method.
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement showRecordEditForm() method.
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);

        $data['date_posted'] = date('Y-m-d H:i:s');
        $announcement = new Announcement($data);
        $validator = $announcement->getValidator();

        if($validator->isValid()){
            $announcement->save();
            notify_success('Announcement was created successfully');
            return redirect('announcements.showRecordList');
        }

    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement updateRecord() method.
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try{
            $announcement = Announcement::get($id);
            $announcement->delete();
            notify_success('Announcement was deleted successfully');
        }
        catch(\Exception $e){
            notify_error('An internal error occured while trying to delete Announcement record');
        }
    }
}