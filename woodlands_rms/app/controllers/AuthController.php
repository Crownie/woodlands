<?php
/**
 * Created by Tobi Ayilara.
 * Date: 06/02/2017
 * Time: 08:39
 */

namespace app\controllers;


use app\models\Staff;
use Symfony\Component\HttpFoundation\Request;

class AuthController
{
    /**
     * Check if user is logged. redirect if user is not logged in. return null if logged in
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public static function authenticate()
    {
        if(self::getLoggedInStaff() == null){
            //user is not logged in
            return redirect('login.form');
        }
        if(!authorized()){
            //if the current staff is not authorised return
            die('Unauthorised Access');
        }
        return null;
    }

    public function showLoginForm(){
        return view()->render('auth/login');
    }

    public function login(Request $request){
        //TODO: implement the authentication
        $data = get_post_array($request);
        try{
            $staff = Staff::get($data['username']);
            if(!password_verify($data['password'],$staff->getPassword())){
                throw new \Exception('wrong password');
            }
            session()->set('staff_id',$staff->getStaffId());
            notify_success('Welcome back '.$staff->getFirstName());
            return redirect_url('/');
        }catch(\Exception $exception){
            notify_error('Incorrect username or password');
            return redirect('login');
        }
    }

    public function logout(){
        //TODO: remove the user from session
        session()->remove('staff_id');
        notify_success('You are now logged out');
        return redirect('login.form');
    }

    /**
     * @return Staff|null
     */
    public static function getLoggedInStaff(){
        $staff_id = session()->get('staff_id');
        $staff = null;

        try{
            $staff = Staff::get($staff_id);
        }catch (\Exception $exception){

        }
        return $staff;
    }

}