<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 20/02/2017
 * Time: 15:52
 */

namespace app\controllers;



use app\models\Submission;
use Symfony\Component\HttpFoundation\Request;

class SubmissionController extends CRUDController
{


    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        // TODO: Implement showRecordList() method.
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        // TODO: Implement showRecordForm() method.
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        $submission = Submission::get($id);
        return view()->render('submissions/submission-grade', ['submission'=>$submission, 'id'=>$id]);
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement showRecordEditForm() method.
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);

        $submission = new Submission($data);

        $id = $submission->insert();
        notify_success('Assignment has been marked');
        return redirect('assignments.showRecordList');

    }


    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        $data = get_post_array($request);

        $submission = Submission::get($id);
        $submission->updateAttributes($data);

        $validator = $submission->getValidator();
        if ($validator->isValid()) {
            try {
                $submission->save();
                notify_success('Assignment has een marked');
                return redirect('assignments.showRecordList');
            } catch (\Exception $e) {
                notify_error($e->getMessage());
                return redirect('submission.showRecord', ['id' => $id]);
            }
        } else {
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('submission.showRecord', ['id' => $id]);

        }
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement deleteRecord() method.
    }
}