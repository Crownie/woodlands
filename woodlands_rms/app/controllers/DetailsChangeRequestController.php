<?php
/**
 * Created by PhpStorm.
 * User: Tomas Kompas
 * Date: 10/04/2017
 * Time: 14:45
 */

namespace app\controllers;


use app\models\DetailsChangeRequest;
use app\models\Student;
use Symfony\Component\HttpFoundation\Request;

class DetailsChangeRequestController
{
    public function showChangeRequests(){
        $requests = DetailsChangeRequest::find('status = :status',['status'=>'OPEN']);
        return view()->render('details-change-request/details-change-request-list', ['requests'=>$requests]);
    }
    public function reviewChangeRequest($id){
        /* @var $request DetailsChangeRequest */
        $request = DetailsChangeRequest::get($id);
        $data = json_decode($request->getChangedData());
        $data = is_object($data)?get_object_vars($data):new \stdClass();
        return view()->render('details-change-request/details-change-request-review', get_defined_vars());
    }

    public function saveChanges(Request $r, $id){
        $data = get_post_array($r);
        unset($data['both_addresses_same']);
        /* @var $request DetailsChangeRequest */
        $request = DetailsChangeRequest::get($id);

        $student = Student::get($id);

        $student->updateAttributes($data);

        try{
            //dd($student);
            $student->update();
            $request->setStatus('CLOSE');
            $request->save();
            notify_success('changes updated successfully');
            return redirect('details-change-requests.showChangeRequests');
        }catch (\Exception $exception){
            dd($exception);
            notify_error('error saving data');
            return redirect('details-change-requests.reviewChangeRequest',['id'=>$id]);
        }

    }

    public static function countPendingRequests(){
        $requests = DetailsChangeRequest::find('status = :status',['status'=>'OPEN']);
        return count($requests);
    }
}