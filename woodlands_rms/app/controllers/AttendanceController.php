<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 20/02/2017
 * Time: 15:50
 */

namespace app\controllers;



use app\models\Attendance;
use app\models\CourseModule;
use app\models\Student;
use app\models\StudentModuleGroup;
use SendGrid;
use Symfony\Component\HttpFoundation\Request;

class AttendanceController extends CRUDController
{


    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $module_options = ModuleController::getModuleOptionsAll();
        $module_id = $_GET['module'];
        $group_number = $_GET['group'];
        $year = $_GET['year'];
        $week = $_GET['week'];
        $student_module_groups = StudentModuleGroup::find('module_id = :module_id AND group_number = :group_number',['module_id'=>$module_id,'group_number'=>$group_number]);

        return view()->render('attendance/attendance-list', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        // TODO: Implement showRecordForm() method.
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {

    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        // TODO: Implement showRecordEditForm() method.
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);
        $data['module_id'] = $_GET['module'];
        $data['week'] = $_GET['week'];

        $criteria = $data;
        unset($criteria['status']);
        $attendances = Attendance::find('attendance_type = :attendance_type AND module_id = :module_id AND student_id = :student_id AND week = :week',
            $criteria);

        $attendance = count($attendances)<=0?new Attendance($data):$attendances[0];
        $attendance->setStatus($data['status']);
        $attendance->save();
        $student = Student::get($attendance->getStudentId());

        $week =$data['week'];

        if($attendance->getStatus()=='X'){
            if(Attendance::countAbsensePastLast3Weeks($student,$week)>=3){
                send_cause_for_concern($student,'Having poor or no attendance at classes');
            }
        }


        return 'ok';
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        // TODO: Implement updateRecord() method.
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement deleteRecord() method.
    }
}