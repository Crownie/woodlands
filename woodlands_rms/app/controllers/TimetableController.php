<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\controllers;

use app\models\Announcement;
use app\models\Student;
use app\models\StudentModuleGroup;
use app\models\Timetable;
use app\models\TimetableItem;
use Symfony\Component\HttpFoundation\Request;


class TimetableController extends CRUDController
{


    public function __construct()
    {

    }

    public function showRecordList(Request $request)
    {
        $timetableItems = Timetable::getAll();

        return view()->render('timetable/timetable-list', ['timetables'=>$timetableItems]);
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed

     */
    public function showRecordForm(Request $request)
    {
        $timetableItem = new Timetable([]);
        return view()->render('timetable/timetable-create',['timetable'=> $timetableItem] );
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        return redirect('timetables.showRecordEditForm',['id'=>$id]);
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed

     */
    public function showRecordEditForm(Request $request, $id)
    {
        $timetable = Timetable::get($id);
        return view()->render('timetable/timetable-edit',get_defined_vars());
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed

     */

    public function getStudentEvents($student_id){
        /* @var $student Student*/

        $student = Student::get($student_id);
        //dd($student);
        $criteria = [
            'course_id'=>$student->getCourseId(),
            'level'=>$student->getLevel(),
            'year'=>$student->getCurrentYear()
        ];

        $timetableItems = [];
        $timetables = Timetable::find('course_id = :course_id AND level =:level AND year = :year',$criteria);

        if(count($timetables)>0){
            $timetable = $timetables[0];
            $timetableItems = TimetableItem::find('timetable_id = :timetable_id',['timetable_id'=>$timetable->getTimetableId()]);
        }

        $student_module_groups = StudentModuleGroup::find('student_id = :student_id',[
            'student_id'=>$student->getStudentId()
        ]);


        $timetableItemArrs = [];

        //TODO: filter out only timetable for the group the user belongs to
        foreach ($timetableItems as $timetableItem){
            /* @var $timetableItem TimetableItem*/
            $module = $timetableItem->getModule();

            if($timetableItem->getModuleGroupNumber() == '0' || $this->inModuleGroup($student_module_groups,$timetableItem->getModuleId(),$timetableItem->getModuleGroupNumber())){
                $module_code = $module->getModuleCode();
                $module_group_number = $timetableItem->getModuleGroupNumber();
                $room_number = $timetableItem->getRoom()->getRoomNumber();
                $timetableItemArr = $timetableItem->toArray();
                $timetableItemArr['module_code']=$module_code;
                $timetableItemArr['title'] = view()->render('timetable/partials/time-entry',get_defined_vars());
                $timetableItemArrs[] = $timetableItemArr;
            }


        }
        //var_dump($data);

        return json_encode($timetableItemArrs);
    }
    private function inModuleGroup($student_module_groups,$module_id,$group_number){
        foreach ($student_module_groups as $student_module_group){
            if($module_id==$student_module_group->getModuleId() && $group_number == $student_module_group->getGroupNumber()){

                return true;
            }
        }
        return false;
    }

    public function getTutorTimetableItems($staff_id){
        $timetable_items = TimetableItem::getItemsForTutor($staff_id);

        $timetableItemArrs = [];

        foreach ($timetable_items as $timetableItem){
            /* @var $timetableItem TimetableItem*/
            $module = $timetableItem->getModule();

            $module_code = $module->getModuleCode();
            $module_group_number = $timetableItem->getModuleGroupNumber();
            $room_number = $timetableItem->getRoom()->getRoomNumber();
            $timetableItemArr = $timetableItem->toArray();
            $timetableItemArr['module_code'] = $module_code;
            $timetableItemArr['title'] = view()->render('timetable/partials/time-entry', get_defined_vars());
            $timetableItemArrs[] = $timetableItemArr;

        }
        //var_dump($data);

        return json_encode($timetableItemArrs);
    }


    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);
        //dd($data);
        $timetableItem = new Timetable($data);

        try{
           $id = $timetableItem->insert();
            return redirect('timetable.showRecordEditForm', ['id'=>$id]);
        }catch (\Exception $e){
            dd($e->getMessage());
            dd($e->getTraceAsString());
        }
        return redirect('timetable.showRecordForm');
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed

     */
    public function updateRecord(Request $request, $id)
    {
        dd($request);
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed

     */
    public function deleteRecord(Request $request, $id)
    {
        try {
            $timetableItem = Timetable::get($id);
            $timetableItem->delete();
            notify_success('Timetable was deleted successfully');
        } catch (\Exception $e) {
            notify_error('An internal error occurred while trying to delete timetable');
        }

        return redirect('timetables.showRecordList');
    }

    public function getEvents(Request $request,$id)
    {
        $timetableItems = TimetableItem::find('timetable_id = :timetable_id',['timetable_id'=>$id]);

        $timetableItemArrs = [];
        foreach ($timetableItems as $timetableItem){
            /* @var $timetableItem TimetableItem*/
            $module_code = $timetableItem->getModule()->getModuleCode();
            $module_group_number = $timetableItem->getModuleGroupNumber();
            $room_number = $timetableItem->getRoom()->getRoomNumber();
            $timetableItemArr = $timetableItem->toArray();
            $timetableItemArr['module_code']=$module_code;
            $timetableItemArr['title'] = view()->render('timetable/partials/time-entry',get_defined_vars());
            $timetableItemArrs[] = $timetableItemArr;
        }
        //var_dump($data);

        return json_encode($timetableItemArrs);

    }

    public function addItem(Request $request,$id){
       $data = get_post_array($request);
        $timetable_item = new TimetableItem($data);
        $timetable_item->setTimetableId($id);
        $validator = $timetable_item->getValidator();
        if($validator->isValid()){
            $timetable_item->save();
            notify_success('Timetable item was added successfully');
        }else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
        }
        return redirect('timetables.showRecordEditForm',['id'=>$id]);
    }

    public function getItemEditForm(Request $request,$id){
        $timetable = Timetable::get($id);
        $item_id = $_GET['item-id'];
        $timetable_item = TimetableItem::get($item_id);

        return view()->render('timetable/partials/timetable-item-form',get_defined_vars());
    }

    public function updateItem(Request $request,$id){
        $data = get_post_array($request);
        $timetable_item = TimetableItem::get($id);
        $timetable_item->updateAttributes($data);
        $validator = $timetable_item->getValidator();
        if($validator->isValid()){
            $timetable_item->save();
            //get target students
            $students = $timetable_item->getTargetStudents();
            foreach ($students as $student){
                send_email($student->getEmail(),'Timetable updated','Your course timetable has been updated');
            }
            $announcement = new Announcement();
            $announcement->setTitle('Your timetable was updated');
            $announcement->setAnnouncementType('MODULE');
            $announcement->setModuleId($timetable_item->getModuleId());
            $announcement->setTarget('BOTH');
            $announcement->setMessage('Your timetable was updated. Ensure that you have the latest [<a href="http://students.wuc.ac.uk/timetable">View Timetable</a>]');
            $announcement->setDatePosted(date('Y-m-d H:i:s'));
            $announcement->save();
            notify_success('Updated successfully');
        }else{
            notify_error('an error occurred when trying to update the timetable item');

        }
        return redirect_back();
    }

    public function deleteItem(Request $request,$id){
        $timetable_item = TimetableItem::get($id);
        $timetable_item->delete();
        notify_success('Timetable item was deleted successfully');
        return redirect_back();
    }

    public function archive(Request $request,$id){
        $timetable = Timetable::get($id);
        $timetable->setStatus('DORMANT');
        $timetable->save();
        notify_success('Timetable was archived successfully');
        return redirect_back();
    }
}