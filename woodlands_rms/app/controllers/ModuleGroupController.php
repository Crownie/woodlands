<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\Controllers;

use app\models\Module;
use app\models\Student;
use app\models\StudentModuleGroup;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class ModuleGroupController extends CRUDController
{

    /**
     * Displays the record list
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $module_id = $_GET['module_id'];
        $module = Module::get($module_id);
        $lev = $module->getLevel();
        return view()->render('module-group/module-group', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        // TODO: Implement create() method.
    }

    /**
     * Displays a single record specified by the id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $group_number)
    {
        // TODO: Implement show() method.
    }

    /**
     * Displays the form for editing a record
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $group_number)
    {
        // TODO: Implement edit() method.
    }

    /**
     * Stores a new record
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        // TODO: Implement store() method.
    }

    /**
     * Updates the record specified by the id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $group_number)
    {
        // TODO: Implement update() method.
    }

    /**
     * Deletes the record specified by the id
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        // TODO: Implement delete() method.
    }

    public function removeStudent($student_id,$module_id){
        $student_module_groups = StudentModuleGroup::find('student_id=:student_id AND module_id =:module_id',[
            'student_id'=>$student_id,
            'module_id'=>$module_id
        ]);

        if(count($student_module_groups)>0){
            $student_module_group = $student_module_groups[0];
            $student_module_group->delete();
            $student = Student::get($student_id);
            notify_success($student->getFullName().' has been removed from all groups');
        }else{
            notify_error('Record not found');
        }
        return redirect_back();
    }

    public function moveToGroup($student_id,$group_number,$module_id){
        $student_module_groups = StudentModuleGroup::find('student_id=:student_id AND module_id =:module_id',[
            'student_id'=>$student_id,
            'module_id'=>$module_id
        ]);

        if(count($student_module_groups)>0){
            $student_module_group = $student_module_groups[0];
            $student_module_group->setGroupNumber($group_number);
            $student_module_group->update();
            $student = Student::get($student_id);
            notify_success($student->getFullName().' has been moved to Group '.$group_number);
        }else{
            notify_error('Record not found');
        }
        return redirect_back();
    }

    public function getStudentOptions(Request $request,$group_number){

        $module_id = $_GET['module_id'];
        $module = Module::get($module_id);
        $lev = $module->getLevel();
        $students = Student::getStudentsNotInModuleGroup($module_id,$lev);

        $student_options=[];

        foreach ($students as $student){
            /* @var $student Student*/
            $student_options[] =
                [
                    'id'=>$student->getStudentId(),
                    'text'=>$student->getStudentId().' - '.$student->getFullName()
                ];
        }

        return json_encode($student_options);
    }

    public function addStudentToGroup(Request $request,$group_number){
        $data = get_post_array($request);
        $student_ids = $data['selected'];
        $module_id = $_GET['module_id'];
        $module = Module::get($module_id);
        $lev = $module->getLevel();
        foreach ($student_ids as $student_id){
            $student_module_group = new StudentModuleGroup();
            $student_module_group->setGroupNumber($group_number);
            $student_module_group->setModuleId($module_id);
            $student_module_group->setStudentId($student_id);

            try{
                $student_module_group->save();
                notify_success('changes was saved successfully');
            }catch(\Exception $exception){
                notify_error('error adding students to group');
            }
        }
        return '';
    }
}