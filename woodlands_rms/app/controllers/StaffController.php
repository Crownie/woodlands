<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 27/02/2017
 * Time: 13:44
 */

namespace app\controllers;

use app\models\StaffRole;
use app\utilities\database_utilities\DatabaseTable;
use Symfony\Component\HttpFoundation\Request;
use app\utilities\validator\Validator;
use app\models\Staff;

class StaffController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {

        $search = '';
        $status = '';
        if(isset($_GET['search'])){
            $search = $_GET['search'];
            $status = isset($_GET['status'])?$_GET['status']:null;
            $staff = Staff::search($_GET['search'],$status);
        }else{
        $staff = Staff::getAll();
        }
        return view()->render('staff/staff-list', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $staff = new Staff([]);
        return view()->render('staff/staff-create', ['staff'=>$staff]);
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        $staff = Staff::get($id);
        return view()->render('staff/staff-view', ['staff'=>$staff, 'id'=>$id]);
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        $staff = Staff::get($id);
        return view()->render('staff/staff-edit', ['staff'=>$staff, 'id'=>$id]);
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);

        $roles = isset($data['roles'])?$data['roles']:[];
        $data['status'] = 'LIVE';
        unset($data['roles']);



        $staff = new Staff($data);

        $validator = $staff->getValidator();

        if($validator->isValid()){
            $id = $staff->insert();
            foreach ($roles as $role) {
                //dd($role);
                $staffRole = new StaffRole(['staff_id' => $id, 'role' => $role]);
                $staffRole->insert();
            }
            notify_success('Staff was created successfully');
            return redirect('staff.showRecordList');
        }else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
            //return redirect('staff.showRecordForm');
        }

    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        $data = get_post_array($request);

        $roles = isset($data['roles'])?$data['roles']:[];
        unset($data['roles']);



        /** @var  $staff Staff */
        $staff = Staff::get($id);
        $staff->updateAttributes($data);
        $validator = $staff->getValidator();

        if($validator->isValid()){
            $staff->save();
            StaffRole::clearStaffRoles($id);
            foreach ($roles as $role) {

                $staffRole = new StaffRole(['staff_id' => $id, 'role' => $role]);
                $staffRole->save();
            }
            notify_success('Staff was updated successfully');
            return redirect('staff.showRecordList');
        }else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('staff.showRecordEditForm',['id'=>$id]);
        }
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try{
            $staff = Staff::get($id);
            $staff->delete();
            StaffRole::clearStaffRoles($id);
            notify_success('Staff record was deleted successfully');
        }catch(\Exception $e){
            notify_error('An internal error occurred while trying to delete Staff record'.$e->getMessage());
        }

        return redirect('staff.showRecordList');
    }

    public function archive(Request $request){
        $data = get_post_array($request);

        $staff = Staff::get($data['staff_id']);
        $staff->setStatus('DORMANT');
        $staff->setDormancyReason($data['dormancy_reason']);
        $staff->save();
        notify_success('Staff was archived successfully');
        return redirect_back();
    }

    public static function getModuleLeaders(){
        $leaders = ['0'=>''];
        $moduleLeaders = StaffRole::find('role = :role',['role'=>'ML']);
        foreach ($moduleLeaders as $moduleLeader){
            $staff = Staff::get($moduleLeader->getStaffId());
            $leaders[$moduleLeader->getStaffId()] = $staff->getFullName();
        }

        return $leaders;
    }
}