<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 20/02/2017
 * Time: 15:49
 */

namespace app\controllers;



use app\models\Course;
use app\models\CourseModule;
use app\models\Module;
use app\models\ModuleLeader;
use Symfony\Component\HttpFoundation\Request;

class ModuleController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $search = '';
        $status = '';
        if(isset($_GET['search'])){
            $search = $_GET['search'];
            $status = isset($_GET['status'])?$_GET['status']:null;
            $modules = Module::search($_GET['search'],$status);
        }else {
            $modules = Module::find('status=\'LIVE\'');
        }
        return view()->render('module/module-list', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $module = new Module([]);
        return view()->render('module/module-create', ['module'=>$module]);
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        $module = Module::get($id);
        return view()->render('module/module-view', ['id' => $id, 'module' => $module]);
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        $module = Module::get($id);
        return view()->render('module/module-edit', ['module'=>$module, 'id'=>$id]);
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);


        $staff_id = isset($data['module_leader_id'])?$data['module_leader_id']:[];

        unset ($data['module_leader_id']);
        $data['status'] = 'LIVE';
        $module = new Module($data);
        $validator = $module->getValidator();

        if($validator->isValid()){
            $module_id = $module->insert();
            if($staff_id != '0'){
                $moduleLeader = new ModuleLeader([
                    'staff_id'=>$staff_id,
                    'module_id'=>$module_id
                ]);
                $moduleLeader->save();
            }
            notify_success('Module successfully created');
            return redirect('modules.showRecordList');
        }else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('modules.showRecordForm');
        }
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        $data = get_post_array($request);
        $module = Module::get($id);



        $staff_id = isset($data['module_leader_id'])?$data['module_leader_id']:[];
        unset ($data['module_leader_id']);

        $module->updateAttributes($data);

        $validator = $module->getValidator();
        if ($validator->isValid()) {
            try {

                $module->save();
                if($staff_id != '0') {
                    //dd($id);
                    ModuleLeader::clearModuleLeader($id);
                    $moduleLeader = new ModuleLeader([
                        'staff_id' => $staff_id,
                        'module_id' => $id
                    ]);
                    //dd($moduleLeader);
                    $moduleLeader->save();
                }
                notify_success('Module updated successfully');
                return redirect('modules.showRecordList');
            } catch (\Exception $e) {
                notify_error($e->getMessage());
                return redirect('modules.showRecordEditForm', ['id' => $id]);
            }
        } else {
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('modules.showRecordEditForm', ['id' => $id]);

        }
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try{
            $module = Module::get($id);
            $module->delete();
            notify_success('module was deleted successfully');
        }catch(\Exception $e){
            notify_error('An internal error occurred while trying to delete course');
        }

        return redirect('modules.showRecordList');
    }

    public function getCourseOptions($id)
    {
        $course_options = [];

        $courses = Course::getAll();
        $selected_courses = Course::find('module_id=:course_id', ['module_id' => $id]);

        foreach ($courses as $course) {
            /* @var $course Course */
            $selected = false;

            for ($i = 0, $j = count($selected_courses); $i < $j; $i++) {
                $selected_courses = $selected_courses[$i];

                if($selected_courses->getTitle() == $course->getTitle()){
                    $selected = true;
                    array_splice($selected_courses,$i,1);

                    break;
                }
            }

            $course_options[] = [
                'id' => $course->getTitle(),
                'text' => $course->getTitle() . ' - ' . $course->getTitle(),
                'selected' => $selected
            ];
        }

        return json_encode($course_options);
    }

    public function addCourseToModule(Request $request, $id)
    {
        $data = get_post_array($request);
        $selected = $data['selected'];
        Course::updateModules($id,$selected);
        return '';
    }

    public static function getModuleOptions($course_id){
        $module_options = [];

        $course_modules = CourseModule::find('course_id=:course_id', ['course_id' => $course_id]);

        foreach ($course_modules as $course_module) {
            /* @var $module Module */
            /* @var $course_module CourseModule */
            $module = $course_module->getModule();
            $module_options[$module->getModuleId().''] = $module->getModuleCode() . ' - ' . $module->getTitle();
        }
        return $module_options;
    }

    public static function getModuleOptionsAll(){
        $module_options = [];

        $modules = Module::getAll();

        foreach ($modules as $module) {
            /* @var $module Module */
            $module_options[$module->getModuleId().''] = $module->getModuleCode() . ' - ' . $module->getTitle();
        }
        return $module_options;
    }

    public function archive(Request $request,$id){
        $module = Module::get($id);
        $module->setStatus('DORMANT');
        $module->save();
        notify_success('Module was archived successfully');
        return redirect_back();
    }

}