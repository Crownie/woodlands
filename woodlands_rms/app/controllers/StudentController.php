<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 20/02/2017
 * Time: 15:51
 */

namespace app\controllers;



use app\models\PersonalTutor;
use app\models\Qualification;
use app\models\Student;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class StudentController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $search = '';
        $course_id = '';
        $status = '';
        if(isset($_GET['search'])){
            $search = $_GET['search'];
            $course_id=isset($_GET['course_id'])?$_GET['course_id']:null;
            $course_id=$course_id==0?null:$course_id;
            $status = isset($_GET['status'])?$_GET['status']:null;
            $students = Student::search($_GET['search'],$course_id,$status);
        }else{
            $students = Student::getAll();
        }
        return view()->render('student/student-list', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $student = new Student([]);
        return view()->render('student/student-create', ['student'=>$student]);
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        $student = Student::get($id);
        return view()->render('student/student-view', ['student'=>$student, 'id'=>$id]);
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        $student = Student::get($id);
        $qualifications = $student->getQualifications();
        return view()->render('student/student-edit', get_defined_vars());
    }





    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);
        /*foreach($data['dates_achieved'] as $date){
            echo $date.'<br>';
        };*/
        $qualifications = isset($data['qualifications'])?$data['qualifications']:[];
        $grades_achieved = isset($data['grades_achieved'])?$data['grades_achieved']:[];
        $qualification_types = isset($data['qualification_types'])?$data['qualification_types']:[];
        $dates_achieved = isset($data['dates_achieved'])?$data['dates_achieved']:[];


        $same_addresses = isset($data['both_addresses_same'])?$data['both_addresses_same']:'n';
        $data['qualifications_verified'] = isset($data['qualifications_verified'])?'1':'0';
        //$course = isset($data['course'])?$data['course']:[];



        //unset ($data['course']);
        unset ($data['qualifications']);
        unset ($data['grades_achieved']);
        unset ($data['qualification_types']);
        unset ($data['dates_achieved']);
        if(isset ($data['both_addresses_same']))unset($data['both_addresses_same']);

        if ($same_addresses == 'y'){
            unset($data['address_line1']);
            unset($data['address_line2']);
            unset($data['city']);
            unset($data['country']);
            unset($data['postcode']);
        }
        $data['password'] = password_hash('student',PASSWORD_DEFAULT);
        $data['current_year'] = date('Y');

        if($data['qualifications_verified'] == '1'){
            $data['status']='LIVE';
        }else{
            $data['status']='PROVISIONAL';
        }

        $student = new Student($data);

        $validator = $student->getValidator();

        if($validator->isValid()){
            //dd($data);
            $id = $student->insert();

            for($i = 0; $i<count($qualifications); $i++){


                $df = \DateTime::createFromFormat('d/m/Y', $dates_achieved[$i]);

                $dates_achieved[$i] = $df->format('Y-m-d');
                $qualification = new Qualification([
                    'student_id' => $id,
                    'qualification_type' => $qualification_types[$i],
                    'qualification_name' => $qualifications[$i],
                    'grade_achieved' => $grades_achieved[$i],
                    'date_achieved' => $dates_achieved[$i]
                ]);
                $qualification->insert();
            }
            notify_success('Student was created successfully');
            return redirect ('students.showRecordList');
        }
        else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('students.showRecordForm');
        }

    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        $data = get_post_array($request);
        /*foreach($data['dates_achieved'] as $date){
            echo $date.'<br>';
        };*/
        $qualifications = isset($data['qualifications'])?$data['qualifications']:[];
        $grades_achieved = isset($data['grades_achieved'])?$data['grades_achieved']:[];
        $qualification_types = isset($data['qualification_types'])?$data['qualification_types']:[];
        $dates_achieved = isset($data['dates_achieved'])?$data['dates_achieved']:[];
        $same_addresses = isset($data['both_addresses_same'])?$data['both_addresses_same']:'n';
        $data['qualifications_verified'] = isset($data['qualifications_verified'])?'1':'0';
        //$course = isset($data['course'])?$data['course']:[];

        //$data['status']='LIVE';

        //unset ($data['course']);
        unset ($data['qualifications']);
        unset ($data['grades_achieved']);
        unset ($data['qualification_types']);
        unset ($data['dates_achieved']);
        if(isset ($data['both_addresses_same']))unset($data['both_addresses_same']);

        if ($same_addresses == 'y'){
            unset($data['address_line1']);
            unset($data['address_line2']);
            unset($data['city']);
            unset($data['country']);
            unset($data['postcode']);
        }

        /** @var  $student Student */
        $student = Student::get($id);
        $data['current_year'] = date('Y');
        $student->updateAttributes($data);

        $validator = $student->getValidator();

        if($validator->isValid()){
            //dd($data);
            $student->save();
            Qualification::clearQualifications($id);
            for($i = 0; $i<count($qualifications); $i++){


                $df = \DateTime::createFromFormat('d/m/Y', $dates_achieved[$i]);

                $dates_achieved[$i] = $df->format('Y-m-d');




                $qualification = new Qualification([
                    'student_id' => $id,
                    'qualification_type' => $qualification_types[$i],
                    'qualification_name' => $qualifications[$i],
                    'grade_achieved' => $grades_achieved[$i],
                    'date_achieved' => $dates_achieved[$i]
                ]);

                $qualification->save();
            }
            notify_success('Student was updated successfully');
            return redirect ('students.showRecordList');
        }
        else{
            $errors = $validator->getErrors();
            show_form_errors($errors);
            foreach ($errors as $error){
                notify_error('There were errors in the form: '.$error[0]);
            }
            //notify_error($error);
            //dd($errors);
            return redirect('students.showRecordEditForm',['id'=>$id]);
        }
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try{
            $student = Student::get($id);
            Qualification::clearQualifications($id);
            $student->delete();

            notify_success('Student record was deleted successfully');
        }catch(\Exception $e){
            notify_error('An internal error occurred while trying to delete Student record'.$e->getMessage());
        }

        return redirect('students.showRecordList');
    }

    public function getQualificationFields(){
        $num = $_GET['num'];
        return view()->render('student/partials/student-qualification-fields',get_defined_vars());
    }

    public function removePersonalTutor($id){
        try{
            $student = Student::get($id);
            $student->setPersonalTutorId(null);
            $student->save();
            notify_success($student->getFullName().' was removed');
        }catch (Exception $exception){
            notify_error('unable to remove student');
        }
       return redirect_back();
    }

    public function archive(Request $request){
        $data = get_post_array($request);

        $student = Student::get($data['student_id']);
        $student->setStatus('DORMANT');
        $student->setDormancyReason($data['dormancy_reason']);
        $student->save();
        notify_success('Student was archived successfully');
        return redirect_back();
    }

    public static function getStudentOptionsAll(){
        $students = Student::getAll();

        $student_options=[];

        foreach ($students as $student){
            /* @var $student Student*/
            $student_options[$student->getStudentId()] =$student->getStudentId().' - '.$student->getFullName();
        }
        return $student_options;
    }

    public function makeLive($id){
        $student = Student::get($id);
        $student->setStatus('LIVE');
        if($student->getPersonalTutorId()==null){
            $student->setPersonalTutorId(PersonalTutor::getPersonalTutorIdWithLowestStudentCount());
        }
        $student->save();
        notify_success('Student record is now live');
        return redirect_back();
    }
}