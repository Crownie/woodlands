<?php
/**
 * Created by Mahbub Haque.
 */

namespace app\Controllers;

use app\models\Assignment;
use app\models\AssignmentFile;
use app\models\Submission;
use Symfony\Component\HttpFoundation\Request;

class AssignmentController extends CRUDController
{

    /**
     * Displays the record list
     * @param Request $request
     * @return mixed
     */
    public function showRecordList(Request $request)
    {
        $submissions = Submission::getAll();
        $assignments = Assignment::getAll();
        return view()->render('assignment/assignment-list', get_defined_vars());
    }

    /**
     * Displays the form for creating new record
     * @param Request $request
     * @return mixed
     */
    public function showRecordForm(Request $request)
    {
        $assignment = new Assignment([]);
        return view()->render('assignment/assignment-create',['assignment'=>$assignment]);
    }

    /**
     * Displays a single record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecord(Request $request, $id)
    {
        $assignment = Assignment::get($id);
        $assignment_files = AssignmentFile::find('assignment_id = :assignment_id',['assignment_id'=>$assignment->getAssignmentId()]);

        $submissions = Submission::find('assignment_id=:assignment_id',[
            'assignment_id'=>$id
        ]);
        return view()->render('assignment/assignment-view', get_defined_vars());
    }

    /**
     * Displays the form for editing a record
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function showRecordEditForm(Request $request, $id)
    {
        $assignment = Assignment::get($id);
        return view()->render('assignment/assignment-edit', ['assignment'=>$assignment, 'id'=>$id]);
    }

    /**
     * Stores a new record
     * @param Request $request
     * @return mixed
     */
    public function storeRecord(Request $request)
    {
        $data = get_post_array($request);

        $fileDestination= __DIR__."/../uploads/".basename($_FILES['assignment_file']['name']);

        $data['start_date'] = date('Y-m-d H:i:s',strtotime($data['start_date']));
        $data['due_date'] = date('Y-m-d H:i:s',strtotime($data['due_date']));

        $assignment = new Assignment($data);

        $validator = $assignment->getValidator();

        if($validator->isValid()){

            $lastId = $assignment->insert();

            $assignmentFile = new AssignmentFile();
            $assignmentFile->setAssignmentId($lastId);
            $assignmentFile->setFileUrl($_FILES['assignment_file']['name']);
            $assignmentFile->save();

            move_uploaded_file($_FILES['assignment_file']['tmp_name'],$fileDestination);

            notify_success('Assignment was created successfully');
            return redirect('assignments.showRecordList');
        }else{
            $errors = $validator->getErrors();
            dd($errors);
            show_form_errors($errors);
            return redirect('assignments.showRecordForm');
        }
    }

    /**
     * Updates the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function updateRecord(Request $request, $id)
    {
        $data = get_post_array($request);
        $data['start_date'] = date('Y-m-d H:i:s',strtotime($data['start_date']));
        $data['due_date'] = date('Y-m-d H:i:s',strtotime($data['due_date']));

        $assignment = Assignment::get($id);
        $assignment->updateAttributes($data);

        $validator = $assignment->getValidator();
        if ($validator->isValid()) {
            try {
                $assignment->save();
                notify_success('Assignment was updated successfully');
                return redirect('assignments.showRecordList');
            } catch (\Exception $e) {
                notify_error($e->getMessage());
                return redirect('assignments.showRecordEditForm', ['id' => $id]);
            }
        } else {
            $errors = $validator->getErrors();
            show_form_errors($errors);
            return redirect('assignments.showRecordEditForm', ['id' => $id]);

        }
    }

    /**
     * Deletes the record specified by the id
     * @param Request $request
     * @param $id int id of the the specified record
     * @return mixed
     */
    public function deleteRecord(Request $request, $id)
    {
        try{
            $assignment = Assignment::get($id);
            $assignment->delete();
            notify_success('Assignment was deleted successfully');
        }catch(\Exception $e){
            notify_error('An internal error occurred while trying to delete the assignment');
        }

        return redirect('assignments.showRecordList');
    }
}