<?php
/**
 * Created by Tobi Ayilara.
 * Date: 12/04/2017
 * Time: 10:52
 */

namespace app\controllers;


use app\models\PersonalTutor;
use app\models\Timetable;
use app\models\TimetableItem;

class ReportController
{
    public function listReports(){
        return view()->render('report/report-list',get_defined_vars());
    }

    public function reportTimetableForTutor(){
        $staff_id = isset($_GET['staff_id'])?$_GET['staff_id']:null;

        return view()->render('report/timetable/tutor-timetable',get_defined_vars());
    }

    public function reportTimetableForStudent(){
        $student_id = isset($_GET['student_id'])?$_GET['student_id']:null;

        return view()->render('report/timetable/student-timetable',get_defined_vars());
    }

    public function reportTuteesByTutor(){
        $personal_tutors = PersonalTutor::find('',[]);
        $lev = isset($_GET['level'])?$_GET['level']:'';
        return view()->render('report/personal-tutor/tutees-by-tutor',get_defined_vars());
    }

    public function reportGradesByStudent(){
        
    }

}