<?php
/**
 * Created by Tobi Ayilara.
 * Date: 20/02/2017
 * Time: 12:53
 */

namespace app;

use app\controllers\AuthController;
use app\utilities\database_utilities\DatabaseTable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class RecordManagementSystem
{
    /** @var Router */
    private static $router;

    public static function main()
    {
        define('BASE_URL', '//rms.wuc.ac.uk');
        $router = new Router();
        self::$router = $router;
        self::setup($router);
        require 'routes.php';
        $router->run();
    }

    private static function setup(Router $router)
    {
        global $pdo;
        require_once 'db-connection.php';

        DatabaseTable::setConnection($pdo);
        $router['debug']=true;
        $router->before([RecordManagementSystem::class,'middleware']);
        $router->after([RecordManagementSystem::class,'afterRoute']);
    }

    public static function middleware(Request $request){
        $path = $request->getPathInfo();



        if(!$request->isMethod('get')){
            //verify csrf token for requests that are not GET
            $token = $request->request->get('_token');
            $request->request->remove('_token');
            if(!is_csrf_token_valid($token)){
                throw new InvalidCsrfTokenException();
            }
        }
        //trim the data before it gets to the controller
        $fields = $request->request->all();
        //dd($fields);
        foreach ($fields as $key => $value){
            if(is_string($value)){
                $fields[$key] = trim($value);
            }
        }
        $request->request->replace($fields);

        //check if the requested route requires user to be logged in
        if(is_route_protected($request)){
            return AuthController::authenticate();
        }

        return null;
    }

    public static function afterRoute(Request $request){
        if(!$request->isMethod('get')){
            session()->set('form_data',get_post_array($request));
        }else{
            session()->remove('form_data');
            session()->remove('form_errors');
        }
    }

    /**
     * @return Router
     */
    public static function getRouter()
    {
        return self::$router;
    }

    public static function getCurrentRouteName()
    {
        $request=self::$router['request_stack']->getCurrentRequest();
        $route_name = $request->get('_route');
        return $route_name;
    }


}