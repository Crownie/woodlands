<?php $this->layout('layouts/master', ['title' => 'List Assignments']) ?>

<ul class="breadcrumb">
    <li class="active">Assignment</li>
</ul>

<h2 class="page-header">Assignment</h2>

<p>
    <a href="<?=route('assignments.showRecordForm')?>" class="btn btn-success pull-right">Create Assignment</a>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Module</th>
        <th>Assignment Title</th>
        <th>Start Date</th>
        <th>Due date</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody
    <?php
    foreach ($assignments as $assignment){
        //dd($assignment);
        $this->insert('assignment/partials/assignment-item',['assignment'=>$assignment]);
    }
    ?>

    </tbody>
</table>

