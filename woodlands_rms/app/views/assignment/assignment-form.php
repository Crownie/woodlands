<?php
use app\controllers\ModuleController;
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

Form::input('title', '*Assignment Title', $assignment->getTitle());

Form::datetime('start_date','Start Date',$assignment->getStartDate());

Form::datetime('due_date','Due Date',$assignment->getDueDate());

Form::searchableSelect('module_id', '*Module', '', ModuleController::getModuleOptionsAll(), 'col-sm-6');

Form::textarea('description','Description',$assignment->getDescription());

Form::input('assignment_file','Upload file', '', ['type'=>'file']);

Form::button('Submit','btn btn-success');

/*Qualification::clearQualifications($id);
for($i = 0; $i<count($qualifications); $i++){
    $dates_achieved[$i] = date('Y-m-d H:i:s',strtotime($dates_achieved[$i]));




    $qualification = new Qualification([
        'student_id' => $id,
        'qualification_type' => $qualification_types[$i],
        'qualification_name' => $qualifications[$i],
        'grade_achieved' => $grades_achieved[$i],
        'date_achieved' => $dates_achieved[$i]
    ]);

    $qualification->save();
}
notify_success('Student was updated successfully');
return redirect ('students.showRecordList');*/