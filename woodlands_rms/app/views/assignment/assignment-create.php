<?php $this->layout('layouts/master', ['title' => 'Create Assignment']) ?>

<ul class="breadcrumb">
    <li><a href="<?=route('assignments.showRecordList')?>">Assignment</a></li>
    <li class="active">Create Assignment</li>
</ul>

<h2 class="page-header">Create Assignment</h2>

<form action="<?=route('assignments.storeRecord')?>" method="POST" enctype="multipart/form-data">
    <?php $this->insert('assignment/assignment-form',get_defined_vars()) ?>
</form>

