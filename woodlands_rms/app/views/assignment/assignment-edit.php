<?php
$this->layout('layouts/master', ['title' => 'Edit Assignment']) ?>

<ul class="breadcrumb">
    <li><a href="<?=route('assignments.showRecordList')?>">Assignment</a></li>
    <li><a href="<?=route('assignments.showRecord',['id'=>$id])?>">View Assignment</a></li>
    <li class="active">Edit Assignment</li>
</ul>

<h2 class="page-header">Edit module</h2>

<form action="<?=route('assignments.updateRecord',['id'=>$id])?>" method="POST" enctype="multipart/form-data">
    <?php $this->insert('assignment/assignment-form',get_defined_vars()) ?>
</form>