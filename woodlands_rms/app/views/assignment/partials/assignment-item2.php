<tr data-id="<?=$submission->getSubmissionId()?>">
    <th><?=$submission->getTitle()?></th>
    <th><?=$submission->getStudentId()?></th>
    <th><?=$submission->getDateSubmitted()?></th>

    <th>
        <?php
         echo '<a href="'.BASE_URL.'/app/uploadsStudents/'.$submission->getFileUrl().'">'.$submission->getFileUrl().'</a>';
        ?>
    </th>

    <th>
        <a href="<?=route('submission.showRecord',['id'=>$submission->getSubmissionId()])?>" class="btn btn-default">Mark</a>
    </th>