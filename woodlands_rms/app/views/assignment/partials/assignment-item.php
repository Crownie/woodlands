<tr data-id="<?=$assignment->getAssignmentId()?>">
    <th><?=$assignment->getModule()->getTitle()?></th>
    <th><?=$assignment->getTitle()?></th>
    <th><?=$assignment->getStartDate()?></th>
    <th><?=$assignment->getDueDate()?></th>

    <th>
        <a href="<?=route('assignments.showRecord',['id'=>$assignment->getAssignmentId()])?>" class="btn btn-default">View</a>
        <a href="<?=route('assignments.showRecordEditForm',['id'=>$assignment->getAssignmentId()])?>" class="btn btn-default">Edit</a>
        <?php
            gen_delete_button(route('assignments.deleteRecord',['id'=>$assignment->getAssignmentId()]),'this will delete the '.$assignment->getTitle().' assignment');
        ?>
    </th>
</tr>




