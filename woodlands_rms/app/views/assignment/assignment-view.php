<?php use app\models\Course;
use app\models\CourseModule;
use app\models\Module;

$this->layout('layouts/master', ['title' => 'View Assignment'])
/**@var $module Module */
?>

<ul class="breadcrumb">
    <li><a href="<?=route('assignments.showRecordList')?>">Assignment</a></li>
    <li class="active">View Assignment</li>
</ul>

<h2 class="page-header">View Assignment</h2>

<a href="<?= route('assignments.showRecordEditForm', ['id' => $id]) ?>" class="pull-right btn btn-success">Edit</a>
<span class="clearfix"></span>

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#menu1">Assignment Info</a></li>
    <li><a data-toggle="tab" href="#menu2">Submissions</a></li>
</ul>

<div class="tab-content">
    <div id="menu1" class="tab-pane fade in active"><!— for tab 1-->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Assignment Details</h1>
            </div>
            <div class="panel-body">
                <dl>
                    <dt>Assignment Title</dt>
                    <dd><?= $assignment->getTitle() ?></dd>
                    <dt>Module</dt>
                    <dd><?= $assignment->getModule()->getTitle() ?></dd>
                    <dt>Start Date</dt>
                    <dd><?= $assignment->getStartDate() ?></dd>
                    <dt>Due Date</dt>
                    <dd><?= $assignment->getDueDate() ?></dd>
                    <dt>Description</dt>
                    <dd><?= $assignment->getDescription() ?></dd>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Assignment Files</h1>
            </div>
            <div class="panel-body">

                <div class="list-group">
                    <?php
                    foreach ($assignment_files as $assignment_file) {
                        ?>
                        <a class="list-group-item"
                           href="<?= BASE_URL . '/app/uploads/' . $assignment_file->getFileUrl() ?>">
                            <i class="fa fa-file-text fa-2x"></i>
                            <?= $assignment_file->getFileUrl() ?>
                        </a>
                        <?php
                    }

                    ?>
                </div>
            </div>
        </div>


    </div>

    <div id="menu2" class="tab-pane fade"> <!— for tab 2-->

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Submission Title</th>
                <th>File</th>
                <th>Date Submitted</th>
                <th>Feedback and Grade</th>
                <th>Grade</th>
            </tr>
            </thead>

            <tbody
            <?php
            foreach ($submissions as $submission){
                /* @var $submission \app\models\Submission*/
                ?>
                <tr>
                    <td><?=$submission->getStudentId()?></td>
                    <td><a href="<?=route('students.showRecord',['id'=>$submission->getStudentId()])?>"><?=$submission->getStudent()->getFullName()?></a></td>
                    <td><?=$submission->getTitle()?></td>

                    <td>
                        <?=
                            '<a href="http://'.$submission->getFileUrl().'">Download</a>';
                        ?>
                    </td>

                    <td><?=$submission->getDateSubmitted()?></td>
                    <td><a href="<?=route('submission.showRecord',['id'=>$submission->getSubmissionId()])?>" class="btn btn-default">Mark</a></td>

                    <td>
                        <?php

                            if(empty($submission->getGrade()))
                            {
                                echo 'Not marked';
                            }
                            else
                            {
                                echo $submission->getGrade();
                            }
                        ?>
                    </td>
                </tr>

            <?php
             }
            ?>

            </tbody>
        </table>
    </div>


