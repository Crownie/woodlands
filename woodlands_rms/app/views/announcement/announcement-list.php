<?php $this->layout('layouts/master', ['title' => 'List Announcements']) ?>

<h2 class="page-header">List Announcements</h2>

<p>
    <a href="<?=route('announcements.showRecordForm')?>" class="btn btn-success pull-right">Create announcement</a>
    <div class="clearfix"></div>
</p>

<div class="announcements-container">
    <?php
    foreach ($announcements as $announcement) {
        ?>
        <br>
        <div class="row well">
            <div class="col-sm-12">
                <p><strong><?= $announcement->getTitle() ?></strong></p>
                <div class="pull-right">
                    <?php
                    gen_delete_button(route('announcements.deleteRecord', ['id'=>$announcement->getAnnouncementId()]),'This will delete the announcement record');
                    ?>
                </div>
                <div style="border-top:1px solid #cfcfcf;border-bottom:1px solid #cfcfcf;">
                    <strong>Posted
                        on:</strong> <?= date('l, j F Y h:i A', strtotime($announcement->getDatePosted())) ?> |
                    <strong>Posted by:</strong> Simon White
                </div>
                <p>
                    <?= $announcement->getMessage() ?>
                </p>
            </div>
        </div>


        <?php

    }
    ?>
</div>