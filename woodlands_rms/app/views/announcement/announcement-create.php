<?php $this->layout('layouts/master', ['title' => 'Create Announcement']) ?>

<h2 class="page-header">Create Announcement</h2>
<p>
    <a href="<?=route('announcements.showRecordList')?>" class="pull-left btn btn-success">List</a>
<div class="clearfix"></div>
</p>
<form action="<?=route('announcements.storeRecord')?>" method="POST" id="announcement-form">
    <?php $this->insert('announcement/announcement-form',get_defined_vars()) ?>
</form>

