<?php
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

Form::input('title', '*Announcement Title');
Form::select('announcement_type', '*Announcement Type','',[
    'GENERAL' => 'General',
    'COURSE' => 'Course',
    'MODULE' => 'Module'
]);
Form::select('target', '*Target at','',[
    'BOTH' =>'Staff and Students',
    'STUDENTS'=>'Students only',
    'STAFF'=>'Staff only'
]);
$courseOptions = \app\Controllers\CourseController::getCourseOptions();
Form::searchableSelect('course_id','Course to announce to',[],$courseOptions);
$moduleOptions = \app\Controllers\ModuleController::getModuleOptionsAll();
Form::searchableSelect('module_id','Module to announce to',[],$moduleOptions);
Form::row();
Form::textarea('message','*Message (Limit: 500 characters)');
Form::button('Submit','btn-success');