<?php $this->layout('layouts/master', ['title' => 'Create Event']) ?>

<h2 class="page-header">Create Event</h2>

<form action="<?=route('diary.storeRecord')?>" method="POST">
    <?php $this->insert('diary/diary-form',get_defined_vars()) ?>
</form>