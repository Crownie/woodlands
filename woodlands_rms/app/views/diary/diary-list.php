<?php $this->layout('layouts/master', ['title' => 'List Events']) ?>

<h2 class="page-header">Diary</h2>
<p><a href="<?=route('diary.showRecordForm')?>" class="btn btn-success">Add Event</a></p>


<div id="calendar" data-json-url = "<?=route('diary.getEvents')?>"></div>

<?php $this->insert('diary/diary-modal',get_defined_vars()) ?>