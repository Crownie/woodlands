<!-- Diary Modal -->
<div class="modal fade" id="diary-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Event Details</h4>
            </div>
            <div class="modal-body"><!--modal content-->
                <h4 class="event-title text-center">PT Appointment</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <strong>Target Group:</strong>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-left">Computing Students</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <strong>Start Date:</strong>
                    </div>
                    <div class="col-sm-4">
                        <p class="start-date text-left">2nd December 2016</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <strong>End Date:</strong>
                    </div>
                    <div class="col-sm-4">
                        <p class="end-date text-left">14:30 - 17:30</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <strong>Location:</strong>
                    </div>
                    <div class="col-sm-4">
                        <p class="location text-left">Test data</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <strong>Description:</strong>
                    </div>
                    <div class="col-sm-6">
                        <p class="description text-justify">Lorem ipsum dolor sit amet</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Edit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>