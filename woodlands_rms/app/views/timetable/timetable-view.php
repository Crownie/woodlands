<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 06/03/2017
 * Time: 14:37
 */
use app\models\Timetable;
use app\models\TimetableItem;

$this->layout('layouts/master', ['title' => 'User Profile'])
/**@var $course Course */
?>

<h2 class="page-header">Timetable</h2>

<p>
    <a href="<?=route('timetables.showRecordEditForm',['id'=>$id])?>" class="pull-right btn btn-success">Edit</a>
<div class="clearfix"></div>
</p>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Timetable Details</h1>
    </div>
    <div class="panel-body">
        <dl>
            <dt>Course Name</dt>
            <dd><?=$timetable->getTitle()?></dd>
            <dt>Year</dt>
            <dd><?=$timetable->getYear()?></dd>
        </dl>

    </div>
</div>

