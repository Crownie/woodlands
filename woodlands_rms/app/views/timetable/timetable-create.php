<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 06/03/2017
 * Time: 14:37
 */
 $this->layout('layouts/master', ['title' => 'Create Timetable']);
?>

<ul class="breadcrumb">
    <li><a href="<?=route('timetables.showRecordList')?>">Timetable</a></li>
    <li class="active">Create Timetable</li>
</ul>

<h2 class="page-header">Create Timetable</h2>


<form action="<?=route('timetables.storeRecord')?>" method="POST">
    <?php $this->insert('timetable/timetable-form',get_defined_vars()) ?>
</form>