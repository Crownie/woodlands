<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 06/03/2017
 * Time: 14:39
 */

use app\Controllers\CourseController;
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

/* @var $timetable \app\models\Timetable*/
Form::select('course_id', '*Course', $timetable->getCourseId(), CourseController::getCourseOptions());

Form::select('year', '*Year', $timetable->getYear(),[
    '2016/17' => '2016/17',
    '2017/18' => '2017/18'
    ] );

Form::select('level', '*Level', $timetable->getLevel(), [
    '1'=>'Level 1',
    '2'=>'Level 2',
    '3'=>'Level 3'
]);

Form::button('Submit', 'btn-success');


/*
Form::input('Name', '*Module', $timetable->getModule());

Form::input('module', '*Module', $timetable->getModule());

Form::input('year', '*Year', $timetable->getYear());


Form::input('group', '*Group', $timetable->getGroup(), [
    'A' => 'Group A',
    'B' => 'Group B',
    'C' => 'Group C',
    'D' => 'Group D',
    'E' => 'Group E'
]  );

Form::input('day_of_the_week', 'Day of the Week', $timetable->getDayOfTheWeek(), [
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday'
]  );

Form::input('start_time', '*Start Time', $timetable->geStartTime());

Form::input('room', '*Room', $timetable->getRoom());

Form::input('duration', '*Duration', $timetable->getDuration());

Form::input('lecturer', '*Lecturer', $timetable->getLecturer());

Form::button('Submit', 'btn-success');
*/