<?php $this->layout('layouts/master', ['title' => 'Timetable']) ?>

<ul class="breadcrumb">
    <li class="active">Timetable</li>
</ul>

<h2 class="page-header">Timetable</h2>

<p>
    <?php
    if(authorized('timetables.showRecordForm')) {
        ?>
        <a href="<?=route('timetables.showRecordForm')?>" class="btn btn-success pull-right">Create Timetable</a>
        <?php
    }
    ?>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Timetable ID</th>
        <th>Title</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php

    foreach ($timetables as $timetable){
        $this->insert('timetable/partials/timetable-item',['timetable'=>$timetable]);

    }
    ?>


    </tbody>
</table>
