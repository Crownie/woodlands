<?php
use app\controllers\ModuleController;
use app\controllers\RoomController;
use app\utilities\Form;

/* @var $timetable_item \app\models\TimetableItem */
?>

    <form action="<?= route('timetables.updateItem', ['id' => $item_id]) ?>" method="post">
        <?php
        echo csrf_input();
        Form::select('module_id', '*Module', $timetable_item->getModuleId(), ModuleController::getModuleOptions($timetable->getCourseId()), 'col-sm-4');
        Form::select('timetable_item_type', '*Type', $timetable_item->getTimetableItemType(), ['LECTURE' => 'Lecture', 'PRACTICAL' => 'Practical'], 'col-sm-4');
        Form::select('module_group_number', '*Group', $timetable_item->getModuleGroupNumber(),
            ['0' => 'All Groups', '1' => 'Group 1', '2' => 'Group 2', '3' => 'Group 3', '4' => 'Group 4']
            , 'col-sm-4');
        Form::row();

        $days = array(
            'Monday' => 'Monday',
            'Tuesday' => 'Tuesday',
            'Wednesday' => 'Wednesday',
            'Thursday' => 'Thursday',
            'Friday' => 'Friday',
        );

        Form::select('day', '*Day', $timetable_item->getDay(), $days, 'col-sm-4');

        $hours = [];

        for ($i = 9; $i <= 18; $i++) {
            $hours[$i . ':' . '00:00'] = $i . ':' . '00';
        }

        Form::select('start_time', '*Start Time', $timetable_item->getStartTime(), $hours, 'col-sm-4');
        Form::select('end_time', '*End Time', $timetable_item->getEndTime(), $hours, 'col-sm-4');
        Form::row();
        Form::select('room_id', '*Room', $timetable_item->getRoomId(), RoomController::getRoomOptions(), 'col-sm-4');
        Form::row();

        if(authorized('timetables.updateItem')) {

        ?>
        <div class="col-sm-12">
            <button class="btn btn-success">Update</button>
            <?php
            gen_delete_button(route('timetables.deleteItem', ['id' => $item_id]), 'this will delete the selected ' . $timetable_item->getModule()->getModuleCode() . ' timetable item');
            ?>
        </div>
        <?php
        }else{
            ?>
            <div class="alert alert-info">
                <strong><i class="fa fa-info-circle"></i></strong> To edit timetable item please contact the admin
            </div>
            <?php
        }
        Form::row();
        ?>
    </form>

