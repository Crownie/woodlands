<tr data-id="<?=$timetable->getTimetableId()?>">
    <th><?=$timetable->getTimetableId()?></th>
    <td><?=$timetable->getTitle()?></td>
    <th>
        <a href="<?=route('timetables.showRecordEditForm',['id'=>$timetable->getTimetableId()])?>" class="btn btn-default">View</a>

        <?php
        if(authorized('timetables.showRecordForm')) {
            ?>
            <?php
            gen_delete_button(route('timetables.deleteRecord',['id'=>$timetable->getTimetableId()]),'this will delete the '.$timetable->getTimetableId().' timetable');
            ?>
            <div class="btn btn-default">Archive</div>            <?php
        }
        ?>
    </th>
</tr>