<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 06/03/2017
 * Time: 14:39
 */
use app\controllers\ModuleController;
use app\controllers\RoomController;
use app\models\Timetable;
use app\utilities\Form;

/* @var $timetable Timetable */
$this->layout('layouts/master', ['title' => 'View Timetable']);
?>

<ul class="breadcrumb">
    <li><a href="<?= route('timetables.showRecordList') ?>">Timetable</a></li>
    <li class="active">View Timetable</li>
</ul>

<h2 class="page-header"><?= $timetable->getTitle() ?> Timetable</h2>

<?php
if (authorized('timetables.showRecordForm')) {
    ?>
    <form action="<?= route('timetables.addItem', ['id' => $id]) ?>" method="post">
        <?php
        echo csrf_input();
        Form::select('module_id', '*Module', '', ModuleController::getModuleOptions($timetable->getCourseId()), 'col-sm-4');
        Form::select('timetable_item_type', '*Type', '', ['LECTURE' => 'Lecture', 'PRACTICAL' => 'Practical'], 'col-sm-4');
        Form::select('module_group_number', '*Group', '',
            ['0' => 'All Groups', '1' => 'Group 1', '2' => 'Group 2', '3' => 'Group 3', '4' => 'Group 4']
            , 'col-sm-4');
        Form::row();

        $days = array(
            'Monday' => 'Monday',
            'Tuesday' => 'Tuesday',
            'Wednesday' => 'Wednesday',
            'Thursday' => 'Thursday',
            'Friday' => 'Friday',
        );

        Form::select('day', '*Day', '', $days, 'col-sm-4');

        $hours = [];

        for ($i = 9; $i <= 18; $i++) {
            $hours[$i . ':' . '00'] = $i . ':' . '00';
        }
        Form::select('start_time', '*Start Time', '', $hours, 'col-sm-4');
        Form::select('end_time', '*End Time', '', $hours, 'col-sm-4');
        Form::row();
        Form::select('room_id', '*Room', '', RoomController::getRoomOptions(), 'col-sm-4');
        Form::row();

        Form::button('Add', 'btn btn-success');
        ?>
    </form>
    <?php
}
?>


<div class="pull-right btn btn-default print-element" data-target=".timetable-container">
    <i class="fa fa-print"></i>
    Print Timetable
</div>
<span class="clearfix"></span>

<div class="timetable-container">
    <style>
        @media print {
            .timetable-container h4 {
                display: none;
            }

            .timetable-container h4 {
                display: block;
            }
        }
    </style>
    <h4><?= $timetable->getTitle() ?> Timetable</h4>
    <div class="timetable" data-json-url="<?= route('timetables.getEvents', ['id' => $timetable->getTimetableId()]) ?>"
         data-edit-form-url="<?= route('timetables.getItemEditForm', ['id' => $timetable->getTimetableId()]) ?>"></div>
</div>
<!-- Modal -->
<div class="modal fade" id="timetable-entry-editor-modal" tabindex="-1" role="dialog"
     aria-labelledby="timetable-entry-editor-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Timetable Item</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>