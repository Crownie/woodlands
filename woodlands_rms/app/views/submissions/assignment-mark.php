<?php
use app\controllers\ModuleController;
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

Form::select('grade', 'Student Grade', $submission->getGrade(), [
    'A+' => 'A+',
    'A-' => 'A-',
    'A' => 'A',
    'B+' => 'B+',
    'B-' => 'B-',
    'B' => 'B',
    'C+' => 'C+',
    'C-' => 'C-',
    'C' => 'C',
    'D' => 'D',
    'D+' => 'D+',
    'D-' => 'D-',
    'F' => 'F']);

Form::row();

Form::textarea('feedback', 'Feedback', $submission->getFeedback());

Form::button('Submit','btn btn-success');
