<?php $this->layout('layouts/master', ['title' => 'User Profile']) ?>

<h2 class="page-header">Mark assignment</h2>

<form action="<?=route('submission.updateRecord',['id'=>$id])?>" method="POST" enctype="multipart/form-data">
    <?php $this->insert('submissions/assignment-mark',get_defined_vars()) ?>
</form>

