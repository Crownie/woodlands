<?php
/**
 * Created by Tobi Ayilara.
 * Date: 09/04/2017
 * Time: 22:26
 */
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

</head>
<body>
    <p>PRIVATE &amp; CONFIDENTIAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
    <p>&nbsp;</p>
    <p><?=date('d/m/Y')?></p>
    <p>&nbsp;</p>
    <p>Dear <?=$student->getFullName()?></p>
    <p>&nbsp;</p>
    <p>Your name has been referred as a possible cause for concern by your module co-ordinator because you are recorded as:</p>
    <p>&nbsp;</p>
    <p><?=$issue?></p>
    <p>&nbsp;</p>
    <p>It is essential that you see me as soon as possible.&nbsp; You must do this by <?=date('jS F Y', strtotime(date('Y-m-d'). ' + 7 days'));?>.&nbsp; The meeting will provide an opportunity to check and review your progress and find a means of resolving any difficulties you may be experiencing.&nbsp; You are reminded that all students enrolling on a course at UCN are expected to meet the academic requirements of their programme.&nbsp; The Student Code details the implications of 'failure to meet academic, professional or vocational requirements' and whilst there may be mitigating circumstances not known to us at this time, you should regard this letter as an informal warning that your continuation on the course may be at risk. Failure to make contact may result in us deeming you to be withdrawn from your course.</p>
    <p>&nbsp;</p>
    <p>If for any reason you are experiencing difficulties which impact on your academic work, you should contact your personal tutor for advice.&nbsp; You must however see me as instructed by the Course Administrator.</p>
    <p>&nbsp;</p>
    <p>Yours sincerely</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>Module Co-Ordinater</p>
    <p>&nbsp;</p>
    <p>CC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Student file / Course Leader&nbsp;&nbsp;</p>
    <p>CAUSE FOR CONCERN LETTER&nbsp;</p>
</body>
</html>