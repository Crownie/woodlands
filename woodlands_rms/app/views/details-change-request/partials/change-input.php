<?php
/**
 * Created by Tobi Ayilara.
 * Date: 10/04/2017
 * Time: 21:11
 */
$value = isset($data[$field_name])?$data[$field_name]:'';
$student_data = $student->toArray();
if(isset($student_data[$field_name]) && strcmp($value,$student_data[$field_name])) {


    ?>

    <div class="col-sm-6">
        <div class="input-group details-change-input">
            <input type="text" class="form-control" data-target-field="<?= $field_name ?>" readonly
                   value="<?= $value ?>">
            <span class="input-group-btn">
                <button class="btn btn-success accept-change" type="button">
                    <i class="fa fa-check"></i>
                </button>
        <button class="btn btn-danger reject-change" type="button">
            <i class="fa fa-times"></i>
        </button>
      </span>
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
    <?php
}