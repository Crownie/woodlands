<?php
use app\models\Student;
?>
<tr data-id="<?=$request->getStudentId() ?>">
    <td><?= $request->getStudentId() ?></td>
    <td><?= Student::get($request->getStudentId())->getFullName() ?></td>
    <td><?= $request->getStatus() ?></td>
    <td>
        <a href="<?= route('details-change-requests.reviewChangeRequest',['id'=>$request->getStudentId()])?>" class="btn btn-default">View</a>
    </td>

</tr>