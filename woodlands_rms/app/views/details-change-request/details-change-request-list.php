<?php


$this->layout('layouts/master', ['title' => 'List Details Change Requests'])?>


<h2 class="page-header">List Details Change Requests</h2>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Student ID</th>
        <th>Student Name</th>
        <th>Status</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($requests as $request){
        $this->insert('details-change-request/partials/details-change-request-item',['request'=>$request]);
    }
    ?>


    </tbody>
</table>