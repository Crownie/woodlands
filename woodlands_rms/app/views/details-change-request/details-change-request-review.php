<?php
use app\models\Student;
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'Review Details Change Request']);
$student = Student::get($request->getStudentId());
?>

<h4>Contact Information</h4>
<form id="student-form" action="" method="POST">
    <?php
    echo csrf_input();
    //////////////////////////////////////////////////////////////////////////////////////////////
    Form::input('phone', '*Contact Number', $student->getPhone());
    Form::row();
    Form::detailsChangeInput('phone', $data,get_defined_vars());
    Form::row();
    Form::input('email', '*Email Address', $student->getEmail());
    Form::row();

    Form::detailsChangeInput('email', $data,get_defined_vars());
    Form::row();

    ?>

    <h4>Term-time Address</h4>
    <?php
    Form::input('term_time_address_line1', '*Address Line 1', $student->getTermTimeAddressLine1());
    Form::row();
    Form::detailsChangeInput('term_time_address_line1', $data,get_defined_vars());
    Form::row();

    Form::input('term_time_address_line2', 'Address Line 2', $student->getTermTimeAddressLine2());
    Form::row();
    Form::detailsChangeInput('term_time_address_line2', $data,get_defined_vars());
    Form::row();

    Form::input('term_time_city', '*City', $student->getTermTimeCity());
    Form::row();
    Form::detailsChangeInput('term_time_city', $data,get_defined_vars());
    Form::row();

    Form::input('term_time_country', '*Country', $student->getTermTimeCountry());
    Form::row();
    Form::detailsChangeInput('term_time_country', $data,get_defined_vars());
    Form::row();

    Form::input('term_time_postcode', '*Postcode', $student->getTermTimePostcode());
    Form::row();
    Form::detailsChangeInput('term_time_postcode', $data,get_defined_vars());
    ?>
    <hr>
    <h4>Non Term-time Address</h4>
    <?php


    Form::checkbox('both_addresses_same', '', $student->isAddressTheSame(), [
        'yes' => 'Same as term-time address'
    ], 'col-sm-6 both_addresses_same');
    Form::row();
    Form::input('address_line1', '*Address Line 1', $student->getAddressLine1());
    Form::row();
    Form::detailsChangeInput('address_line1', $data,get_defined_vars());
    Form::row();

    Form::input('address_line2', 'Address Line 2', $student->getAddressLine2());

    Form::row();
    Form::detailsChangeInput('address_line2', $data,get_defined_vars());

    Form::row();
    Form::input('city', '*City', $student->getCity());
    Form::row();
    Form::detailsChangeInput('city', $data,get_defined_vars());
    Form::row();

    Form::input('country', '*Country', $student->getCountry());
    Form::row();

    Form::detailsChangeInput('country', $data,get_defined_vars());
    Form::row();

    Form::input('postcode', '*Postcode', $student->getPostcode());
    Form::row();
    Form::detailsChangeInput('postcode', $data,get_defined_vars());
    Form::row();


    ?>
    <hr>
    <?php
    Form::button('Submit', 'btn btn-success');
    ?>
</form>
<br>