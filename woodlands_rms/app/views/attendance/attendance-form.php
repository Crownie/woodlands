<?php
use app\utilities\Form;
?>

<?= csrf_input() ?>
<?php
Form::select('year', 'Academic year', '', [
    '2016' => '2016/17',
    '2017' => '2017/18',
    '2018' => '2018/19']);

Form::select('week', 'Week', '', [
    '13' => '13',
    '14' => '14',
    '15' => '15']);

Form::select('group', 'Group', '', [
    '1' => '1',
    '2' => '2',
    '3' => '3']);