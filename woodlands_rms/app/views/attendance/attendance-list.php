<?php
use app\models\Attendance;
use app\utilities\Form;
    csrf_input();

    $this->layout('layouts/master', ['title' => 'List Attendance']);

?>

<h2 class="page-header">Attendance</h2>

<form action="" method="get">
    <div class="row">

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                    <br/>

                    <?php
                    Form::select('module', 'Module', $module_id, $module_options);
                    Form::row();
                    ?>

                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                    <br/>

                    <?php
                    Form::select('year', 'Academic year', $year, [
                        '2016' => '2016/17',
                        '2017' => '2017/18',
                        '2018' => '2018/19']);
                    Form::row();
                    ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                    <br/>

                    <?php
                    $week_options = [];


                    for($i = 0; $i <= 32; $i++ ){
                        $week_options[''.$i] = $i;
                    }

                    Form::select('week', 'Week', $week, $week_options);
                    Form::row();
                    ?>

                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                    <br/>

                    <?php
                    Form::select('group', 'Group', $group_number, [
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5']);
                    Form::row();
                    ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-success pull-right">Search</button>
            <span class="clearfix"></span>
            <br>
        </div>
    </div>

</form>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <table class="table table-striped" width="1000" align="center">
                    <tr><!--headings for table-->
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Surname</th>
                        <th>Lecture</th>
                        <th>Practical</th>
                    </tr>

                    <?php foreach($student_module_groups as $student_module_group){
                        $student = $student_module_group->getStudent();

                        ?>
                    <tr>
                        <td><?=  $student->getStudentId(); ?></td>
                        <td><?=  $student->getFirstName(); ?></td>
                        <td><?=  $student->getSurname(); ?></td>
                        <td>
                            <?php
                            $attendance_type = 'lecture';
                            $this->insert('attendance/partials/attendance-radio-button',get_defined_vars());  ?>
                        </td>
                        <td>
                            <?php
                            $attendance_type = 'practical';
                            $this->insert('attendance/partials/attendance-radio-button',get_defined_vars());  ?>
                        </td>
                    </tr>
                    <?php } ?>





                </table>
            </div>
        </div>
    </div>





