<?php use app\utilities\Form;

$this->layout('layouts/blank', ['title' => 'Login']) ?>
<style>
    body {
        background-image: url(<?=asset('images/woodland-tile-pattern.png')?>);
        background-repeat: repeat;
    }
</style>
<div class="login-logo">

</div>
<div class="login-box">
    <div class="text-center">Enter your username and password</div>
    <hr>
    <form method="POST">
        <?=csrf_input()?>
        <?php
            Form::input('username','*Username','99100012',[],'col-sm-12');
            Form::input('password','*Password','staff',['type'=>'password'],'col-sm-12');
        ?>

        <div class="col-sm-12">
            <button type="submit" class="btn btn-block btn-lg btn-success pull-right">Login</button>
            <span class="clearfix"></span>
        </div>
        <div class="clearfix"></div>
    </form>
<div class="well">u:(e.g. 99100012);p:staff</div>
</div>