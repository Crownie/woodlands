<?php use app\models\Student;
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'Timetable For Tutor']);
?>
<form action="" method="get">
    <?php
    Form::select('level', '*Level', $lev, [
            '-'=>'All',
        '1'=>'Level 1',
        '2'=>'Level 2',
        '3'=>'Level 3'
    ]);
    Form::button('Search', 'btn btn-default');
    Form::row();
    ?>
</form>

    <?php
    foreach ($personal_tutors as $personal_tutor) {
        if(intval($lev)<=0){
            $students = Student::find('personal_tutor_id=:p', ['p' => $personal_tutor->getStaffId()]);
        }else{
            $students = Student::find('personal_tutor_id=:p AND level=:l', ['p' => $personal_tutor->getStaffId(),'l'=>$lev]);
        }

        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title"><?=$personal_tutor->getFullName()?> <span class="pull-right">[ <?=count($students)?> ] students</span></h1>
            </div>
            <div class="panel-body">
                <ul>
                    <?php
                    foreach ($students as $student) {
                        ?>
                        <li><?= $student->getFullName() ?></li>

                        <?php
                    }?>
                </ul>
            </div>
        </div>
            <?php
    }
    ?>
