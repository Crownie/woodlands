<?php $this->layout('layouts/master', ['title' => 'Reports']);

$reports = [
    'Timetable' => [
        'Timetable for individual Tutor' => route('reports.reportTimetableForTutor'),
        'Timetable for individual Student' => route('reports.reportTimetableForStudent'),
        'Current timetable for year group' => route('timetables.showRecordList')
    ],
    'Personal Tutors' => [
        'List of tutees by tutor' => route('reports.reportTuteesByTutor'),
        'Timetable for individual Student' => route('reports.reportTimetableForStudent'),
        'Current timetable for year group' => route('timetables.showRecordList')
    ],
];

foreach ($reports as $title => $report) {
    $this->insert('report/partials/report-item', get_defined_vars());
}


?>

