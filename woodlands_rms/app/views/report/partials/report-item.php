<?php
/**
 * Created by Tobi Ayilara.
 * Date: 12/04/2017
 */
?>

<div class="col-sm-6">
    <div class="widget panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?=$title?></h3>
        </div>
        <div class="panel-body">
            <div class="list-group">
                <?php

                foreach ($report as $name=>$url){
                    ?>

                    <a href="<?=$url?>" class="list-group-item">
                        <h4 class="list-group-item-heading"><?=$name?></h4>
                    </a>

                    <?php
                }

                ?>
            </div>
        </div>
    </div>
</div>
