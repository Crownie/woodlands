<?php use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'Timetable For Tutor']);
?>
    <h2 class="page-header">Timetable For Individual Student</h2>

    <form action="" method="get">
        <?php
        Form::searchableSelect('student_id','Student',$student_id,\app\controllers\StudentController::getStudentOptionsAll());
        Form::button('Show Timetable','btn btn-default');
        ?>
    </form>
<?php

    if(!empty($student_id)){
        $student = \app\models\Student::get($student_id);
        ?>
        <div class="pull-right btn btn-default print-element" data-target=".timetable-container">
            <i class="fa fa-print"></i>
            Print Timetable
        </div>
        <span class="clearfix"></span>
        <div class="timetable-container">
        <h4>Timetable for <?=$student->getFullName()?></h4>
        <div class="timetable" data-json-url="<?=route('timetables.getStudentEvents',['student_id'=>$student_id])?>" data-edit-form-url=""></div>
        </div>
        <?php
    }

?>