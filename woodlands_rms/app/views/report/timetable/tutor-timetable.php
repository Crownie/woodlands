<?php use app\models\Staff;
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'Timetable For Tutor']);

?>
<h2 class="page-header">Timetable For Individual Tutor</h2>
<form action="" method="get">
    <?php
    Form::searchableSelect('staff_id','Staff',$staff_id,\app\controllers\StaffController::getModuleLeaders());
    Form::button('Show Timetable','btn btn-default');
    ?>
</form>
<?php

if(!empty($staff_id)) {
    $staff = Staff::get($staff_id);
    ?>
    <div class="pull-right btn btn-default print-element" data-target=".timetable-container">
        <i class="fa fa-print"></i>
        Print Timetable
    </div>
<span class="clearfix"></span>
    <div class="timetable-container">
        <h4>Timetable for <?=$staff->getFullName()?></h4>
        <div class="timetable" data-json-url="<?= route('timetables.getTutorTimetableItems', ['staff_id' => 99100991]) ?>"
             data-edit-form-url=""></div>
    </div>
    <?php
}