<?php
/**
 * Created by Alex
 */

use app\Controllers\CourseController;
use app\controllers\PersonalTutorController;
use app\models\Student;
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'Display Personal Tutors'])
?>

<h2 class="page-header">Personal tutors</h2>

<form action="" method="get">
    <?php
    Form::select('department', '*Department', $department_id, PersonalTutorController::getDepartmentOptions());
    Form::select('level', '*Level', $lev, [
        '1'=>'Level 1',
        '2'=>'Level 2',
        '3'=>'Level 3'
    ]);

    Form::button('Search','btn btn-success');

    ?>
</form>

<div class="clearfix"></div>
<hr/>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php
    foreach($personal_tutors as $personal_tutor){
        /* @var $personal_tutor \app\models\PersonalTutor*/
        $students=Student::getStudentsByDeptAndLevel($department_id,$lev,$personal_tutor->getStaffId());
        $this->insert('personal-tutor/partials/personal-tutor-panel',get_defined_vars());
    }
    ?>
</div>
