<?php $this->layout('layouts/master', ['title' => 'List Modules']) ?>
<?php use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'User Profile']) ?>

<ul class="breadcrumb">
    <li class="active">Modules</li>
</ul>

<h2 class="page-header">Modules</h2>

<form action="" method="get">

    <div class="panel panel-default">
        <br>
        <?php
        Form::input('search', 'Search', $search, ['placeholder' => 'Enter Module Code or Title'], 'col-sm-6');
        Form::select('status', 'Filter by status', $status, ['LIVE' => 'LIVE', 'DORMANT' => 'DORMANT']);
        ?>

        <div class="col-sm-12">
            <button class="btn btn-default pull-right" type="submit">
                <i class="fa fa-search"></i>
                Search
            </button>
        </div>
        <span class="clearfix"></span>
        <br>
    </div>
    <span class="clearfix"></span>
</form>

<p>
    <?php
    if(authorized('modules.showRecordForm')) {
        ?>
        <a href="<?=route('modules.showRecordForm')?>" class="btn btn-success pull-right">Create module</a>
        <?php
    }
    ?>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Module ID</th>
        <th>Module Name</th>
        <th>Module Level</th>
        <th>Module Credits</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($modules as $module){
        $this->insert('module/partials/module-item',['module'=>$module]);
    }
    ?>


    </tbody>
</table>


