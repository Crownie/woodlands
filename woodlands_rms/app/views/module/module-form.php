<?php
use app\utilities\Form;
?>

<?= csrf_input() ?>
<?php
Form::input('module_code', '*Module code', $module->getModuleCode());
Form::input('title', '*Module title', $module->getTitle());
Form::select('level', '*Module level', $module->getLevel(), [
    '1' => 'Level 1',
    '2' => 'Level 2',
    '3' => 'Level 3']);
Form::input('credits', '*Module credits', $module->getCredits(), ['type' => 'number']);
Form::input('ass1', 'Assessment 1 Weighting(%)', $module->getAss1(), ['type' => 'number']);
Form::input('ass2', 'Assessment 2 Weighting(%)', $module->getAss2(), ['type' => 'number']);
Form::input('exam', 'Exam Weighting', $module->getExam(), ['type' => 'number']);
Form::row();
$moduleLeaders = \app\controllers\StaffController::getModuleLeaders();
Form::searchableSelect('module_leader_id','Select a module leader',[],$moduleLeaders);
Form::button('Submit', 'btn-success');
?>


