<?php use app\models\Course;
use app\models\CourseModule;

$this->layout('layouts/master', ['title' => 'View Module'])
/**@var $module Module */
?>

<ul class="breadcrumb">
    <li><a href="<?=route('modules.showRecordList')?>">Modules</a></li>
    <li class="active">View Module</li>
</ul>

<h2 class="page-header">View Module</h2>

<p>
    <a href="<?=route('modules.showRecordEditForm',['id'=>$id])?>" class="pull-right btn btn-success">Edit</a>
<div class="clearfix"></div>
</p>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Module Details</h1>
    </div>
    <div class="panel-body">
        <dl>
            <dt>Module code</dt>
            <dd><?=$module->getModuleCode()?></dd>
            <dt>Title</dt>
            <dd><?=$module->getTitle()?></dd>
            <dt>Level</dt>
            <dd><?=$module->getLevel()?></dd>
            <dt>Credits</dt>
            <dd><?=$module->getCredits()?></dd>
            <dt>Assessment 1</dt>
            <dd><?=$module->getAss1()?></dd>
            <dt>Asessment 2</dt>
            <dd><?=$module->getAss2()?></dd>
            <dt>Exam details</dt>
            <dd><?=$module->getExam()?></dd>
            <?php

                if (!empty($module->getModuleLeader())) {
                    echo '<dt>Module leader</dt>';
                    echo '<dd><a href="' . route('staff.showRecord', ['id' => $module->getModuleLeader()->getStaffId()]) . '">' . $module->getModuleLeader()->getFullName() . '</a></dd>';
                }
               ?>
        </dl>

    </div>
</div>
