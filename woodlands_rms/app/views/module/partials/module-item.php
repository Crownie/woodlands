<tr data-id="<?=$module->getModuleId()?>">
    <th><?=$module->getModuleCode()?></th>
    <td><?=$module->getTitle()?></td>
    <td><?=$module->getLevel()?></td>
    <td><?=$module->getCredits()?></td>

    <th>
        <a href="<?=route('modules.showRecord',['id'=>$module->getModuleId()])?>" class="btn btn-default">View</a>


        <?php
        if (authorized('modules.showRecordForm')) {
            ?>
            <a href="<?=route('modules.showRecordEditForm',['id'=>$module->getModuleId()])?>" class="btn btn-default">Edit</a>
            <?php
            //gen_delete_button(route('modules.deleteRecord',['id'=>$module->getModuleId()]),'this will delete the '.$module->getTitle().' module');
            ?>
            <form style="display: inline-block" action="<?=route('modules.archive',['id'=>$module->getModuleId()]) ?>" method="post">
                <?=csrf_input()?>
                <button type="submit" class="btn btn-default">Archive</button>
            </form>
            <?php
        }
        ?>
    </th>
</tr>