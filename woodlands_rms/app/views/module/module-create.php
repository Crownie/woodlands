<?php $this->layout('layouts/master', ['title' => 'Create Module']) ?>

<ul class="breadcrumb">
    <li><a href="<?=route('modules.showRecordList')?>">Modules</a></li>
    <li class="active">Create Module</li>
</ul>

<h2 class="page-header">Create module</h2>

<form action="<?=route('modules.storeRecord')?>" method="POST">
    <?php $this->insert('module/module-form',get_defined_vars()) ?>
</form>