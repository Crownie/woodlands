<?php
$this->layout('layouts/master', ['title' => 'Edit Module']) ?>

<ul class="breadcrumb">
    <li><a href="<?=route('modules.showRecordList')?>">Modules</a></li>
    <li><a href="<?=route('modules.showRecord',['id'=>$id])?>">View Module</a></li>
    <li class="active">Edit Module</li>
</ul>

<h2 class="page-header">Edit module</h2>

<form action="<?=route('modules.updateRecord',['id'=>$id])?>" method="POST">
    <?php $this->insert('module/module-form',get_defined_vars()) ?>
</form>