<div class="sidebar">
    <ul class="nav sidebar-nav">
        <?php
        sidebarMenuItem('Dashboard', '/', 'tachometer');

        sidebarMenuItem('Course', route('courses.showRecordList'), 'graduation-cap', [
            ['title' => 'Create Course', 'url' => route('courses.showRecordForm')],
            ['title' => 'List Courses', 'url' => route('courses.showRecordList')]
        ]);

        sidebarMenuItem('Module', route('modules.showRecordList'), 'book', [
            ['title' => 'Create Module', 'url' => route('modules.showRecordForm')],
            ['title' => 'List Modules', 'url' => route('modules.showRecordList')],
            ['title' => 'Module Groups', 'url' => route('module-groups.showRecordList').'?module_id=1']
        ]);

        sidebarMenuItem('Student', route('students.showRecordList'), 'user', [
            ['title' => 'Create Student', 'url' => route('students.showRecordForm')],
            ['title' => 'List Students', 'url' => route('students.showRecordList')],
            ['title' => 'List Requests', 'url' => route('details-change-requests.showChangeRequests')]
        ]);

        sidebarMenuItem('Staff', route('staff.showRecordList'), 'user', [
            ['title' => 'Create Staff', 'url' => route('staff.showRecordForm')],
            ['title' => 'List Staff', 'url' => route('staff.showRecordList')]
        ]);

        sidebarMenuItem('Personal Tutor', route('personal-tutors.showRecordList').'?department=&level=', 'user');

        sidebarMenuItem('Announcement', route('announcements.showRecordList'),'bullhorn');

        sidebarMenuItem('Assignment', route('assignments.showRecordList'), 'file-text', [
            ['title' => 'Create Assignment', 'url' => route('assignments.showRecordForm')],
            ['title' => 'List Assignment', 'url' => route('assignments.showRecordList')]
        ]);

        sidebarMenuItem('Timetable', route('timetables.showRecordList'), 'th-list');

        sidebarMenuItem('Rooms', route('rooms.showRecordList'), 'th-list');

        sidebarMenuItem('Attendance', route('attendance.showRecordList').'?module=&year=&week=&group=', 'tasks');

        sidebarMenuItem('Diary', route('diary.showRecordList'), 'calendar');

        sidebarMenuItem('Report', route('reports.listReports'), 'line-chart');
        ?>
    </ul>
</div>