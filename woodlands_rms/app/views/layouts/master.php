<!doctype html>
<html lang="en">
<head>
    <title>Woodlands RMS: <?= $title ?></title>
    <?php use app\controllers\AuthController;

    $this->insert('layouts/partials/head') ?>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <div class="logo"></div>
                <span class="clearfix"></span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?=route('details-change-requests.showChangeRequests')?>"> <i class="fa fa-2x fa-bell"></i>
                    <?php
                    $c = \app\controllers\DetailsChangeRequestController::countPendingRequests();
                        if($c>0){
                            echo '<span class="badge">'.$c.'</span>';
                        }
                    ?>
                    </a></li>
                <li id="logged-in-user" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <div class="profile-pic pull-left"></div>
                        <div class="logged-in-user-name pull-left" title="<?=AuthController::getLoggedInStaff()->getFullName()?>"><?=AuthController::getLoggedInStaff()->getFullName()?></div>
                        <span class="caret pull-right"></span>
                        <span class="clearfix"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=route('staff.showRecord',['id'=>AuthController::getLoggedInStaff()->getStaffId()])?>"><i class="fa fa-user"></i> My Profile</a></li>
                        <li><a href="#"><i class="fa fa-question"></i> Help</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?=route('logout')?>"><i class="fa fa-sign-out"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php $this->insert('layouts/partials/sidebar') ?>
<main>
    <?=$this->section('content')?>
</main>

<?php $this->insert('partials/multiple-select-modal') ?>
<?php $this->insert('layouts/partials/foot') ?>
</body>
</html>