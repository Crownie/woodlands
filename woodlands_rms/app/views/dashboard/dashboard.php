<?php $this->layout('layouts/master', ['title' => 'Dashboard']) ?>
<h2 class="page-header">Dashboard</h2>

<div class="row">
    <div class="col-sm-6">
        <div class="widget panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Events</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">PT Appointment</h4>
                        <p class="list-group-item-text">2nd June 2017 - 12:00</p>
                    </a>
                    <a href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">PT Appointment</h4>
                        <p class="list-group-item-text">2nd June 2017 - 12:00</p>
                    </a>
                    <a href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">PT Appointment</h4>
                        <p class="list-group-item-text">2nd June 2017 - 12:00</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="widget panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Notifications</h3>
            </div>
            <div class="panel-body">
                No notifications
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="widget panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Activity Log</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">Dr Raj added new assignment</h4>
                        <p class="list-group-item-text">appointment with John Doe</p>
                    </a>
                    <a href="#" class="list-group-item">
                        <h4 class="list-group-item-heading">Dr Raj archived</h4>
                        <p class="list-group-item-text">appointment with John Doe</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>