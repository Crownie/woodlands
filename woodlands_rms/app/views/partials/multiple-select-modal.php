<!-- Modal -->
<div class="modal fade" id="multiple-select-modal" tabindex="-1" role="dialog" aria-labelledby="multiple-select-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">

                        <select multiple="multiple" id="multiple-select" name="my-select[]">
                        </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success btn-multiple-select-modal-save">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>