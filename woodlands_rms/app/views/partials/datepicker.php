<div class="<?=$parent_class_attr?> form-group">
    <label ><?=$label?></label>
    <div class="form-group">
        <div class="input-group date" id="<?=$element_id?>">
            <input <?=$required?> name="<?=$field_name?>" value="<?=form_data($field_name, $default)?>" type="text" class="form-control" />
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-<?=$icon?>"></span>
                    </span>
        </div>
        <?=$field_error_message?>
    </div>
</div>

<script type="text/javascript">

    if (document.readyState === "complete" || document.readyState === "loaded") {
        $('#<?=$element_id?>').datetimepicker(<?=$js_options_arr?>);
    }else{
        document.addEventListener('DOMContentLoaded', function(){
            $(function () {
                $('#<?=$element_id?>').datetimepicker(<?=$js_options_arr?>);
            });
        }, false);
    }

</script>