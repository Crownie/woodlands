<?php
/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 13/03/2017
 * Time: 13:46
 */
?>
<tr data-id="<?=$room->getRoomId()?>">
    <th><?=$room->getRoomNumber()?></th>
    <td><?=$room->getRoomType()?></td>
    <td><?=$room->getCapacity()?></td>
<th>
    <?php
    if(authorized('rooms.showRecordForm')) {
        gen_delete_button(route('rooms.deleteRecord',['id'=>$room->getRoomId()]),'this will delete the '.$room->getRoomNumber().' Room');
    }
    ?>
</th>
</tr>