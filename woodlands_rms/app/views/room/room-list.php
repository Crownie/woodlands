<?php $this->layout('layouts/master', ['title' => 'Rooms']) ?>

<h2 class="page-header">Rooms</h2>

<p>
    <?php
    if(authorized('rooms.showRecordForm')) {
        ?>
        <a href="<?=route('rooms.showRecordForm')?>" class="btn btn-success pull-right">Create Room</a>
        <?php
    }
    ?>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Room Number</th>
        <th>Room Type</th>
        <th>Capacity</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($rooms as $room){
        $this->insert('room/partials/room-item',['room'=>$room]);
    }
    ?>
    </tbody>
</table>