<?php $this->layout('layouts/master', ['title' => 'Edit Staff'])?>

<ul class="breadcrumb">
    <li><a href="<?=route('staff.showRecordList')?>">Staff</a></li>
    <li><a href="<?=route('staff.showRecord',['id'=>$id])?>">View Staff</a></li>
    <li class="active">Edit Staff</li>
</ul>

<h2 class="page-header">Edit Staff</h2>

<form action="<?=route('staff.updateRecord',['id'=>$id])?>" method="POST">
    <?php $this->insert('staff/staff-form',get_defined_vars()) ?>
</form>
