<?php
use app\models\Staff;

$this->layout('layouts/master', ['title' => 'View Staff']);

?>

<ul class="breadcrumb">
    <li><a href="<?=route('staff.showRecordList')?>">Staff</a></li>
    <li class="active">View Staff</li>
</ul>

<h2 class="page-header">View Staff Member</h2>

<p>
    <a href="<?=route('staff.showRecordEditForm',['id'=>$id])?>" class="pull-right btn btn-success">Edit</a>
    <a href="<?=route('staff.showRecordList')?>" class="pull-left btn btn-success">List</a>
<div class="clearfix"></div>
</p>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Staff Details</h1>
    </div>
    <div class="panel-body">
        <dl>
            <dt>Staff ID</dt>
            <dd><?=$staff->getStaffId()?></dd>
            <dt>First Name</dt>
            <dd><?=$staff->getFirstName()?></dd>
            <dt>Middle Name</dt>
            <dd><?=$staff->getMiddleName()?></dd>
            <dt>Surname</dt>
            <dd><?=$staff->getSurname()?></dd>
            <dt>Address Line 1</dt>
            <dd><?=$staff->getAddressLine1()?></dd>
            <dt>Address Line 2</dt>
            <dd><?=$staff->getAddressLine2()?></dd>
            <dt>City</dt>
            <dd><?=$staff->getCity()?></dd>
            <dt>Country</dt>
            <dd><?=$staff->getCountry()?></dd>
            <dt>Postcode</dt>
            <dd><?=$staff->getPostcode()?></dd>
            <dt>Phone number</dt>
            <dd><?=$staff->getPhone()?></dd>
            <dt>Email</dt>
            <dd><?=$staff->getEmail()?></dd>
            <dt>Record Status</dt>
            <dd><?=$staff->getStatus()?></dd>
            <dt>Dormancy Reason</dt>
            <dd><?=$staff->getDormancyReason()?></dd>
            <dd><?php
                $roles = $staff->getRoles();
                if(!empty($roles)) {
                    echo '<dt>Staff Roles</dt>';
                    $roleNames = ['PT' => 'Personal Tutor', 'ML' => 'Module Leader', 'CL' => 'Course Leader'];
                    for ($i = 0; $i < count($roles); $i++) {
                        if (isset($roles[$i + 1])) {
                            echo $roleNames[$roles[$i]] . ', ';
                        } else {
                            echo $roleNames[$roles[$i]];
                        }
                    }
                }
                ?></dd>
        </dl>
    </div>
</div>
