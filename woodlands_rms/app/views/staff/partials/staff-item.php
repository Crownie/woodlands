<tr data-id="<?=$staff->getStaffId()?>">
    <th><?=$staff->getStaffId()?></th>
    <th><?=$staff->getFirstName()?></th>
    <td><?=$staff->getSurname()?></td>
    <td><?=$staff->getPhone()?></td>
    <td><?=$staff->getEmail()?></td>
    <td>
        <a href="<?=route('staff.showRecord',['id'=>$staff->getStaffId()])?>" class="btn btn-default">View</a>
        <?php
        if(authorized('staff.showRecordForm')) {
            ?>
            <a href="<?=route('staff.showRecordEditForm',['id'=>$staff->getStaffId()])?>" class="btn btn-default">Edit</a>

            <div data-staff-id="<?=$staff->getStaffId()?>" class="btn btn-default staff-archive-btn" data-toggle="modal" data-target="#staff-archive-modal">Archive</div>

            <?php
        }
        ?>
    </td>
</tr>