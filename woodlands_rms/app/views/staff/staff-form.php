<?php
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php

//Form::input('staff_id', '*Staff ID', $staff->getStaffId());
Form::input('first_name', '*First Name', $staff->getFirstName());
Form::input('middle_name', 'Middle Name', $staff->getMiddleName());
Form::input('surname', '*Surname', $staff->getSurname());
Form::input('address_line1', '*Address Line 1', $staff->getAddressLine1());
Form::input('address_line2', 'Address Line 2', $staff->getAddressLine2());
Form::input('city', '*City', $staff->getCity());
Form::input('country', '*Country', $staff->getCountry());
Form::input('postcode', '*Postcode', $staff->getPostcode());
Form::input('phone', '*Contact Number', $staff->getPhone());
Form::input('email', '*Email Address', $staff->getEmail());
Form::checkbox('roles', '*Role(s)', $staff->getRoles(), [
    'ML' => 'Module Leader',
    'PT' => 'Personal Tutor',
    'CL' => 'Course Leader',]);
Form::button('Submit', 'btn-success');
