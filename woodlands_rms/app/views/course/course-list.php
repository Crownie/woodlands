<?php use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'List Courses']) ?>
<ul class="breadcrumb">
    <li class="active">Courses</li>
</ul>
<h2 class="page-header">Courses</h2>

<p>
<?php
    if(authorized('courses.showRecordForm')) {
        ?>
        <a href="<?=route('courses.showRecordForm')?>" class="btn btn-success pull-right">Create Course</a>
        <?php
    }
?>
<div class="clearfix"></div>
</p>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Course Code</th>
        <th>Title</th>
        <th>Duration</th>
        <th>Cost</th>
        <th>Start Year</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($courses as $course){
        $this->insert('course/partials/course-item',['course'=>$course]);
    }
    ?>


    </tbody>
</table>



