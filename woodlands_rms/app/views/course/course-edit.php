<?php $this->layout('layouts/master', ['title' => 'Edit Course']) ?>

<ul class="breadcrumb">
    <li><a href="<?=route('courses.showRecordList')?>">Courses</a></li>
    <li><a href="<?=route('courses.showRecord',['id'=>$id])?>">View Course</a></li>
    <li class="active">Edit Course</li>
</ul>

<h2 class="page-header">Edit Course</h2>

<form action="<?=route('courses.updateRecord',['id'=>$id])?>" method="POST">
    <?php $this->insert('course/course-form',get_defined_vars()) ?>
</form>
