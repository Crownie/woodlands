<?php use app\models\Course;
use app\models\CourseModule;

$this->layout('layouts/master', ['title' => 'View Course'])
/**@var $course Course */
?>

<ul class="breadcrumb">
    <li><a href="<?=route('courses.showRecordList')?>">Courses</a></li>
    <li class="active">View Course</li>
</ul>
<h2 class="page-header">Course</h2>

<p>
    <a href="<?=route('courses.showRecordEditForm',['id'=>$id])?>" class="pull-right btn btn-success">Edit</a>
<div class="clearfix"></div>
</p>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Course Details</h1>
    </div>
    <div class="panel-body">
        <dl>
            <dt>Course Title</dt>
            <dd><?=$course->getTitle()?></dd>
            <dt>Duration</dt>
            <dd><?=$course->getDuration()?></dd>
            <dt>Cost</dt>
            <dd><?='£'.$course->getCost()?></dd>
            <dt>UCAS points</dt>
            <dd><?=$course->getUcasPoints()?></dd>
            <dt>Start Year</dt>
            <dd><?=$course->getStartYear()?></dd>
            <dt>Description</dt>
            <dd><?=$course->getDescription()?></dd>
        </dl>

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Modules</h1>
    </div>
    <div class="panel-body">
        <ul>
        <?php
           $course_modules = $course->getCourseModules();
            foreach ($course_modules as $course_module){
                /* @var $course_module CourseModule*/
                echo '<li>'.$course_module->getModule()->getTitle().'</li>';
            }
        ?>
        </ul>
    </div>
</div>