<?php $this->layout('layouts/master', ['title' => 'Edit Student'])?>

<ul class="breadcrumb">
    <li><a href="<?=route('students.showRecordList')?>">Students</a></li>
    <li><a href="<?=route('students.showRecord',['id'=>$id])?>">View Student</a></li>
    <li class="active">Edit Student</li>
</ul>

<h2 class="page-header">Edit Student</h2>
<p>
    <a href="<?=route('students.showRecordList')?>" class="pull-left btn btn-success">List</a>
<div class="clearfix"></div>
</p>
<form action="<?=route('students.updateRecord',['id'=>$id])?>" method="POST" id="student-form">

    <?php $this->insert('student/student-form',get_defined_vars()) ?>
</form>
