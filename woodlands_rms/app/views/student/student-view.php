<?php
use app\models\Student;

$this->layout('layouts/master', ['title' => 'View Student']);

?>

<ul class="breadcrumb">
    <li><a href="<?= route('students.showRecordList') ?>">Students</a></li>
    <li class="active">View Student</li>
</ul>

<h2 class="page-header">View Student</h2>

<p>
    <a href="<?= route('students.showRecordEditForm', ['id' => $id]) ?>" class="pull-right btn btn-success">Edit</a>
    <a href="<?= route('students.showRecordList') ?>" class="pull-left btn btn-success">List</a>
<div class="clearfix"></div>
</p>


<div class="panel panel-default">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#menu1">Student details</a></li>
        <li><a data-toggle="tab" href="#menu2">Courses</a></li>
        <li><a data-toggle="tab" href="#menu3">Qualifications</a></li>
        <li><a data-toggle="tab" href="#menu4">Timetable</a></li>
    </ul>
    <div class="tab-content">
        <div id="menu1" class="tab-pane fade in active">
            <div class="panel-body">
                <dl>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Personal Details</h1>
                        </div>
                        <div class="panel-body">
                            <dl>
                                <dt>Student ID</dt>
                                <dd><?= $student->getStudentId() ?></dd>
                                <hr>
                                <dt>First Name</dt>
                                <dd><?= $student->getFirstName() ?></dd>
                                <?php if (!empty($student->getMiddleName())) { ?>
                                    <dt>Middle Name</dt>
                                    <dd><?= $student->getMiddleName() ?></dd>
                                <?php } ?>
                                <dt>Surname</dt>
                                <dd><?= $student->getSurname() ?></dd>
                            </dl>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Contact Details</h1>
                        </div>
                        <div class="panel-body">
                            <dl>
                                <dt>Phone number</dt>
                                <dd><?= $student->getPhone() ?></dd>
                                <dt>Email</dt>
                                <dd><?= $student->getEmail() ?></dd>
                            </dl>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Record Status</h1>
                        </div>
                        <div class="panel-body">
                            <dl>
                                <dt>Student status</dt>
                                <dd><?= $student->getStatus() ?></dd>

                                <?php if (!empty($student->getDormancyReason())) { ?>
                                    <dt>Dormancy Reason</dt>
                                    <dd><?= $student->getDormancyReason() ?></dd>
                                <?php } ?>
                            </dl>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Personal Tutor</h1>
                        </div>
                        <div class="panel-body">
                            <?php
                            try {
                                $staff = \app\models\PersonalTutor::get($student->getPersonalTutorId());
                                ?>
                                <a href="<?= route('staff.showRecord', ['id' => $staff->getStaffId()]) ?>"><?= $staff->getFullName() ?></a>
                                <?php
                            } catch (\Exception $e) {
                                echo 'No personal tutor assigned';
                            }
                            ?>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Term-time address</h1>
                        </div>
                        <div class="panel-body">
                            <dl>
                                <dt>Address Line 1</dt>
                                <dd><?= $student->getTermTimeAddressLine1() ?></dd>
                                <?php if (!empty($student->getTermTimeAddressLine2())) { ?>
                                    <dt>Address Line 2</dt>
                                    <dd><?= $student->getTermTimeAddressLine2() ?></dd>
                                <?php } ?>
                                <dt>City</dt>
                                <dd><?= $student->getTermTimeCity() ?></dd>
                                <dt>Country</dt>
                                <dd><?= $student->getTermTimeCountry() ?></dd>
                                <dt>Postcode</dt>
                                <dd><?= $student->getTermTimePostcode() ?></dd>
                            </dl>
                        </div>
                    </div>

                    <?php if (!empty($student->getAddressLine1())) {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Term-time address</h1>
                            </div>
                            <div class="panel-body">
                                <dl>
                                    <h4>Home address</h4>
                                    <dt>Address Line 1</dt>
                                    <dd><?= $student->getAddressLine1() ?></dd>
                                    <?php if (!empty($student->getAddressLine2())) { ?>
                                        <dt>Address Line 2</dt>
                                        <dd><?= $student->getAddressLine2() ?></dd>
                                    <?php } ?>
                                    <dt>City</dt>
                                    <dd><?= $student->getCity() ?></dd>
                                    <dt>Country</dt>
                                    <dd><?= $student->getCountry() ?></dd>
                                    <dt>Postcode</dt>
                                    <dd><?= $student->getPostcode() ?></dd>
                                </dl>
                            </div>
                        </div>
                    <?php } ?>


            </div>
        </div>
        <div id="menu2" class="tab-pane fade in">
            <div class="panel-body">

                <h4>Course enrolled on:</h4>
                <?php if (!empty($student->getCourse())) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title"><a
                                        href="<?php echo route('courses.showRecord', ['id' => $student->getCourse()->getCourseId()]) ?>"> <?php echo $student->getCourse()->getTitle() ?></a>
                            </h1>
                        </div>
                        <div class="panel-body">
                            <?php
                            $courseModules = $student->getCourse()->getCourseModules();
                            foreach ($courseModules as $courseModule) {
                                echo '<dt><a href="' . route('modules.showRecord', ['id' => $courseModule->getModuleId()]) . '">' . $courseModule->getModule()->getTitle() . '</a></dt>';
                            }
                            ?>
                        </div>
                    </div>
                <?php } else echo 'Not enrolled on a course yet. Edit this student to add a course.'; ?>

            </div>
        </div>

        <div id="menu3" class="tab-pane fade in">
            <div class="panel-body">
                <?php
                $qualifications = $student->getQualifications();
                $counter = 1;

                if (!empty($qualifications)) {
                    foreach ($qualifications as $qualification) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Qualification <?php echo $counter ?></h1>
                            </div>
                            <div class="panel-body">
                                <dt>Qualification name</dt>
                                <dd><?php echo $qualification->getQualificationName() ?></dd>

                                <dt>Qualification Type</dt>
                                <dd><?php echo $qualification->getQualificationTypeName() ?></dd>

                                <dt>Grade Achieved</dt>
                                <dd><?php echo $qualification->getGradeAchieved() ?></dd>

                                <dt>Date Achieved</dt>
                                <dd><?php echo $qualification->getDateAchieved() ?></dd>
                                <?php $counter++ ?>
                            </div>
                        </div>
                    <?php }
                } else echo 'Student has no qualifications added. Click Edit to add qualifications.'; ?>
            </div>
        </div>

        <div id="menu4" class="tab-pane fade in">
            <div class="pull-right btn btn-default print-element" data-target=".timetable-container">
                <i class="fa fa-print"></i>
                Print Timetable
            </div>
            <span class="clearfix"></span>

            <div class="timetable-container">
                <style>
                    @media print {
                        .timetable-container h4 {
                            display: none;
                        }

                        .timetable-container h4 {
                            display: block;
                        }
                    }
                </style>
                <h4>Timetable for <?=$student->getFullName()?></h4>
                <div class="timetable"
                     data-json-url="<?= route('timetables.getStudentEvents', ['student_id' => $id]) ?>"
                     data-edit-form-url=""></div>

            </div>
        </div>

    </div>
</div>

