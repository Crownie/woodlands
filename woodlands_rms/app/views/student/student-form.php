<?php
use app\utilities\Form;

?>
<?= csrf_input() ?>
<?php
?>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#menu1">Personal Details</a></li>
        <li><a data-toggle="tab" href="#menu2">Contact Details</a></li>
        <li><a data-toggle="tab" href="#menu3">Qualifications</a></li>
        <li><a data-toggle="tab" href="#menu4">Course</a></li>
    </ul>
    <div class="tab-content">
    <div id="menu1" class="tab-pane fade in active"><!— for tab 1-->
<?php
        Form::input('first_name', '*First Name', $student->getFirstName());
        Form::input('middle_name', 'Middle Name', $student->getMiddleName());
        Form::input('surname', '*Surname', $student->getSurname());
?>
        <div data-target="#menu2" class="next-tab-btn col-sm-3 btn btn-info pull-right">Next</div>
    </div>
    <div id="menu2" class="tab-pane fade"> <!— for tab 2-->
    <h4>Contact Information</h4>
<?php
//////////////////////////////////////////////////////////////////////////////////////////////
        Form::input('phone', '*Contact Number', $student->getPhone());
        Form::input('email', '*Email Address', $student->getEmail());
?>
    <h4>Term-time Address</h4>
<?php
        Form::input('term_time_address_line1', '*Address Line 1', $student->getTermTimeAddressLine1());
        Form::input('term_time_address_line2', 'Address Line 2', $student->getTermTimeAddressLine2());
        Form::input('term_time_city', '*City', $student->getTermTimeCity());
        Form::input('term_time_country', '*Country', $student->getTermTimeCountry());
        Form::input('term_time_postcode', '*Postcode', $student->getTermTimePostcode());
        Form::row();
?>
    <hr>
    <h4>Non Term-time Address</h4>
<?php



        Form::checkbox('both_addresses_same','',$student->isAddressTheSame(),[
            'yes' => 'Same as term-time address'
        ],'col-sm-6 both_addresses_same');
        Form::row();

        Form::input('address_line1', '*Address Line 1', $student->getAddressLine1());
        Form::input('address_line2', 'Address Line 2', $student->getAddressLine2());
        Form::input('city', '*City', $student->getCity());
        Form::input('country', '*Country', $student->getCountry());
        Form::input('postcode', '*Postcode', $student->getPostcode());
?>
        <div data-target="#menu3" class="next-tab-btn col-sm-3 btn btn-info pull-right">Next</div>

    </div>
    <div id="menu3" class="tab-pane fade"> <!— for tab 2-->

        <?php
        ?>
        <div class="panel panel-default">
            <div class="panel-body">
        <?php

        Form::checkbox('qualifications_verified','',$student->getQualificationsVerified(),[
            'yes' => 'Qualifications verified'
        ],'col-sm-6');
        //Form::row();
        ?></div>
        </div>
                <?php
        $counter = 1;
        $qualifications = isset($qualifications)?$qualifications:[];
        foreach ($qualifications as $qualification){
            ?>
            <div class="qualification panel panel-default">
                <div class="panel-heading">Qualification <?=$counter ?><div class="remove-qualification btn btn-xs btn-danger pull-right">x</div></div>
                <div class="panel-body">
                    <?php
                    Form::select('qualification_types[]','*Type of qualification',$qualification->getQualificationType(),[
                        'fs' => 'Functional Skill',
                        'elq' => 'Entry Level Qualfication',
                        'gcse' => 'GCSE Qualification',
                        'alvl' => 'A/AS Level Qualification',
                        'ib' => 'International Baccalaureate Diploma',
                        'vq' => 'Vocational Qualification',
                        'heq' => 'Higher Education Qualification',
                        'oth' => 'Other'
                    ],'col-sm-6');
                    Form::input('qualifications[]', '*Qualification name',$qualification->getQualificationName(),[],'col-sm-6');
                    Form::input('grades_achieved[]', '*Grade/Mark Achieved',$qualification->getGradeAchieved(),[],'col-sm-6');
                    Form::date('dates_achieved[]', '*Date Achieved',date('d/m/Y',strtotime($qualification->getDateAchieved())) , 'col-sm-6');
                    ?>          </div>
            </div>
            <?php
            $counter++;
        }
        ?>

<?php
    if($counter<2) {
        Form::button('Add a qualification', 'btn-primary btn-block add-qualification-btn', 'null');
    }
    else{
        Form::button('Add another qualification', 'btn-primary btn-block add-qualification-btn', 'null');
    }

?>
        <div data-target="#menu4" class="next-tab-btn col-sm-3 btn btn-info pull-right">Next</div>

    </div>
    <div id="menu4" class="tab-pane fade in">

<?php
$courseOptions = \app\Controllers\CourseController::getCourseOptions();
        Form::searchableSelect('course_id','Course to enrol on',[],$courseOptions,'col-sm-4');
        Form::select('level', '*Level', $student->getLevel(), [
            '1'=>'Level 1',
            '2'=>'Level 2',
            '3'=>'Level 3'
        ]);
        Form::button('Submit', 'btn-success pull-right');
?>
    </div>
</div>
