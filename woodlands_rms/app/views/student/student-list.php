<?php use app\utilities\Form;


$this->layout('layouts/master', ['title' => 'Students']) ?>

<ul class="breadcrumb">
    <li class="active">Students</li>
</ul>

<h2 class="page-header">List Students</h2>

<form action="" method="get">

    <div class="panel panel-default">
        <br>
        <?php
        Form::input('search','Search',$search,['placeholder'=>'Enter ID or Name'],'col-sm-12');
        $courseOptions = \app\Controllers\CourseController::getCourseOptions();
        array_unshift($courseOptions,'Any Course');
        Form::searchableSelect('course_id', 'Filter by course', $course_id, $courseOptions);
        Form::select('status', 'Filter by status', $status, ['LIVE'=>'LIVE', 'PROVISIONAL'=>'PROVISIONAL', 'DORMANT'=>'DORMANT']);
        ?>

        <div class="col-sm-12"><button class="btn btn-default pull-right" type="submit">
                <i class="fa fa-search"></i>
                Search
            </button></div>
        <span class="clearfix"></span>
        <br>
    </div>
    <span class="clearfix"></span>
</form>

<p>
    <?php
    if(authorized('students.showRecordForm')) {
        ?>
        <a href="<?= route('students.showRecordForm') ?>" class="btn btn-success pull-right">Create student</a>
        <?php
    }
    ?>
<div class="clearfix"></div>
</p>
<h5>Number of students found: <span class="label label-default"><?=count($students)?></span> </h5>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Student ID</th>
        <th>First Name</th>
        <th>Surname</th>
        <th>Course</th>
        <th>Contact Phone</th>
        <th>Status</th>
        <th>Actions</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($students as $student) {
        $this->insert('student/partials/student-item', ['student' => $student]);
    }
    ?>


    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="student-archive-modal" tabindex="-1" role="dialog"
     aria-labelledby="student-archive-modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Archive Student</h4>
            </div>
            <div class="modal-body">
                <form action="<?= route('students.archive') ?>" method="post">
                    <?php
                    echo csrf_input();
                    Form::hiddenInput('student_id', '');
                    Form::select('dormancy_reason', '*Dormancy Reason', '', [
                        'GRADUATED' => 'GRADUATED',
                        'WITHDRAWN' => 'WITHDRAWN',
                        'TERMINATED' => 'TERMINATED'
                    ]);
                    Form::row();
                    Form::button('Archive', 'btn btn-success');
                    Form::row();
                    ?>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>