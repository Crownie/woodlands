<div class="qualification panel panel-default">
            <div class="panel-heading">Qualification <?=$num?> <div class="remove-qualification btn btn-xs btn-danger pull-right">x</div></div>
            <div class="panel-body">
<?php

use app\utilities\Form;

        Form::select('qualification_types[]','*Type of qualification',[],[
            'fs' => 'Functional Skill',
            'elq' => 'Entry Level Qualfication',
            'gcse' => 'GCSE Qualification',
            'alvl' => 'A/AS Level Qualification',
            'ib' => 'International Baccalaureate Diploma',
            'vq' => 'Vocational Qualification',
            'heq' => 'Higher Education Qualification',
            'oth' => 'Other'
        ],'col-sm-6');
        Form::input('qualifications[]', '*Qualification name','',[],'col-sm-6');
        Form::input('grades_achieved[]', '*Grade/Mark Achieved','',[],'col-sm-6');
        Form::date('dates_achieved[]', '*Date Achieved', '', 'col-sm-6');

?>
</div>
</div>