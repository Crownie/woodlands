<tr data-id="<?= $student->getStudentId() ?>">
    <td><?= $student->getStudentId() ?></td>
    <td><?= $student->getFirstName() ?></td>
    <td><?= $student->getSurname() ?></td>
    <?php if (!empty($student->getCourse())) {
        echo '<td>' . $student->getCourse()->getTitle() . '</td>';
    } else {
        echo '<td>No course</td>';
    }
    ?>
    <td><?= $student->getPhone() ?></td>
    <td><?= $student->getStatus() ?></td>
    <th>
        <a href="<?= route('students.showRecord', ['id' => $student->getStudentId()]) ?>"
           class="btn btn-default">View</a>


        <?php
        if (authorized('students.showRecordForm')) {
            ?>
            <a href="<?= route('students.showRecordEditForm', ['id' => $student->getStudentId()]) ?>"
               class="btn btn-default">Edit</a>
            <?php
            //gen_delete_button(route('students.deleteRecord', ['id' => $student->getStudentId()]), 'this will delete the ' . $student->getFirstName() . ' student record');
            ?>
            <?php
            if ($student->getStatus() != 'LIVE') {
                ?>
                <form style="display: inline-block"
                      action="<?= route('students.makeLive', ['id' => $student->getStudentId()]) ?>" method="post">
                    <?= csrf_input() ?>
                    <button type="submit" class="btn btn-default">Make Live</button>
                </form>
                <?php
            } else {
                ?>
                <div data-student-id="<?= $student->getStudentId() ?>" class="btn btn-default student-archive-btn"
                     data-toggle="modal" data-target="#student-archive-modal">Archive
                </div>

                <?php
            }
        }
        ?>
    </th>
</tr>