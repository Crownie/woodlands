<?php $this->layout('layouts/master', ['title' => 'Create Student'])?>

<ul class="breadcrumb">
    <li><a href="<?=route('students.showRecordList')?>">Students</a></li>
    <li class="active">Create Student</li>
</ul>

<h2 class="page-header">Create Student</h2>
<p>
    <a href="<?=route('students.showRecordList')?>" class="pull-left btn btn-success">List</a>
<div class="clearfix"></div>
</p>
<form action="<?=route('students.storeRecord')?>" method="POST" id="student-form">
    <?php $this->insert('student/student-form',get_defined_vars()) ?>
</form>
