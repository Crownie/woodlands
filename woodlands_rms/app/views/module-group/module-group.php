<?php
/**
 * Created by Alex
 */

use app\Controllers\CourseController;
use app\controllers\ModuleController;
use app\controllers\PersonalTutorController;
use app\models\Student;
use app\utilities\Form;

$this->layout('layouts/master', ['title' => 'Show Module Groups'])
?>

<h2 class="page-header">Module Groups</h2>

<form action="" method="get">
    <?php
    Form::searchableSelect('module_id', '*Module', $module_id, ModuleController::getModuleOptionsAll());
    Form::button('Search','btn btn-success');

    ?>
</form>

<div class="clearfix"></div>
<hr/>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php
    for($group_number = 1; $group_number<=5;$group_number++){
        $students = \app\models\StudentModuleGroup::getStudentsByGroupAndLevel($module_id,$group_number);
        $this->insert('module-group/partials/module-group-panel',get_defined_vars());
    }
    ?>
</div>
