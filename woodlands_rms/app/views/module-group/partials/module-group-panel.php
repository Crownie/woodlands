<?php
use app\models\Student;

$uid = generate_random_string();

/* @var $student \app\models\Student*/
?>
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
            <div class="col-sm-4">
                <div>
                    <?php
                    echo 'Group '.$group_number
                    ?>
                </div>
            </div>
            <div class="col-sm-4">Number of tutees: <?= count($students) ?></div>
            <div class="btn btn-default btn-multiple-select-modal" data-title="Assign Students to Group <?=$group_number?>" data-options-url="<?=route('module-groups.getStudentOptions',['group_number'=>$group_number]).'?module_id='.$module_id?>" data-save-url="<?=route('module-groups.addStudentToGroup',['group_number'=>$group_number]).'?module_id='.$module_id?>">Assign Students</div>
            <a class="pull-right btn btn-default accordion-toggle-btn" role="button" data-toggle="collapse"
               data-parent="#accordion" href="#collapse<?= $uid ?>"><i class="fa fa-caret-up"></i></a>
        </h4>
    </div>
    <div id="collapse<?= $uid ?>" class="panel-collapse collapse" aria-expanded="false" role="tabpanel"
         aria-labelledby="headingOne">
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Student id</th>
                    <th>First name</th>
                    <th>Surname</th>
                    <th>Course</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach ($students as $student) {
                    ?>
                    <tr>
                        <th><?= $student->getStudentId() ?></th>
                        <td><?= $student->getFirstName() ?></td>
                        <td><?= $student->getSurname() ?></td>
                        <?php
                        $course = $student->getCourse();
                        if ($course) {
                            ?>
                            <td><?= $course->getTitle() ?></td>
                            <?php
                        }else{
                            echo '<td>No course</td>';
                        }
                        ?>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Move To
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <?php
                                    for($g=1;$g<=5;$g++){
                                        if($g==$group_number)
                                            continue;
                                        ?>
                                        <li><a  href="<?=route('module-groups.moveToGroup',['student_id'=>$student->getStudentId(),'group_number'=>$g,'module_id'=>$module_id])?>">Group <?=$g?></a></li>
                                        <?php
                                    }
                                    ?>
                                    <li><a  href="<?=route('module-groups.removeStudent',['student_id'=>$student->getStudentId(),'module_id'=>$module_id])?>">No Group</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php

                }

                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
