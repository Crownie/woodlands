delimiter //
DROP TRIGGER IF EXISTS trig_generate_student_id //

CREATE TRIGGER trig_generate_student_id BEFORE INSERT ON students
FOR EACH ROW BEGIN
    
    DECLARE previous_year VARCHAR(4);
    DECLARE this_year VARCHAR(4);
	DECLARE next_personal_tutor_id INT;
    DECLARE no_of_students_by_pt INT;

	

    
	SET @auto_id := ( SELECT AUTO_INCREMENT 
                    FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME='students'
                      AND TABLE_SCHEMA=DATABASE() );
                    
	SET previous_year = SUBSTRING(@auto_id,1,4);
	SET this_year = YEAR(CURDATE());

    IF previous_year != this_year THEN
		SET NEW.student_id = CAST(CONCAT(this_year,'', '1001') AS unsigned);
	/*ELSEIF NEW.student_id IS NULL THEN
		SET NEW.student_id = @auto_id;*/
	END IF;
    
    
    SELECT s.staff_id,count(st.student_id) as num INTO next_personal_tutor_id,no_of_students_by_pt FROM staff s
		LEFT JOIN students st
        ON s.staff_id = st.personal_tutor_id
        JOIN staff_roles sr
        ON s.staff_id = sr.staff_id
        WHERE sr.role='PT'
        GROUP BY s.staff_id
        ORDER BY num
        LIMIT 1;
        
   IF NEW.status = 'LIVE' THEN
		SET NEW.personal_tutor_id = next_personal_tutor_id;
   END IF;
END;
//

delimiter ;