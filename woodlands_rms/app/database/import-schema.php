<?php
/**
 * Created by Tobi Ayilara.
 * Date: 12/02/2017
 * Time: 23:29
 */

/**
 * open this link in the browser to import the schema (hold Ctrl and click the link)
 * @link http://rms.wuc.ac.uk/app/database/import-schema.php
 */

$host = "localhost";
$username = 'admin';
$password = 'password';
$db = '';
$file = __DIR__ . '/' . 'woodlands_schema.sql';


try
{
    $db = new PDO('mysql:dbname='.$db.';host='.$host,$username,$password);
    $sql = file_get_contents($file);;

    $qr = $db->exec($sql);
    echo "Import action - 100% successfull";
}
catch (PDOException $e)
{
    echo 'Connection failed: ' . $e->getMessage();
}