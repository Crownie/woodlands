-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dev_woodlands
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dev_woodlands
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dev_woodlands` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `dev_woodlands` ;

-- -----------------------------------------------------
-- Table `dev_woodlands`.`courses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`courses` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`courses` (
  `course_id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `cost` DECIMAL(9,2) NOT NULL,
  `start_year` INT(4) NOT NULL,
  `duration` INT(1) NOT NULL,
  `description` TEXT NULL,
  `ucas_points` INT(3) NULL,
  PRIMARY KEY (`course_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`students`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`students` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`students` (
  `student_id` INT NOT NULL AUTO_INCREMENT,
  `status` ENUM('Provisional','Live','Dormant') NULL,
  `dormancy_reason` ENUM('Graduated','Withdraw','Terminated') NULL,
  `first_name` VARCHAR(45) NULL,
  `middle_name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `term_time_address_line1` VARCHAR(45) NULL,
  `term_time_address_line2` VARCHAR(45) NULL,
  `term_time_city` VARCHAR(45) NULL,
  `term_time_country` VARCHAR(45) NULL,
  `term_time_postcode` VARCHAR(45) NULL,
  `address_line1` VARCHAR(45) NULL,
  `address_line2` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `postcode` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `qualifications_verified` TINYINT(1) NULL DEFAULT 0,
  `course_id` INT NULL,
  PRIMARY KEY (`student_id`),
  INDEX `fk_students_courses1_idx` (`course_id` ASC),
  CONSTRAINT `fk_students_courses1`
    FOREIGN KEY (`course_id`)
    REFERENCES `dev_woodlands`.`courses` (`course_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`modules`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`modules` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`modules` (
  `module_code` VARCHAR(6) NOT NULL,
  `level` INT NULL,
  `credits` INT NULL,
  `title` VARCHAR(45) NULL,
  `ass1` INT(3) NULL,
  `ass2` INT(3) NULL,
  `exam` INT(3) NULL,
  PRIMARY KEY (`module_code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`module_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`module_groups` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`module_groups` (
  `module_group_id` INT NOT NULL,
  `level` INT(4) NULL,
  `year` INT(4) NULL,
  `group_number` VARCHAR(45) NULL,
  `modules_module_code` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`module_group_id`),
  INDEX `fk_module_groups_modules1_idx` (`modules_module_code` ASC),
  UNIQUE INDEX `u_module_groups` (`level` ASC, `year` ASC, `group_number` ASC),
  CONSTRAINT `fk_module_groups_modules1`
    FOREIGN KEY (`modules_module_code`)
    REFERENCES `dev_woodlands`.`modules` (`module_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`rooms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`rooms` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`rooms` (
  `room_id` INT NOT NULL AUTO_INCREMENT,
  `room_number` VARCHAR(45) NULL,
  `room_type` VARCHAR(45) NULL,
  `capacity` INT NULL,
  PRIMARY KEY (`room_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`timetables`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`timetables` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`timetables` (
  `timetable_id` INT NOT NULL AUTO_INCREMENT,
  `course_id` INT NOT NULL,
  `year` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`timetable_id`),
  INDEX `fk_timetables_courses1_idx` (`course_id` ASC),
  CONSTRAINT `fk_timetables_courses1`
    FOREIGN KEY (`course_id`)
    REFERENCES `dev_woodlands`.`courses` (`course_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`timetable_items`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`timetable_items` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`timetable_items` (
  `timetable_item_id` INT NOT NULL,
  `timetable_item_type` ENUM('Lecture','Practical') NULL,
  `module_group_id` INT NOT NULL,
  `module_code` VARCHAR(6) NOT NULL,
  `class_date` DATETIME NULL,
  `room_id` INT NOT NULL,
  `timetable_id` INT NOT NULL,
  PRIMARY KEY (`timetable_item_id`),
  INDEX `fk_class_schedules_module_groups1_idx` (`module_group_id` ASC),
  INDEX `fk_class_schedules_modules1_idx` (`module_code` ASC),
  INDEX `fk_class_schedules_rooms1_idx` (`room_id` ASC),
  INDEX `fk_timetable_items_timetables1_idx` (`timetable_id` ASC),
  CONSTRAINT `fk_class_schedules_module_groups1`
    FOREIGN KEY (`module_group_id`)
    REFERENCES `dev_woodlands`.`module_groups` (`module_group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_class_schedules_modules1`
    FOREIGN KEY (`module_code`)
    REFERENCES `dev_woodlands`.`modules` (`module_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_class_schedules_rooms1`
    FOREIGN KEY (`room_id`)
    REFERENCES `dev_woodlands`.`rooms` (`room_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timetable_items_timetables1`
    FOREIGN KEY (`timetable_id`)
    REFERENCES `dev_woodlands`.`timetables` (`timetable_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`attendance`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`attendance` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`attendance` (
  `attendance_id` INT NOT NULL,
  `status` ENUM('Present','Absent') NULL DEFAULT 'Absent',
  `student_id` INT NOT NULL,
  `timetable_item_id` INT NOT NULL,
  PRIMARY KEY (`attendance_id`, `student_id`, `timetable_item_id`),
  INDEX `fk_attendance_students1_idx` (`student_id` ASC),
  INDEX `fk_attendance_timetable_items1_idx` (`timetable_item_id` ASC),
  CONSTRAINT `fk_attendance_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `dev_woodlands`.`students` (`student_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attendance_timetable_items1`
    FOREIGN KEY (`timetable_item_id`)
    REFERENCES `dev_woodlands`.`timetable_items` (`timetable_item_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`assignments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`assignments` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`assignments` (
  `assignment_id` INT NOT NULL AUTO_INCREMENT,
  `start_date` DATETIME NULL,
  `due_date` DATETIME NULL,
  `title` VARCHAR(45) NULL,
  `description` TEXT NULL,
  `module_code` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`assignment_id`),
  INDEX `fk_assignments_modules1_idx` (`module_code` ASC),
  CONSTRAINT `fk_assignments_modules1`
    FOREIGN KEY (`module_code`)
    REFERENCES `dev_woodlands`.`modules` (`module_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`staff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`staff` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`staff` (
  `staff_id` INT NOT NULL AUTO_INCREMENT,
  `status` ENUM('Live','Dormant') NULL,
  `dormancy_reason` ENUM('Retired','Resigned','Misconduct') NULL,
  `first_name` VARCHAR(45) NULL,
  `middle_name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `address_line1` VARCHAR(45) NULL,
  `address_line2` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `postcode` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`staff_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 99100001;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`submissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`submissions` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`submissions` (
  `submission_id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  `date_submitted` VARCHAR(45) NULL,
  `student_id` INT NOT NULL,
  `assignment_id` INT NOT NULL,
  `file_url` VARCHAR(255) NULL,
  PRIMARY KEY (`submission_id`),
  INDEX `fk_submissions_students1_idx` (`student_id` ASC),
  INDEX `fk_submissions_assignments1_idx` (`assignment_id` ASC),
  CONSTRAINT `fk_submissions_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `dev_woodlands`.`students` (`student_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_submissions_assignments1`
    FOREIGN KEY (`assignment_id`)
    REFERENCES `dev_woodlands`.`assignments` (`assignment_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`entry_qualifications`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`entry_qualifications` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`entry_qualifications` (
  `qualification_id` INT NOT NULL,
  PRIMARY KEY (`qualification_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`module_leaders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`module_leaders` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`module_leaders` (
  `staff_id` INT NOT NULL,
  `module_code` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`staff_id`, `module_code`),
  INDEX `fk_staff_has_modules_modules1_idx` (`module_code` ASC),
  INDEX `fk_staff_has_modules_staff_idx` (`staff_id` ASC),
  CONSTRAINT `fk_staff_has_modules_staff`
    FOREIGN KEY (`staff_id`)
    REFERENCES `dev_woodlands`.`staff` (`staff_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_staff_has_modules_modules1`
    FOREIGN KEY (`module_code`)
    REFERENCES `dev_woodlands`.`modules` (`module_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`staff_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`staff_roles` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`staff_roles` (
  `role_id` INT NOT NULL AUTO_INCREMENT,
  `role` ENUM('CL','ML','PT') NOT NULL,
  `staff_id` INT NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE INDEX `u_staff_roles` (`role` ASC, `staff_id` ASC),
  INDEX `fk_staff_roles_staff1_idx` (`staff_id` ASC),
  CONSTRAINT `fk_staff_roles_staff1`
    FOREIGN KEY (`staff_id`)
    REFERENCES `dev_woodlands`.`staff` (`staff_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`students_module_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`students_module_groups` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`students_module_groups` (
  `student_id` INT NOT NULL,
  `module_group_id` INT NOT NULL,
  PRIMARY KEY (`student_id`, `module_group_id`),
  INDEX `fk_students_has_module_groups_students1_idx` (`student_id` ASC),
  INDEX `fk_students_module_groups_module_groups1_idx` (`module_group_id` ASC),
  CONSTRAINT `fk_students_has_module_groups_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `dev_woodlands`.`students` (`student_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_module_groups_module_groups1`
    FOREIGN KEY (`module_group_id`)
    REFERENCES `dev_woodlands`.`module_groups` (`module_group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`exams`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`exams` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`exams` (
  `exam_id` INT NOT NULL AUTO_INCREMENT,
  `start_date` VARCHAR(45) NULL,
  `hours` VARCHAR(45) NULL,
  `title` VARCHAR(45) NULL,
  `module_code` VARCHAR(6) NOT NULL,
  `location` VARCHAR(100) NULL,
  PRIMARY KEY (`exam_id`),
  INDEX `fk_exams_modules1_idx` (`module_code` ASC),
  CONSTRAINT `fk_exams_modules1`
    FOREIGN KEY (`module_code`)
    REFERENCES `dev_woodlands`.`modules` (`module_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`assignment_files`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`assignment_files` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`assignment_files` (
  `assignment_id` INT NOT NULL,
  `file_url` VARCHAR(255) NULL,
  PRIMARY KEY (`assignment_id`),
  CONSTRAINT `fk_assignments_has_files_assignments1`
    FOREIGN KEY (`assignment_id`)
    REFERENCES `dev_woodlands`.`assignments` (`assignment_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`exam_files`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`exam_files` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`exam_files` (
  `exam_id` INT NOT NULL,
  `file_url` VARCHAR(255) NULL,
  PRIMARY KEY (`exam_id`),
  CONSTRAINT `fk_exams_has_files_exams1`
    FOREIGN KEY (`exam_id`)
    REFERENCES `dev_woodlands`.`exams` (`exam_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`personal_tutorials`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`personal_tutorials` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`personal_tutorials` (
  `personal_tutorial_id` INT NOT NULL AUTO_INCREMENT,
  `personal_tutorial_meeting` VARCHAR(255) NOT NULL,
  `date_of_meeting` DATE NULL,
  `tutorial_summary` TEXT NULL,
  `action_points_summary` TEXT NULL,
  `next_meeting_date` TEXT NULL,
  `student_attended` TINYINT(1) NULL DEFAULT 0,
  `staff_id` INT NOT NULL,
  `student_id` INT NOT NULL,
  PRIMARY KEY (`personal_tutorial_id`),
  INDEX `fk_personal_tutorials_staff1_idx` (`staff_id` ASC),
  INDEX `fk_personal_tutorials_students1_idx` (`student_id` ASC),
  CONSTRAINT `fk_personal_tutorials_staff1`
    FOREIGN KEY (`staff_id`)
    REFERENCES `dev_woodlands`.`staff` (`staff_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_personal_tutorials_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `dev_woodlands`.`students` (`student_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`events` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`events` (
  `event_id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  `location` VARCHAR(200) NULL,
  `description` TEXT NULL,
  `student_id` INT NULL,
  `staff_id` INT NULL,
  PRIMARY KEY (`event_id`),
  INDEX `fk_events_students1_idx` (`student_id` ASC),
  INDEX `fk_events_staff1_idx` (`staff_id` ASC),
  CONSTRAINT `fk_events_students1`
    FOREIGN KEY (`student_id`)
    REFERENCES `dev_woodlands`.`students` (`student_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_events_staff1`
    FOREIGN KEY (`staff_id`)
    REFERENCES `dev_woodlands`.`staff` (`staff_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dev_woodlands`.`course_modules`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dev_woodlands`.`course_modules` ;

CREATE TABLE IF NOT EXISTS `dev_woodlands`.`course_modules` (
  `course_id` INT NOT NULL,
  `module_code` VARCHAR(6) NOT NULL,
  INDEX `fk_course_module_courses1_idx` (`course_id` ASC),
  INDEX `fk_course_module_modules1_idx` (`module_code` ASC),
  PRIMARY KEY (`course_id`, `module_code`),
  CONSTRAINT `fk_course_module_courses1`
    FOREIGN KEY (`course_id`)
    REFERENCES `dev_woodlands`.`courses` (`course_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_module_modules1`
    FOREIGN KEY (`module_code`)
    REFERENCES `dev_woodlands`.`modules` (`module_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
