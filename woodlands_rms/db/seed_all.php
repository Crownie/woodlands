<?php

$seeders = [
    'Department',
    'Course',
    'Staff',
    'StaffRole',
    'Student',
    'Module',
    'ModuleLeader',
    'CourseModule',
    'StudentModuleGroup',
    'Room',
    'Timetable',
    'TimetableItem',
    'Announcement'
];

foreach ($seeders as $seeder){
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        //echo 'This is a server using Windows!';
        exec('composer seed '.$seeder.'Seeder');
    } else {
        //echo 'This is a server not using Windows!';
        exec('composer lseed '.$seeder.'Seeder');
    }

}