<?php

use Phinx\Seed\AbstractSeed;

class ModuleLeaderSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $file = file(__DIR__.'/../../app/database/csv/staff.csv');

        $csv = array_map('str_getcsv', $file);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        $rows = [];

        foreach($csv as $item){
            $module_codes = explode("/", $item['Module Leader']);

            foreach ($module_codes as $module_code){
                $result = $this->fetchRow('SELECT module_id FROM modules WHERE module_code = \''.$module_code.'\'');
                if(!empty($result)){
                    $module_id = $result['module_id'];
                    $rows [] = [
                        'module_id' => $module_id,
                        'staff_id' => $item['ID'],
                    ];
                }
            }

        }

        $table = $this->table('module_leaders');
        $table->insert($rows)
            ->save();
    }
}
