<?php

use Phinx\Seed\AbstractSeed;

class StudentSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_GB');


        for($i =1;$i<=3;$i++){
            $file = file(__DIR__.'/../../app/database/csv/students-level-'.$i.'.csv');

            $csv = array_map('str_getcsv', $file);

            array_walk($csv, function(&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            });

            array_shift($csv); # remove column header

            $rows=[];

            foreach($csv as $item){



                 $rows [] = [
                    'student_id'=>$item['Student ID'],
                    'first_name'    => $item['First Name'],
                    'middle_name' => '',
                    'surname'=>$item['Surname'],
                    'term_time_address_line1'=>$faker->streetAddress,
                    'term_time_address_line2'=>'',
                    'term_time_city'=>$faker->city,
                    'term_time_country'=>$faker->country,
                    'term_time_postcode'=>$faker->postcode,
                    'address_line1'=>'',
                    'address_line2'=>'',
                    'city'=>'',
                    'country'=>'',
                    'postcode'=>'',
                    'phone'=>$faker->phoneNumber,
                    'email'=>!empty($item['Email'])?$item['Email']:$faker->email,
                    'qualifications_verified'=>1,
                    'course_id'=>1,
                    'current_year'=>date('Y'),
                    'level'=>$item['Level'],
                    'status'=>'LIVE',
                    'personal_tutor_id'=>null,
                    'password'=>password_hash('student',PASSWORD_DEFAULT)
                ];


            }
            $table = $this->table('students');
            $table->insert($rows)
                ->save();
        }


    }
}
