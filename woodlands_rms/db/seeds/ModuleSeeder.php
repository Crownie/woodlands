<?php

use Phinx\Seed\AbstractSeed;

class ModuleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $file = file(__DIR__.'/../../app/database/csv/modules.csv');

        $csv = array_map('str_getcsv', $file);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        $rows = [];

        foreach ($csv as $item){
            $rows[] = [
                'module_code' => $item['Module Code'],
                'title' => $item['Title'],
                'level' => $item['Level'],
                'credits' => $item['Points'],
                'ass1' => intval($item['Ass 1']),
                'ass2' => intval($item['Ass 2']),
                'exam' => intval($item['Exam']),
            ];
        }


        $table = $this->table('modules');
        $table->insert($rows)
            ->save();
    }
}
