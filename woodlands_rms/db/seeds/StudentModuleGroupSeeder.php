<?php

use Phinx\Seed\AbstractSeed;

class StudentModuleGroupSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [];

        for ($i = 1; $i <= 3; $i++) {
            $file = file(__DIR__ . '/../../app/database/csv/students-level-' . $i . '.csv');

            $csv = array_map('str_getcsv', $file);

            array_walk($csv, function (&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            });

            array_shift($csv); # remove column header


            foreach ($csv as $item) {
                $module_codes = array_keys($item);
                array_splice($module_codes, 0, 4);

                foreach ($module_codes as $module_code) {
                    $result = $this->fetchRow('SELECT module_id FROM modules WHERE module_code = \'' . $module_code . '\'');
                    if (!empty($result)) {
                        $module_id = $result['module_id'];
                        if(intval($item[$module_code])>0){
                            $rows [] = [
                                'module_id' => $module_id,
                                'student_id' => $item['Student ID'],
                                'group_number' => intval($item[$module_code]),
                            ];
                        }
                    }
                }
            }
        }

        $table = $this->table('student_module_groups');
        $table->insert($rows)
            ->save();
    }
}
