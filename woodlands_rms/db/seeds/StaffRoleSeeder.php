<?php

use Phinx\Seed\AbstractSeed;

class StaffRoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {

        $file = file(__DIR__.'/../../app/database/csv/staff.csv');

        $csv = array_map('str_getcsv', $file);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        $rows = [];

        foreach($csv as $item){
            $roles = explode("/", $item['Role(s)']);

            foreach ($roles as $role){
                $rows [] = [
                    'role' => $role,
                    'staff_id' => $item['ID'],
                ];
            }

        }

        $table = $this->table('staff_roles');
        $table->insert($rows)
            ->save();
    }
}
