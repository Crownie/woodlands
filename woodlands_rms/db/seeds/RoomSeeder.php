<?php

use Phinx\Seed\AbstractSeed;

class RoomSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $file = file(__DIR__.'/../../app/database/csv/rooms.csv');

        $csv = array_map('str_getcsv', $file);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        $rows = [];

        foreach($csv as $item){
            $rows[] = [
                'room_number'=>$item['Room'],
                'room_type'=>$item['Type'],
                'capacity'=>$item['Cap']
            ];
        }

        $table = $this->table('rooms');
        $table->insert($rows)
            ->save();
    }
}
