<?php

use Phinx\Seed\AbstractSeed;

class StaffSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_GB');
        $rows = [];

        $rows [] = [
            'first_name'    => 'Adam',
            'middle_name' => '',
            'surname'=>'Blake',
            'address_line1'=>$faker->streetAddress,
            'address_line2'=>'',
            'city'=>$faker->city,
            'country'=>$faker->country,
            'postcode'=>$faker->postcode,
            'phone'=>$faker->phoneNumber,
            'email'=>$faker->email,
            'status'=>'LIVE',
            'department_id'=>1,
            'password'=>password_hash('staff',PASSWORD_DEFAULT)

        ];

        $file = file(__DIR__.'/../../app/database/csv/staff.csv');

        $csv = array_map('str_getcsv', $file);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        foreach($csv as $item){
            $rows [] = [
                'staff_id'=>$item['ID'],
                'first_name'    => $item['First Name'],
                'middle_name' => '',
                'surname'=>$item['Surname'],
                'address_line1'=>$faker->streetAddress,
                'address_line2'=>'',
                'city'=>$faker->city,
                'country'=>$faker->country,
                'postcode'=>$faker->postcode,
                'phone'=>$faker->phoneNumber,
                'email'=>$faker->email,
                'status'=>'LIVE',
                'department_id'=>1,
                'password'=>password_hash('staff',PASSWORD_DEFAULT)

            ];
        }

        $table = $this->table('staff');
        $table->insert($rows)
            ->save();
    }
}
