<?php

use Phinx\Seed\AbstractSeed;

class CourseModuleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'course_id' => 1,
                'module_id' => 1,
            ],
            [
                'course_id' => 1,
                'module_id' => 2,
            ],
            [
                'course_id' => 1,
                'module_id' => 3,
            ]
        ];

        $table = $this->table('course_modules');
        $table->insert($rows)
            ->save();
    }
}
