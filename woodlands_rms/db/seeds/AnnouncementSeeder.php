<?php

use Phinx\Seed\AbstractSeed;

class AnnouncementSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'title'=>'Bank Holiday',
                'announcement_type'=>'GENERAL',
                'message'=>'Hello World bnk hldy',
                'module_id'=>null,
                'course_id'=>null,
                'target'=>'BOTH',
                'date_posted'=>date ("Y-m-d H:i:s")
            ],
            [
                'title'=>'Grades Out!',
                'announcement_type'=>'MODULE',
                'message'=>'Proviosion grades has been released',
                'module_id'=>1,
                'course_id'=>null,
                'target'=>'STUDENTS',
                'date_posted'=>date ("Y-m-d H:i:s")
            ]
        ];

        $table = $this->table('announcements');
        $table->insert($rows)
            ->save();
    }
}
