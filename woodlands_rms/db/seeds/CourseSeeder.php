<?php

use Phinx\Seed\AbstractSeed;

class CourseSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'title' => 'Computing',
                'cost' => 9000,
                'start_year' => 2017,
                'duration' => 3,
                'description' => '',
                'ucas_points' => 300,
                'department_id'=>1
            ],
            [
                'title' => 'Business',
                'cost' => 7000,
                'start_year' => 2017,
                'duration' => 3,
                'description' => '',
                'ucas_points' => 300,
                'department_id'=>1
            ],
            [
                'title' => 'Engineering',
                'cost' => 9000,
                'start_year' => 2017,
                'duration' => 3,
                'description' => 'demo random',
                'ucas_points' => 300,
                'department_id'=>1
            ]
        ];

        $table = $this->table('courses');
        $table->insert($rows)
            ->save();
    }
}
