<?php

use Phinx\Seed\AbstractSeed;

class TimetableItemSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [];

        $file = file(__DIR__.'/../../app/database/csv/timetable.csv');

        $csv = array_map('str_getcsv', $file);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        $current_day = '';

        foreach($csv as $item){
            if(!empty($item['Day'])){
                $current_day = $item['Day'];
            }
            $module_code = $item['Module'];
            $room_number = $item['Room'];
            $resultModule = $this->fetchRow('SELECT module_id FROM modules WHERE module_code = \''.$module_code.'\'');

            $resultRoom = $this->fetchRow('SELECT room_id FROM rooms WHERE room_number = \''.$room_number.'\'');

            if(!empty($resultModule)&&!empty($resultRoom)){
                $rows[] = [
                    'timetable_item_type' => $item['Type'],
                    'module_group_number' => $item['Group Number'],
                    'module_id'=>$resultModule['module_id'],
                    'day'=>$current_day,
                    'start_time'=>$item['Time'],
                    'end_time'=>(intval($item['Time'])+1).':00',
                    'room_id'=>$resultRoom['room_id'],
                    'timetable_id'=>1
                ];
            }
        }

        $table = $this->table('timetable_items');
        $table->insert($rows)
            ->save();
    }
}
