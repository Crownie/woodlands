<?php

use Phinx\Seed\AbstractSeed;

class TimetableSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'course_id' => 1,
                'year' => 2017,
                'level'=>1
            ],
            [
                'course_id' => 2,
                'year' => 2017,
                'level'=>2
            ]
        ];

        $table = $this->table('timetables');
        $table->insert($rows)
            ->save();
    }
}
