<?php

use Phinx\Migration\AbstractMigration;

class CreatSubmissionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('submissions',array('id' => false, 'primary_key' => 'submission_id'));
        $table
            ->addColumn('submission_id', 'integer', [
                'identity' => true
            ])

            ->addColumn('title', 'string',[
                'length'=>45
            ])

            ->addColumn('student_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('student_id', 'students', 'student_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('assignment_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('assignment_id', 'assignments', 'assignment_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))

            ->addColumn('file_url', 'string',[
                'length'=>255
            ])
            ->addColumn('grade', 'string',[
                'length'=>2,
                'null'=>true
            ])
            ->addColumn('feedback', 'string',[
                'length'=>500,
                'null'=>true
            ])
            ->addColumn('date_submitted', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP'
            ])
            ->create();
    }
}
