<?php

use Phinx\Migration\AbstractMigration;

class CreateAnnouncementsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('announcements',array('id' => false, 'primary_key' => 'announcement_id'));
        $table
            ->addColumn('announcement_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('title', 'string',[
                'length'=>250,
                'null'=>true
            ])
            ->addColumn('announcement_type', 'enum',[
                'values'=>[
                    'MODULE',
                    'COURSE',
                    'GENERAL'
                ],
                'default'=>'GENERAL'
            ])
            ->addColumn('message', 'string',[
                'length'=>500,
                'null'=>true
            ])
            ->addColumn('module_id', 'integer',[
                'null'=>true
            ])
            ->addColumn('course_id', 'integer',[
                'null'=>true
            ])
            ->addColumn('target', 'enum',[
                'values'=>[
                    'STAFF',
                    'STUDENTS',
                    'BOTH'
                ],
                'default'=>'BOTH'
            ])
            ->addColumn('date_posted', 'datetime',[
                'null'=>true
            ])
            ->create();
    }
}
