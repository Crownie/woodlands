<?php

use Phinx\Migration\AbstractMigration;

class CreateEntryQualificationsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('entry_qualifications',array('id' => false, 'primary_key' => 'qualification_id'));
        $table
            ->addColumn('qualification_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('student_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('student_id', 'students', 'student_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('qualification_name', 'string',[
                'length'=>100,
                'null'=>true
            ])
            ->addColumn('qualification_type', 'string',[
                'length'=>100,
                'null'=>true
            ])
            ->addColumn('date_achieved', 'date',[
                'null'=>true
            ])
            ->addColumn('grade_achieved', 'string',[
                'length'=>45,
                'null'=>true
            ])
            ->create();
    }
}
