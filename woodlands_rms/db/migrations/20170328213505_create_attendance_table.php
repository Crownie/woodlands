<?php

use Phinx\Migration\AbstractMigration;

class CreateAttendanceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('attendance',array('id' => false, 'primary_key' => 'attendance_id'));
        $table
            ->addColumn('attendance_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('status', 'enum',[
                'values'=>['O','X','A'],
                'null'=>true
            ])
            ->addColumn('attendance_type', 'enum',[
                'values'=>['LECTURE','PRACTICAL'],
                'default'=>'LECTURE'
            ])
            ->addColumn('student_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('student_id', 'students', 'student_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('module_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('module_id', 'modules', 'module_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('week', 'integer', [
                'length' => 2,
            ])
            ->addIndex(array('attendance_type','student_id','module_id','week'), array('unique' => true, 'name' => 'u_attendance'))
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
            ])
            ->create();
    }
}
