<?php

use Phinx\Migration\AbstractMigration;

class CreateTimetableItemsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('timetable_items',array('id' => false, 'primary_key' => 'timetable_item_id'));
        $table
            ->addColumn('timetable_item_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('timetable_item_type', 'enum',[
                'values'=>['LECTURE','PRACTICAL'],
                'default'=>'LECTURE'
            ])
            ->addColumn('module_group_number', 'integer',[
                'length'=>1
            ])
            ->addColumn('module_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('module_id', 'modules', 'module_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))

            ->addColumn('day', 'enum',[
                'values'=>['MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY'],
                'default'=>'MONDAY'
            ])
            ->addColumn('start_time', 'time')
            ->addColumn('end_time', 'time')
            ->addColumn('room_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('room_id', 'rooms', 'room_id', array('delete'=> 'RESTRICT', 'update'=> 'NO_ACTION'))
            ->addColumn('timetable_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('timetable_id', 'timetables', 'timetable_id', array('delete'=> 'CASCADE', 'update'=> 'NO_ACTION'))
            ->create();

    }
}
