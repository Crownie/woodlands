<?php

use Phinx\Migration\AbstractMigration;

class CreateStudentsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $studentsTable = $this->table('students',array('id' => false, 'primary_key' => 'student_id'));
        $studentsTable
            ->addColumn('student_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('first_name', 'string',[
                'length'=>45
            ])
            ->addColumn('middle_name', 'string',[
                'length'=>45
            ])
            ->addColumn('surname', 'string',[
                'length'=>45
            ])
            ->addColumn('term_time_address_line1', 'string',[
                'length'=>100
            ])
            ->addColumn('term_time_address_line2', 'string',[
                'length'=>100
            ])
            ->addColumn('term_time_city', 'string',[
                'length'=>45
            ])
            ->addColumn('term_time_country', 'string',[
                'length'=>60
            ])
            ->addColumn('term_time_postcode', 'string',[
                'length'=>45
            ])
            ->addColumn('address_line1', 'string',[
                'length'=>100,
                'null'=>true
            ])
            ->addColumn('address_line2', 'string',[
                'length'=>100,
                'null'=>true
            ])
            ->addColumn('city', 'string',[
                'length'=>45,
                'null'=>true
            ])
            ->addColumn('country', 'string',[
                'length'=>60,
                'null'=>true
            ])
            ->addColumn('postcode', 'string',[
                'length'=>45,
                'null'=>true
            ])
            ->addColumn('phone', 'string',[
                'length'=>45
            ])
            ->addColumn('email', 'string',[
                'length'=>45
            ])
            ->addColumn('qualifications_verified', 'boolean',[
                'default'=>0
            ])
            ->addColumn('course_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('course_id', 'courses', 'course_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('personal_tutor_id', 'integer',[
                'null'=>true
            ])

            ->addForeignKey('personal_tutor_id', 'staff', 'staff_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('status', 'enum', [
                'values' => ['PROVISIONAL', 'LIVE','DORMANT'],
                'default' => 'PROVISIONAL',
            ])
            ->addColumn('dormancy_reason', 'enum', [
                'values' => ['GRADUATED', 'WITHDRAWN','TERMINATED'],
                'null'=>true
            ])
            ->addColumn('current_year', 'integer',[
                'length'=>4,
                'null'=>true
            ])
            ->addColumn('level', 'integer',[
                'null'=>true
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
            ])
            ->addColumn('updated_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => 'CURRENT_TIMESTAMP',
            ])
            ->addColumn('date_read_announcement', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
            ])->addColumn('password','string',[
                'null'=>true,
                'length'=>250
            ])
            ->create();

        $this->execute('ALTER TABLE students AUTO_INCREMENT=20171001');

        $this->execute('
        
DROP TRIGGER IF EXISTS trig_generate_student_id;

CREATE TRIGGER trig_generate_student_id BEFORE INSERT ON students
FOR EACH ROW BEGIN
    
    DECLARE previous_year VARCHAR(4);
    DECLARE this_year VARCHAR(4);
	DECLARE next_personal_tutor_id INT;
    DECLARE no_of_students_by_pt INT;

	

    
	SET @auto_id := ( SELECT AUTO_INCREMENT 
                    FROM INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_NAME=\'students\'
                      AND TABLE_SCHEMA=DATABASE() );
                    
	SET previous_year = SUBSTRING(@auto_id,1,4);
	SET this_year = YEAR(CURDATE());

    IF previous_year != this_year THEN
		SET NEW.student_id = CAST(CONCAT(this_year,\'\', \'1001\') AS unsigned);
	/*ELSEIF NEW.student_id IS NULL THEN
		SET NEW.student_id = @auto_id;*/
	END IF;
    
    
    SELECT s.staff_id,count(st.student_id) as num INTO next_personal_tutor_id,no_of_students_by_pt FROM staff s
		LEFT JOIN students st
        ON s.staff_id = st.personal_tutor_id
        JOIN staff_roles sr
        ON s.staff_id = sr.staff_id
        WHERE sr.role=\'PT\'
        GROUP BY s.staff_id
        ORDER BY num
        LIMIT 1;
        
   IF NEW.status = \'LIVE\' THEN
		SET NEW.personal_tutor_id = next_personal_tutor_id;
   END IF;
END;


        ');
    }
}
