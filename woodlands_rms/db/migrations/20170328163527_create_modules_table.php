<?php

use Phinx\Migration\AbstractMigration;

class CreateModulesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $modulesTable = $this->table('modules',array('id' => false, 'primary_key' => 'module_id'));
        $modulesTable
            ->addColumn('module_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('module_code', 'string',[
                'length'=>10
            ])
            ->addIndex(array('module_code'), array('unique' => true, 'name' => 'u_module_module_code'))
            ->addColumn('title', 'string',[
                'length'=>100
            ])
            ->addColumn('level', 'integer',[
                'length'=>3,
                'null'=>true
            ])
            ->addColumn('credits', 'integer',[
                'length'=>3
            ])
            ->addColumn('ass1',  'integer',[
                'length'=>3
            ])
            ->addColumn('ass2',  'integer',[
                'length'=>3
            ])
            ->addColumn('exam',  'integer',[
                'length'=>3
            ])
            ->addColumn('status', 'enum',[
                'values'=>['LIVE','DORMANT'],
                'default'=>'LIVE'
            ])
            ->create();
    }
}
