<?php

use Phinx\Migration\AbstractMigration;

class CreateExamFilesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('exam_files',array('id' => false, 'primary_key' => 'exam_file_id'));
        $table
            ->addColumn('exam_file_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('exam_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('exam_id', 'exams', 'exam_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('file_url', 'string',[
                'length'=>255
            ])
            ->create();
    }
}
