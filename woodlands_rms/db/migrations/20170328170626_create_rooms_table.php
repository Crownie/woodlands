<?php

use Phinx\Migration\AbstractMigration;

class CreateRoomsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $roomsTable = $this->table('rooms',array('id' => false, 'primary_key' => 'room_id'));
        $roomsTable
            ->addColumn('room_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('room_number', 'string',[
                'length'=>10
            ])
            ->addIndex(array('room_number'), array('unique' => true, 'name' => 'u_room_room_number'))
            ->addColumn('room_type', 'string',[
                'length'=>100,
                'null'=>true
            ])
            ->addColumn('capacity', 'integer',[
                'length'=>9
            ])
            ->create();
    }
}
