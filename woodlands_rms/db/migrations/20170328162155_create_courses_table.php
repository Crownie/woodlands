<?php

use Phinx\Migration\AbstractMigration;

class CreateCoursesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $coursesTable = $this->table('courses',array('id' => false, 'primary_key' => 'course_id'));
        $coursesTable
            ->addColumn('course_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('department_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('department_id', 'departments', 'department_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))

            ->addColumn('title', 'string',[
                'length'=>100
            ])
            ->addColumn('cost', 'decimal',[
                'precision'=>9,
                'scale'=>2
            ])
            ->addColumn('start_year', 'integer',[
                'length'=>4
            ])
            ->addColumn('duration', 'integer')
            ->addColumn('description', 'string')
            ->addColumn('ucas_points', 'integer',[
                'length'=>3,
                'null' => true
            ])
            ->addColumn('status', 'enum', [
                'values' => [ 'LIVE','DORMANT'],
                'default' => 'LIVE',
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
            ])
            ->addColumn('updated_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => 'CURRENT_TIMESTAMP',
            ])
            ->create();
    }
}
