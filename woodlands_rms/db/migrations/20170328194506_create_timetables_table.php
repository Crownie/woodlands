<?php

use Phinx\Migration\AbstractMigration;

class CreateTimetablesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('timetables',array('id' => false, 'primary_key' => 'timetable_id'));
        $table
            ->addColumn('timetable_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('course_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('course_id', 'courses', 'course_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('year', 'integer',[
                'length'=>4
            ])
            ->addColumn('level', 'integer')
            ->addIndex(array('course_id','year','level'), array('unique' => true, 'name' => 'u_timetable'))
            ->create();
    }
}
