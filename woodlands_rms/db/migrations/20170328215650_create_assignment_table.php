<?php

use Phinx\Migration\AbstractMigration;

class CreateAssignmentTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('assignments',array('id' => false, 'primary_key' => 'assignment_id'));
        $table
            ->addColumn('assignment_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('title', 'string')
            ->addColumn('start_date', 'datetime')
            ->addColumn('due_date', 'datetime')
            ->addColumn('description', 'string')
            ->addColumn('module_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('module_id', 'modules', 'module_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))

            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
            ])
            ->create();
    }
}
