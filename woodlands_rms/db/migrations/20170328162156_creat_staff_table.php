<?php

use Phinx\Migration\AbstractMigration;

class CreatStaffTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('staff',array('id' => false, 'primary_key' => 'staff_id'));
        $table
            ->addColumn('staff_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('status', 'enum',[
                'values'=>['LIVE','DORMANT'],
                'default'=>'LIVE'
            ])
            ->addColumn('department_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('department_id', 'departments', 'department_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))

            ->addColumn('dormancy_reason', 'enum',[
                'values'=>['RETIRED','RESIGNED','MISCONDUCT'],
                'null'=>true
            ])
            ->addColumn('first_name', 'string',[
                'length'=>45
            ])
            ->addColumn('middle_name', 'string',[
                'length'=>45
            ])
            ->addColumn('surname', 'string',[
                'length'=>45
            ])
            ->addColumn('address_line1', 'string',[
                'length'=>100
            ])
            ->addColumn('address_line2', 'string',[
                'length'=>100
            ])
            ->addColumn('city', 'string',[
                'length'=>45
            ])
            ->addColumn('country', 'string',[
                'length'=>60
            ])
            ->addColumn('postcode', 'string',[
                'length'=>45
            ])
            ->addColumn('phone', 'string',[
                'length'=>45
            ])
            ->addColumn('email', 'string',[
                'length'=>250
            ])->addColumn('password','string',[
                'null'=>true,
                'length'=>250
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
            ])
            ->create();

        $this->execute('ALTER TABLE staff AUTO_INCREMENT=99100001');
    }
}
