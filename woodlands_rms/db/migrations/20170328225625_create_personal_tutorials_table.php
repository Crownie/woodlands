<?php

use Phinx\Migration\AbstractMigration;

class CreatePersonalTutorialsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('personal_tutorials',array('id' => false, 'primary_key' => 'personal_tutorial_id'));
        $table
            ->addColumn('personal_tutorial_id', 'integer', [
                'identity' => true
            ])
            ->addColumn('personal_tutorial_meeting', 'string')
            ->addColumn('date_of_meeting', 'date')
            ->addColumn('tutorial_summary', 'string')
            ->addColumn('action_points_summary', 'string')
            ->addColumn('next_meeting_date', 'string')
            ->addColumn('student_attended', 'boolean',[
                'default'=>0
            ])
            ->addColumn('staff_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('staff_id', 'staff', 'staff_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addColumn('student_id', 'integer',[
                'null'=>true
            ])
            ->addForeignKey('student_id', 'students', 'student_id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->create();
    }
}
