<?php
use app\utilities\validator\Validator;

/**
 * Created by Tobi Ayilara.
 * Date: 13/04/2017
 * Time: 16:04
 */
class ValidatorTest extends PHPUnit_Framework_TestCase
{
    function testInvalidLength(){
        $data = [
            'sample_field'=>'He'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->length_between(3,4);

        $this->assertFalse($validator->isValid());
    }

    function testValidLength(){
        $data = [
            'sample_field'=>'Hel'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->length_between(3,4);

        $this->assertTrue($validator->isValid());
    }

    function testInvalidRequired(){
        $data = [
            'sample_field'=>''
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->required();

        $this->assertFalse($validator->isValid());
    }

    function testValidRequired(){
        $data = [
            'sample_field'=>'D'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->required();

        $this->assertTrue($validator->isValid());
    }

    function testInvalidEmail(){
        $data = [
            'sample_field'=>'steve@me'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->email();

        $this->assertFalse($validator->isValid());
    }

    function testValidEmail(){
        $data = [
            'sample_field'=>'steve@me.com'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->email();

        $this->assertTrue($validator->isValid());
    }

    function testInvalidAlphanumeric(){
        $data = [
            'sample_field'=>'steve@me'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->alphanumeric();

        $this->assertFalse($validator->isValid());
    }

    function testValidAlphanumeric(){
        $data = [
            'sample_field'=>'Helllo1'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->alphanumeric();

        $this->assertTrue($validator->isValid());
    }

    function testInvalidNumeric(){
        $data = [
            'sample_field'=>'steve@me'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->numeric();

        $this->assertFalse($validator->isValid());
    }

    function testValidNumeric(){
        $data = [
            'sample_field'=>'12'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('sample_field')->numeric();

        $this->assertTrue($validator->isValid());
    }

    function testInvalidMatchesField(){
        $data = [
            'password'=>'Hello1',
            'confirm_password'=>'Hello2'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('confirm_password')->matchesField('password');

        $this->assertFalse($validator->isValid());
    }

    function testValidMatchesField(){
        $data = [
            'password'=>'Hello1',
            'confirm_password'=>'Hello1'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('confirm_password')->matchesField('password');

        $this->assertTrue($validator->isValid());
    }

    function testInvalidAlpha(){
        $data = [
            'name'=>'72812'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('name')->alpha();

        $this->assertFalse($validator->isValid());
    }

    function testValidAlpha(){
        $data = [
            'name'=>'Hello'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('name')->alpha();

        $this->assertTrue($validator->isValid());
    }

    function testNoRule(){
        $data = [
            'name'=>'Hello'
        ];
        $validator = new Validator($data);

        $this->assertTrue($validator->isValid());
    }

    function testCustomErrorMessageWhenTrue(){
        $data = [
            'name'=>'Hello'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('name')->customErrorMessageWhenTrue(true,'Error Message');

        $this->assertFalse($validator->isValid());
    }

    function testCustomErrorMessageWhenTrue2(){
        $data = [
            'name'=>'Hello'
        ];
        $validator = new Validator($data);
        $validator->startRulesFor('name')->customErrorMessageWhenTrue(false,'Error Message');

        $this->assertTrue($validator->isValid());
    }

}
