<?php

/**
 * Created by PhpStorm.
 * User: Tomas Taujanskas
 * Date: 18/04/2017
 * Time: 11:29
 */
class StaffTest extends PHPUnit_Framework_TestCase
{
    function testValidStaffCreate(){
        $data = [
            'first_name'=>'Mary',
            'surname'=>'Bills',
            'address_line1'=>'2 Windsor Way',
            'city'=>'Leicestershire',
            'country'=>'UK',
            'postcode'=>'LE2 4TD',
            'phone'=>'07954855687',
            'email'=>'BillsMary@example.org'
        ];
        $staff = new \app\models\Staff($data);

        $validator = $staff->getValidator();
        $this->assertTrue($validator->isValid());
    }
    function testEmptyStaffName(){
        $data = [
            'first_name'=>'',
            'surname'=>'',
            'address_line1'=>'2 Windsor Way',
            'city'=>'Leicestershire',
            'country'=>'UK',
            'postcode'=>'LE2 4TD',
            'phone'=>'07954855687',
            'email'=>'BillsMary@example.org'
        ];
        $staff = new \app\models\Staff($data);

        $validator = $staff->getValidator();
        $this->assertFalse($validator->isValid());
    }
    function testEmptyStaffAddress(){
        $data = [
            'first_name'=>'Mary',
            'surname'=>'Bills',
            'address_line1'=>'',
            'city'=>'',
            'country'=>'',
            'postcode'=>'',
            'phone'=>'07954855687',
            'email'=>'BillsMary@example.org'
        ];
        $staff = new \app\models\Staff($data);

        $validator = $staff->getValidator();
        $this->assertFalse($validator->isValid());
    }
    function testEmptyStaffPhone(){
        $data = [
            'first_name'=>'Mary',
            'surname'=>'Bills',
            'address_line1'=>'2 Windsor Way',
            'city'=>'Leicestershire',
            'country'=>'UK',
            'postcode'=>'LE2 4TD',
            'phone'=>'',
            'email'=>'BillsMary@example.org'
        ];
        $staff = new \app\models\Staff($data);

        $validator = $staff->getValidator();
        $this->assertFalse($validator->isValid());
    }
    function testEmptyStaffEmail(){
        $data = [
            'first_name'=>'Mary',
            'surname'=>'Bills',
            'address_line1'=>'2 Windsor Way',
            'city'=>'Leicestershire',
            'country'=>'UK',
            'postcode'=>'LE2 4TD',
            'phone'=>'07954855687',
            'email'=>''
        ];
        $staff = new \app\models\Staff($data);

        $validator = $staff->getValidator();
        $this->assertFalse($validator->isValid());
    }
    function testInvalidStaffEmailFormat(){
        $data = [
            'first_name'=>'',
            'surname'=>'',
            'address_line1'=>'2 Windsor Way',
            'city'=>'Leicestershire',
            'country'=>'UK',
            'postcode'=>'LE2 4TD',
            'phone'=>'07954855687',
            'email'=>'BillsMary'
        ];
        $staff = new \app\models\Staff($data);

        $validator = $staff->getValidator();
        $this->assertFalse($validator->isValid());
    }
}
