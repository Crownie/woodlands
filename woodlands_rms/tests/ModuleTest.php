<?php

/**
 * Created by Tobi Ayilara.
 * Date: 13/04/2017
 * Time: 15:47
 */
class ModuleTest extends PHPUnit_Framework_TestCase
{
    public function testOKModule(){
        $data = [
            'module_code' => 'C1202',
            'title' => 'Web programming',
            'level' => 1,
            'credits' => 20,
            'ass1' => 50,
            'ass2' => 50,
            'exam' => 0

        ];
        $module = new \app\models\Module($data);

        $validator = $module->getValidator();


        $this->assertTrue($validator->isValid());
    }
}
