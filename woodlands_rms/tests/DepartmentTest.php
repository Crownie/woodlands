<?php

/**
 * Created by Alex
 */
class DepartmentTest extends PHPUnit_Framework_TestCase
{
    public function testDepartmentNameNull(){
        $data = [
            'name'=>''
        ];
        $department = new \app\models\Department($data);

        $validator = $department->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testDepartmentNameNotNull(){
        $data = [
            'name'=>'Science & Technology'
        ];
        $department = new \app\models\Department($data);

        $validator = $department->getValidator();

        $this->assertTrue($validator->isValid());
    }
}
