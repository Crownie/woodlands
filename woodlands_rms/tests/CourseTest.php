<?php

/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 13/04/2017
 * Time: 16:37
 */
class CourseTest extends PHPUnit_Framework_TestCase
{

    public function testCourse(){

        $data = [

            'course_id'=>1,
            'title'=>'test',
            'cost'=>5000,
            'start_year'=>2017,
            'duration'=>'3 years',
            'description'=>'test',
            'ucas_points'=>300
        ];


        $course = new \app\models\Course($data);

        $validator = $course->getValidator();

        $this->assertTrue($validator->isValid());
    }

}


