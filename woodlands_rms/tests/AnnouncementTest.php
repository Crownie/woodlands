<?php

/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 13/04/2017
 * Time: 15:21
 */
class AnnouncementTest extends PHPUnit_Framework_TestCase
{
    public function testValidAnnouncement()
    {

        $data = [

            'announcement_id' => '1000',
            'title' => 'test',
            'announcement_type' => 'GENERAL',
            'message' => 'test',
            'module_id' => 1,
            'course_id' => '',
            'target' => 'STUDENT',
            'date_posted' => '2017-04-12 13:16:30'

        ];

        $announcement = new \app\models\Announcement($data);

        $validator = $announcement->getValidator();

        $this->assertTrue($validator->isValid());

    }


    public function testInvalidAnnouncement()
    {

        $data = [

            'announcement_id' => '1000',
            'title' => 'test',
            'announcement_type' => '',
            'message' => 'test',
            'module_id' => 1,
            'course_id' => '',
            'target' => 'STUDENT',
            'date_posted' => '2017-04-12 13:16:30'

        ];

        $announcement = new \app\models\Announcement($data);

        $validator = $announcement->getValidator();

        $this->assertFalse($validator->isValid());

    }


}

