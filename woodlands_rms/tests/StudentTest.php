<?php

/**
 * Created by Tobi Ayilara.
 * Date: 13/04/2017
 * Time: 14:39
 */
class StudentTest extends PHPUnit_Framework_TestCase
{
    public function testValidStudentCreate()
    {
        $data = [
            //'student_id' => '',
            'first_name' => 'John',
            'middle_name' => '',
            'surname' => 'Doe',
            'term_time_address_line1' => '25 Parade Way',
            'term_time_address_line2' => '3b Mall Close',
            'term_time_city' => 'Sheffield',
            'term_time_country' => 'UK',
            'term_time_postcode' => 'SF21 4BG',
            'address_line1' => '91 Home Road',
            'address_line2' => '',
            'city' => 'Dartford',
            'country' => 'UK',
            'postcode' => 'DA12 3SG',
            'phone' => '07958224691',
            'email' => 'JDDoe@example.org',
            'qualifications_verified' => 1,
            'course_id' => 1,
            'current_year' => date('Y'),
            'level' => '',
            'status' => 'PROVISIONAL',
            'personal_tutor_id' => 99100012,
            'password' => password_hash('student', PASSWORD_DEFAULT)
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();
        $this->assertTrue($validator->isValid());
    }
    function testEmptyStudentName(){
        $data = [
            'first_name' => '',
            'surname' => '',
            'term_time_address_line1' => '25 Parade Way',
            'term_time_address_line2' => '3b Mall Close',
            'term_time_city' => 'Sheffield',
            'term_time_country' => 'UK',
            'term_time_postcode' => 'SF21 4BG',
            'phone' => '07958224691',
            'email' => 'JDDoe@example.org'
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();


        $this->assertFalse($validator->isValid());
    }
    function testEmptyTermTimeAddress(){
        $data = [
            'first_name' => 'John',
            'surname' => 'Doe',
            'term_time_address_line1' => '',
            'term_time_address_line2' => '',
            'term_time_city' => '',
            'term_time_country' => '',
            'term_time_postcode' => '',
            'phone' => '07958224691',
            'email' => 'JDDoe@example.org'
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();


        $this->assertFalse($validator->isValid());
    }
    function testEmptyPhone(){
        $data = [
            'first_name' => 'John',
            'surname' => 'Doe',
            'term_time_address_line1' => '25 Parade Way',
            'term_time_address_line2' => '3b Mall Close',
            'term_time_city' => 'Sheffield',
            'term_time_country' => 'UK',
            'term_time_postcode' => 'SF21 4BG',
            'phone' => '',
            'email' => 'JDDoe@example.org'
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();


        $this->assertFalse($validator->isValid());
    }
    function testEmptyEmail(){
        $data = [
            'first_name' => 'John',
            'surname' => 'Doe',
            'term_time_address_line1' => '25 Parade Way',
            'term_time_address_line2' => '3b Mall Close',
            'term_time_city' => 'Sheffield',
            'term_time_country' => 'UK',
            'term_time_postcode' => 'SF21 4BG',
            'phone' => '07958224691',
            'email' => ''
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();


        $this->assertFalse($validator->isValid());
    }
    function testInvalidEmailFormat(){
        $data = [
            'first_name' => 'John',
            'surname' => 'Doe',
            'term_time_address_line1' => '25 Parade Way',
            'term_time_address_line2' => '3b Mall Close',
            'term_time_city' => 'Sheffield',
            'term_time_country' => 'UK',
            'term_time_postcode' => 'SF21 4BG',
            'phone' => '07958224691',
            'email' => 'johnDoe.com'
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();


        $this->assertFalse($validator->isValid());
    }
    function testInvalidNameFormat(){
        $data = [
            'first_name' => '!"££',
            'middle_name' =>'$%^&*$"!',
            'surname' => '????^&*&',
            'term_time_address_line1' => '25 Parade Way',
            'term_time_address_line2' => '3b Mall Close',
            'term_time_city' => 'Sheffield',
            'term_time_country' => 'UK',
            'term_time_postcode' => 'SF21 4BG',
            'phone' => '07958224691',
            'email' => 'johnDoe.com'
        ];
        $student = new \app\models\Student($data);

        $validator = $student->getValidator();


        $this->assertFalse($validator->isValid());
    }
}
