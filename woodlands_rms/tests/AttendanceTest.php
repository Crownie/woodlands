<?php

/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 13/04/2017
 * Time: 16:30
 */
class AttendanceTest extends PHPUnit_Framework_TestCase
{

    public function testAttendance(){

        $data = [

            'attendance_id'=>1,
            'status'=>'pending',
            'student_id'=>1,
            'module_id'=>1,
            'week'=>24
        ];

        $attendance = new \app\models\Attendance($data);

        $validator = $attendance->getValidator();

        $this->assertTrue($validator->isValid()); // no validator available so it wont work yet

    }

}
