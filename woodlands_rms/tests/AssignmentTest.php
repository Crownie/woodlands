<?php

/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 13/04/2017
 * Time: 15:35
 */
class AssignmentTest extends PHPUnit_Framework_TestCase
{

    public function testValidAssignment(){

        $data = [

            'start_date'=>'2017-04-12 13:16:30',
            'due_date'=>'2017-10-12 13:16:30',
            'title'=>'test',
            'description'=>'testing',
            'module_id'=>1,
            'assignment_id'=>1
        ];

        $assignment = new \app\models\Assignment($data);

        $validator = $assignment->getValidator();

        $this->assertTrue($validator->isValid());
    }


    public function testInvalidAssignment(){

        $data = [

            'start_date'=>'?',
            'due_date'=>'2017-10-12 13:16:30',
            'title'=>'test',
            'description'=>'testing',
            'module_id'=>'?',
            'assignment_id'=>1
        ];

        $assignment = new \app\models\Assignment($data);

        $validator = $assignment->getValidator();

        $this->assertFalse($validator->isValid()); //gives error
    }
}
