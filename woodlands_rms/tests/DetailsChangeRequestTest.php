<?php

/**
 * Created by Alex
 */
class DetailsChangeRequestTest extends PHPUnit_Framework_TestCase
{
    public function testChangedDataNull(){
        $data = [
            'changed_data'=>'',
            'status'=>'CLOSE'
        ];
        $details_change_request = new \app\models\DetailsChangeRequest($data);

        $validator = $details_change_request->getValidator();

        $this->assertTrue($validator->isValid());
    }
    public function testStatusNull(){
        $data = [
            'changed_data'=>'address',
            'status'=>''
        ];
        $details_change_request = new \app\models\DetailsChangeRequest($data);

        $validator = $details_change_request->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testChangedDataAndStatusNull(){
        $data = [
            'changed_data'=>'',
            'status'=>''
        ];
        $details_change_request = new \app\models\DetailsChangeRequest($data);

        $validator = $details_change_request->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testStatusNotOpenOrClosed(){
        $data = [
            'changed_data'=>'address',
            'status'=>'???'
        ];
        $details_change_request = new \app\models\DetailsChangeRequest($data);

        $validator = $details_change_request->getValidator();

        $this->assertTrue($validator->isValid());
    }
    public function testChangedDataAndStatusNotNull(){
        $data = [
            'changed_data'=>'address',
            'status'=>'OPEN'
        ];
        $details_change_request = new \app\models\DetailsChangeRequest($data);

        $validator = $details_change_request->getValidator();

        $this->assertTrue($validator->isValid());
    }
}
