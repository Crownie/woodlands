<?php

/**
 * Created by Alex
 */
class CourseModuleTest extends PHPUnit_Framework_TestCase
{
    public function testCourseIdNull(){
        $data = [
        'course_id'=>'',
        'module_id'=>'123'
        ];
        $course_module = new \app\models\CourseModule($data);

        $validator = $course_module->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testModuleIdNull(){
    $data = [
        'course_id'=>'1',
        'module_id'=>''
    ];
    $course_module = new \app\models\CourseModule($data);

    $validator = $course_module->getValidator();

    $this->assertTrue($validator->isValid());
}
    public function testCourseIdAndModuleIdNull(){
        $data = [
            'course_id'=>'',
            'module_id'=>''
        ];
        $course_module = new \app\models\CourseModule($data);

        $validator = $course_module->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testCourseIdAndModuleIdNotNull(){
        $data = [
            'course_id'=>'1',
            'module_id'=>'1'
        ];
        $course_module = new \app\models\CourseModule($data);

        $validator = $course_module->getValidator();

        $this->assertTrue($validator->isValid());
    }
}
