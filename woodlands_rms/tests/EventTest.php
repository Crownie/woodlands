<?php

/**
 * Created by Alex
 */
class EventTest extends PHPUnit_Framework_TestCase
{
    public function testEventTitleNull(){
        $data = [
            'title'=>'',
            'start_date'=>'12-SEP-2016',
            'end_date'=>'12-OCT-2016',
            'location'=>'Campus',
            'description'=>'Test',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testStartDateNull(){
        $data = [
            'title'=>'Job Fair',
            'start_date'=>'',
            'end_date'=>'12-OCT-2016',
            'location'=>'Campus',
            'description'=>'Test',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testEndDateNull(){
        $data = [
            'title'=>'Job Fair',
            'start_date'=>'12-SEP-2016',
            'end_date'=>'',
            'location'=>'Campus',
            'description'=>'Test',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testLocationNull(){
        $data = [
            'title'=>'Job Fair',
            'start_date'=>'12-SEP-2016',
            'end_date'=>'14-SEP-2016',
            'location'=>'',
            'description'=>'Test',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testDescriptionNull(){
        $data = [
            'title'=>'Job Fair',
            'start_date'=>'12-SEP-2016',
            'end_date'=>'14-SEP-2016',
            'location'=>'Campus',
            'description'=>'',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testAllFieldsNull(){
        $data = [
            'title'=>'',
            'start_date'=>'',
            'end_date'=>'',
            'location'=>'',
            'description'=>'',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }

    public function testNoFieldsNull(){
        $data = [
            'title'=>'Job Fair',
            'start_date'=>'12-SEP-2016',
            'end_date'=>'14-SEP-2016',
            'location'=>'Campus',
            'description'=>'Test',
            'staff_id'=>'',
            'student_id'=>''
        ];
        $event = new \app\models\Event($data);

        $validator = $event->getValidator();

        $this->assertTrue($validator->isValid());
    }
}
