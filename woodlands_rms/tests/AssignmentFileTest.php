<?php

/**
 * Created by PhpStorm.
 * User: Saif
 * Date: 13/04/2017
 * Time: 16:20
 */
class AssignmentFileTest extends PHPUnit_Framework_TestCase
{

    public function testAssignmentFile(){

        $data = [

            'assignment_id'=>'1',
            'file_url'=>'http://stackoverflow.com/'
        ];

        $assignment_file = new \app\models\AssignmentFile($data);

        $validator = $assignment_file->getValidator();

        $this->assertTrue($validator->isValid()); // no validator available so it wont work yet
    }
}
