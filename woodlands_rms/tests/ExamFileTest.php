<?php

/**
 * Created by Alex
 */
class ExamFileTest extends PHPUnit_Framework_TestCase
{
    public function testAllFieldsNull(){
        $data = [
            'exam_id'=>'',
            'file_url'=>''
        ];
        $exam_file = new \app\models\ExamFile($data);

        $validator = $exam_file->getValidator();

        $this->assertTrue($validator->isValid());
    }
    public function testExamIdNull(){
        $data = [
            'exam_id'=>'',
            'file_url'=>'http://stackoverflow.com/'
        ];
        $exam_file = new \app\models\ExamFile($data);

        $validator = $exam_file->getValidator();

        $this->assertTrue($validator->isValid());
    }
    public function testFileUrlNull(){
        $data = [
            'exam_id'=>'2',
            'file_url'=>''
        ];
        $exam_file = new \app\models\ExamFile($data);

        $validator = $exam_file->getValidator();

        $this->assertTrue($validator->isValid());
    }

        //this test should pass, but it fails
    public function testNoFieldsNull(){
        $data = [
            'exam_id'=>'2',
            'file_url'=>'https://stackoverflow.com/'
        ];
        $exam_file = new \app\models\ExamFile($data);

        $validator = $exam_file->getValidator();

        $this->assertTrue($validator->isValid());
    }

}
